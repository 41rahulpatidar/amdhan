import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {
  version:any;
  constructor() { }

  ngOnInit() {
    if (localStorage.getItem("app_conf")) {
      this.version = JSON.parse(localStorage.getItem("app_conf"))[0].app_meta_data.app_version;
    }else{
      this.version = "v3";
    }
  }

}
