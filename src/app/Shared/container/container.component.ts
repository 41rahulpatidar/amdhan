import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { Language } from 'src/app/Other/translation.constants';

@Component({
  selector: 'app-container',
  templateUrl: './container.component.html',
  styleUrls: ['./container.component.scss']
})
export class ContainerComponent implements OnInit {

  @Input() backPath: string = "";
  @Input() userData: any;
  label: any = Language;
  constructor(private router: Router) { }

  ngOnInit() {
    var body = document.body;
    body.classList.remove("login-background");
    body.classList.add("other-background");
  }

  goToBack() {
    this.router.navigate([this.backPath]);
  }
}

