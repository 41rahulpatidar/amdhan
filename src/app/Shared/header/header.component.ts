import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Language } from 'src/app/Other/translation.constants';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  @Input() userData: any;
  label: any = Language;
  className = HeaderComponent.name;

  pwdData:any = {};
  profileData: any = {};
  logoImagePath:any = '/assets/images/amdhan-logo.png';
  constructor(private router: Router) {
  }

  ngOnInit() {

    if (localStorage.getItem("logged_in_user_details")) {
      this.userData = JSON.parse(localStorage.getItem("logged_in_user_details"));
    }
  }

  logout() {
    // localStorage.clear();
    //issue while logut fixed
    // localStorage.removeItem("userInfo");
    localStorage.removeItem("vendor_token");
    localStorage.removeItem("logged_in_user_details");
    localStorage.removeItem("current_section");
    this.router.navigate(['/login']);

    var body = document.body;
    body.classList.add("login-background");
    body.classList.remove("other-background");
  }

  gotToDashboard(){
    this.router.navigate(['/dashboard']);
  }

  onChangePassClick(){
    
  }

}
