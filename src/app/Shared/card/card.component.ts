import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Language } from '../../Other/translation.constants';
import { Router } from '@angular/router';
import { varConstants } from 'src/app/Other/variable.constants';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss']
})
export class CardComponent implements OnInit {

  label: any = Language;
  @Input() headerVisible:boolean = false;
  @Input() headerData:any = {
    backPath : "",
    heading : "",
    addButtonRoute : "",
    addButtonToolTip: ""
  };
  @Output() callFunction = new EventEmitter;

  constructor(private router : Router) { }

  ngOnInit() {
  }

  goToPage(route){
    if(route == "/inquiry-quotation-section/inquiry-details"){
          let object=[];
          object['from'] = "/inquiry-quotation-section/inquiry";
          object['tab'] = this.label.Inquiry;
          object['mode'] = varConstants.isNew;
          this.router.navigate([route], { state: { data: object } });
    }else if(route == "/asn-shipment-section/delivery-details"){
        let object=[];
          object['from'] = "/inquiry-quotation-section/inquiry";
          object['tab'] = this.label.Inquiry;
          object['mode'] = varConstants.isNew;
          this.router.navigate([route], { state: { data: object } });
    }
    else if(route == "/orders/order-details"){
        let object=[];
          object['from'] = "/orders/new";
          object['tab'] = this.label.Orders;
          object['mode'] = varConstants.isNew;
          this.router.navigate([route], { state: { data: object } });
    }
    else if(route == "/marketing-section/message-details"){
        let object=[];
          object['from'] = "/marketing-section/message";
          object['tab'] = this.label.SUB_TAB.Message;
          object['mode'] = varConstants.isNew;
          this.router.navigate([route], { state: { data: object } });
    }
    else{
          this.router.navigate([route])
    }
  }

  onClickAdd(){
    if (this.headerData.addButtonRoute) {
      this.goToPage(this.headerData.addButtonRoute);
    }else{
      this.callFunction.emit();
    }
  }
}
