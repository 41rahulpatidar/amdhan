import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { PackagedModule } from "../Packaged/packaged.module";
import { CommonModule } from '@angular/common';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule,
    PackagedModule,
  ],
  exports: [
    PackagedModule
  ], 
  providers: []
})
export class SharedModule { }
