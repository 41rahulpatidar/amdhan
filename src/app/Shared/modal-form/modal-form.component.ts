import { Component, Input, OnInit, OnChanges, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-modal-form',
  templateUrl: './modal-form.component.html',
  styleUrls: ['./modal-form.component.scss']
})
export class ModalFormComponent implements OnChanges {
  @Input() modalData:any = {
    modalId: "default-modal",
    header: "",
    close: "close"
  };
  @Output() callFunction = new EventEmitter;
  constructor() { }

  ngOnChanges() {
  }

  closeModal() {
    document.getElementById(this.modalData.modalId).style.display = "none";
    document.getElementById(this.modalData.modalId).classList.remove("show");
    document.body.classList.remove("modal-open");
  }

  onClickSave(){
    this.callFunction.emit(this.modalData.modalId);
  }

}
