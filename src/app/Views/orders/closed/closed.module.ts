import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PackagedModule } from 'src/app/Packaged/packaged.module';
import { ClosedRoutingModule } from './closed-routing.module';
import { ClosedComponent } from './closed.component';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';

@NgModule({
  declarations: [ClosedComponent],
  imports: [
    CommonModule,
    PackagedModule,
    ClosedRoutingModule,
    OwlDateTimeModule, 
   	OwlNativeDateTimeModule
  ]
})
export class ClosedModule { }
