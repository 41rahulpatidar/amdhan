import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PackagedModule } from 'src/app/Packaged/packaged.module';
import { OpenRoutingModule } from './new-routing.module';
import { OpenComponent } from './open.component';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';

@NgModule({
  declarations: [OpenComponent],
  imports: [
    CommonModule,
    PackagedModule,
    OpenRoutingModule,
    OwlDateTimeModule, 
   	OwlNativeDateTimeModule
  ]
})
export class OpenModule { }
