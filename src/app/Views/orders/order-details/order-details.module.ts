import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PackagedModule } from 'src/app/Packaged/packaged.module';

import { OrderDetailsRoutingModule } from './order-details-routing.module';
import { OrderDetailsComponent } from './order-details.component';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';
import { Angular2CsvModule } from 'angular2-csv';

@NgModule({
  declarations: [OrderDetailsComponent],
  imports: [
    CommonModule,
    PackagedModule,
    OrderDetailsRoutingModule,
    NgMultiSelectDropDownModule.forRoot(),
    OwlDateTimeModule, 
   	OwlNativeDateTimeModule,
   	Angular2CsvModule
  ]
})
export class OrderDetailsModule { }
