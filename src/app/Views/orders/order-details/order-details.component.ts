import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { AlertService } from 'src/app/Service/alert.service';
import { Language } from '../../../Other/translation.constants';
import { varConstants } from 'src/app/Other/variable.constants';
import { NgxSpinnerService } from 'ngx-spinner';
import { UtilService } from 'src/app/Service/util.service';
import { OrdersService } from '../orders.service';
import { IDropdownSettings } from 'ng-multiselect-dropdown';
import {DatePipe} from '@angular/common';
import { saveAs as download } from "file-saver";

@Component({
  selector: 'app-order-details',
  templateUrl: './order-details.component.html',
  styleUrls: ['./order-details.component.scss']
})
export class OrderDetailsComponent implements OnInit {

  @ViewChild('addInquiryForm', { static: true }) addInquiryForm: any;
  inquiryDetails: any = {};
  userData: any;
  tabsArray: any = [];
  label: any = Language;
  isEdit: any = {
    inquiryDetailsEditModeEnable: false
  };
  addInquiryData: any = {
    fieldname: "",
    FirstName: "",
    LastName: "",
    Username: "",
    Email: "",
    Password: ""
  }
  inquiryLineItemsDetails: any = [];
  itemsDetails:any = {};
  viewModeField: boolean = false;
  editOrderId: any = '';
  dropdownStatusList: any = [];
  selectedStatusItems: any = [];
  dropdownSettingsStatus:IDropdownSettings;

  invoiceItemDetails: any = [];
  editInquiryItemIndex: any = '';
  documentDetails: any = {};
  invoiceDocumentDetails: any = [];
  editDocumentItemIndex: any = '';
  validToMin: any;

  soldToPartyList: any = [];
  selectedSoldToPartyItems: any = [];
  dropdownSoldToPartySettings: any = {};

  shipToPartyList: any = [];
  selectedShipToPartyItems: any = [];
  dropdownShipToPartySettings: any = {};

  sdDocCurrencyList: any = [];
  selectedSdDocCurrencyItems: any = [];
  dropdownSdDocCurrencySettings: any = {};

  salesOrganizationList: any = [];
  selectedSalesOrgItem: any = [];
  salesOrgSetting: any = {};

  materilMasterList: any = [];
  materialMasterSelectItem: any = [];
  materialMasterSetting: any = {};

  invoiceItemSelectedItem: any = [];
  invocieItemSetting: any = {};

  formSaveErrorMsg: any = '';
  pipe = new DatePipe('en-US');
  inquiryDetailsLabel : any = '';
  currentDate: any = new Date();
  currentSection: any;
  visibaleButton: boolean= true;
  selectedFile: any = '';
  inquiryLineItemForm: any = {};
  inquiryDocumentForm: any = {};
  isMenuAccess: boolean = true;
  isAddEditPermission: boolean = true;
  showApproveBttn: boolean = false;
  backPath: any = 'orders/all';
  enableEditEnquiryForm: boolean = true;
  options: any = {}
  orderExportData: any = [];

  constructor(private router: Router,
    private alertService: AlertService,
    private spinner: NgxSpinnerService,
    private inquiryQuote: OrdersService,
    private util: UtilService,) { }

  ngOnInit() {

    this.options = {
      fieldSeparator: ',',
      quoteStrings: '"',
      decimalseparator: '.',
      showLabels: true,
      headers: [],
      showTitle: true,
      title: 'Order Details',
      useBom: false,
      removeNewLines: true
    };

    this.inquiryLineItemForm = {
      submitted: false
    }
    this.inquiryDocumentForm = {
      submitted: false
    }
    this.currentSection = JSON.parse(localStorage.getItem("current_section"));
    if(this.currentSection.type == 'CloseInquiry'){
      this.visibaleButton = false;
    }
    this.inquiryDetailsLabel = this.label.TAB.Order_Details;
    this.selectedStatusItems = [ { id: 0, text: 'Draft' }];
    this.getCustomerMasterList();
    this.dropdownStatusList = [
      { id: 0, text: 'Draft' },
      { id: 1, text: 'Submited' },
      { id: 2, text: 'Open' },
      { id: 3, text: 'Close' },
      { id: 4, text: 'Rejected' },
    ];
    this.sdDocCurrencyList = [{id: 'INR', text: 'INR'}];
    this.selectedSdDocCurrencyItems = [{id: 'INR', text: 'INR'}];
    this.dropdownSettingsStatus = {
      singleSelection: true,
      idField: 'id',
      textField: 'text',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      allowSearchFilter: true,
      closeDropDownOnSelection: true
    };

    this.materialMasterSetting = {
      singleSelection: true,
      idField: 'Material_ID',
      textField: 'MaterialNumber',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      allowSearchFilter: true,
      closeDropDownOnSelection: true
    }
    this.dropdownSoldToPartySettings = {
      singleSelection: true,
      idField: 'CustomerNumber',
      textField: 'CustomerName',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      allowSearchFilter: true,
      closeDropDownOnSelection: true
    };

    this.dropdownShipToPartySettings = {
      singleSelection: true,
      idField: 'BusinessPartnerCustomerNo',
      textField: 'CustomerName',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      allowSearchFilter: true,
      closeDropDownOnSelection: true
    };

    this.dropdownSdDocCurrencySettings = {
      singleSelection: true,
      idField: 'id',
      textField: 'text',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      itemsShowLimit: 3,
      allowSearchFilter: true,
      closeDropDownOnSelection: true
    }

    this.salesOrgSetting = {
      singleSelection: true,
      idField: 'SalesOrg',
      textField: 'SalesOrg',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      itemsShowLimit: 3,
      allowSearchFilter: true,
      closeDropDownOnSelection: true
    }

    this.invocieItemSetting = {
      singleSelection: true,
      idField: 'SalesOrder_ItemID',
      textField: 'Item',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      itemsShowLimit: 3,
      allowSearchFilter: true,
      closeDropDownOnSelection: true
    }

    this.editOrderId = localStorage.getItem('editOrderId');
    if(this.editOrderId){
      this.enableEditEnquiryForm = false;
    }
    console.log(this.editOrderId, 'edit enquiry')
    this.itemsDetails.Material_Number = '';
    this.userData = JSON.parse(localStorage.getItem("logged_in_user_details"));
    if (history.state.data) {
      this.inquiryDetails = history.state.data;
      this.tabsArray = [
        { id: "inquiry-items-section1", name: this.label.TAB.Items, isActiveClass: "active" },
        { id: "inquiry-items-section2", name: this.label.TAB.Documents, isActiveClass: "" }
      ]
      if (history.state.data['mode'] == varConstants.isRegistered) {
        //this.inquiryDetails = history.state.data;
      } else {
        //this.enableForm('addInquiryForm');
      }
    } else {
      this.router.navigate(['/dashboard']);
    }

    //**** User Menu Access Start ****//
    if(this.isMenuAccess == false){
      this.router.navigate(["/dashboard"]);
    }
    if(this.userData.RoleType == '2'){
      this.isAddEditPermission = false;
    }
    if(this.userData.RoleType == '3'){
      if(this.userData.Lst_RoleData1[0].ApprovalLevel == 'L01'){
        this.isAddEditPermission = false;
      }else{
        this.isAddEditPermission = true;
      }
    }
    //**** User Menu Access End ****//

    this.getMaterialMasterList();
    setTimeout(()=> {
      this.inquiryDetails.CreatedDate = new Date();
    },500)

    this.backPath = 'orders/'+localStorage.getItem('previousOrderSection');
  }
  
  onChangeTab(tabDetails: any) {
    for (let i = 0; i < this.tabsArray.length; i++) {
      if (tabDetails.id == this.tabsArray[i].id) {
        this.tabsArray[i].isActiveClass = "active";
      } else {
        this.tabsArray[i].isActiveClass = "";
      }
    }
  }

  openModal(id) {
    this.inquiryLineItemForm.submitted = false;
    this.inquiryDocumentForm.submitted = false;
    this.itemsDetails = {}
    document.getElementById(id).style.display = "block";
    document.getElementById(id).classList.add("show");
    document.body.classList.add("modal-open");
    if(id == 'add-inquiry-items-modal'){
      this.materialMasterSelectItem = [];
      this.editInquiryItemIndex = '';
      for (let i = 0; i <= 100; i++) {
        let item = (i+1)* 10;
        let tmp = this.invoiceItemDetails.find(list => list.Item == item);
        console.log(tmp, 'item', item, this.invoiceItemDetails)
        if(tmp == undefined){
          this.itemsDetails.Item = item;
          break;
        }
      }
    }

    if(id == 'add-document-items-modal'){
      this.editDocumentItemIndex = '';
      this.documentDetails = {};
    }
  }

  closeModal(id) {
    document.getElementById(id).style.display = "none";
    document.getElementById(id).classList.remove("show");
    document.body.classList.remove("modal-open");
  }

  addInquiryItem(event){
    this.inquiryLineItemForm.submitted = true;
    if(!this.itemsDetails.Item || this.materialMasterSelectItem.length == 0 || !this.itemsDetails.Description || !this.itemsDetails.NetValue || !this.itemsDetails.OrderQunatity){
      return false;
    }
    let loginUserDetails = localStorage.getItem('logged_in_user_details');
    loginUserDetails = JSON.parse(loginUserDetails);
    if(this.itemsDetails['SalesOrder_ItemID'] != undefined && this.itemsDetails['SalesOrder_ItemID'] != '0' && this.itemsDetails['SalesOrder_ItemID'] != 0){
      let itemDetails = {
                          "SalesOrder_ItemID": this.itemsDetails['SalesOrder_ItemID'],
                          "DocumentNumber": "",
                          "Item": this.itemsDetails['Item'],
                          "Material": (this.materialMasterSelectItem.length == 0) ? '': this.materialMasterSelectItem[0]['Material_ID'],
                          "Description": this.itemsDetails['Description'],
                          "NetValue": this.itemsDetails['NetValue'],
                          "Currency": "",
                          "OrderQunatity": this.itemsDetails['OrderQunatity'],
                          "SalesOrderID": this.editOrderId,
                          "Status": 0
                        }
      
      this.spinner.show();
      this.inquiryQuote.updateOrderLineItemDetails(itemDetails,this.itemsDetails['SalesOrder_ItemID']).pipe().subscribe((data: any) => {
        this.alertService.success(Language.ALERT.Success, Language.ALERT.Saved_Message);
        this.getOrderItemListById();
      }, error => {
        this.spinner.hide()
        this.alertService.error(Language.ALERT.Title_Error, Language.ALERT.Went_Wrong_Message);
      });
    }else{

      let itemDetails = {
                          "SalesOrder_ItemID": 0,
                          "DocumentNumber": "",
                          "Item": this.itemsDetails['Item'],
                          "Material": (this.materialMasterSelectItem.length == 0) ? '': this.materialMasterSelectItem[0]['Material_ID'],
                          "Description": this.itemsDetails['Description'],
                          "NetValue": this.itemsDetails['NetValue'],
                          "Currency": "",
                          "OrderQunatity": this.itemsDetails['OrderQunatity'],
                          "SalesOrderID": this.editOrderId,
                          "Status": 0
                        }
      
      this.spinner.show();
      this.inquiryQuote.addOrderLineItemDetails(itemDetails).pipe().subscribe((data: any) => {
        this.alertService.success(Language.ALERT.Success, Language.ALERT.Saved_Message);
        this.getOrderItemListById();
      }, error => {
        this.spinner.hide()
        this.alertService.error(Language.ALERT.Title_Error, Language.ALERT.Went_Wrong_Message);
      });
    }
    this.closeModal(event);
  }

  editInquiryItem(inquiryLineItem, index) {
    document.getElementById('add-inquiry-items-modal').style.display = "block";
    document.getElementById('add-inquiry-items-modal').classList.add("show");
    document.body.classList.add("modal-open");
    this.editInquiryItemIndex = index;
    this.itemsDetails['Item'] = inquiryLineItem.Item;
    this.materialMasterSelectItem = [];
    let tmp = this.materilMasterList.find(list => list.Material_ID == inquiryLineItem.Material);
    if(tmp != undefined){
      this.materialMasterSelectItem.push(tmp);
    }
    if(inquiryLineItem.SalesOrder_ItemID != undefined && inquiryLineItem.SalesOrder_ItemID != null && inquiryLineItem.SalesOrder_ItemID != ''){
      this.itemsDetails['SalesOrder_ItemID'] = inquiryLineItem.SalesOrder_ItemID;
    }else{
      this.itemsDetails['SalesOrder_ItemID'] = 0;
    }
    this.itemsDetails['Description'] = inquiryLineItem.Description;
    this.itemsDetails['NetValue'] = inquiryLineItem.NetValue;
    this.itemsDetails['OrderQunatity'] = inquiryLineItem.OrderQunatity;
    // this.itemsDetails['NetWeight'] = inquiryLineItem.NetWeight;
    // this.itemsDetails['WeightUnit'] = inquiryLineItem.WeightUnit;
  }
  removeInquiryItem(inquiryLineItem, index){
    this.invoiceItemDetails.splice(index,1);
  }

  getSingleInquiryDetails(){
    this.spinner.show();
    this.inquiryQuote.getSingleOrderData(this.editOrderId).pipe()
      .subscribe((data: any) => {
        this.spinner.hide();
        this.inquiryDetails = data;
        this.inquiryDetailsLabel = this.label.TAB.Order_Details + '( '+this.inquiryDetails.SalesOrderID+' )';
        if(this.inquiryDetails.Status == 1 || this.inquiryDetails.Status == 'Submited'){
          this.selectedStatusItems = [ { id: 1, text: 'Submited' }];
        }else if(this.inquiryDetails.Status == 4){
          this.selectedStatusItems = [ { id: 4, text: 'Rejected' }];
        }
        else{
          this.selectedStatusItems = [ { id: 0, text: 'Draft' }];
        }
        this.selectedSoldToPartyItems = [];
        let tmp = this.soldToPartyList.find(item => item.CustomerNumber == this.inquiryDetails.SoldToParty)
        if(tmp != undefined){
          this.selectedSoldToPartyItems.push(tmp);
          this.getCustomerDetails(tmp.CustomerNumber, 'Set-Value');
        }
        this.inquiryDetails.CreatedDate = new Date(this.inquiryDetails.CreatedOn);
        // this.invoiceItemDetails = this.inquiryDetails.SalesInquiryDetails;
        this.getOrderItemListById();
        this.getInvoiceDocumentDetails();
        
        console.log(this.invoiceItemDetails, 'get')
      }, error => {
        this.spinner.hide()
        this.alertService.error(Language.ALERT.Title_Error, Language.ALERT.Went_Wrong_Message);
        // this.util.PrintLogs(varConstants.ERROR, this.className, this.getSingleInquiryDetails.name, error);
      });
  }

  exportExcelData(){
    this.orderExportData = [];
    this.orderExportData = [
      {
        SoldToParty: this.label.Sold_To_Party,
        ShipToParty: this.label.Ship_to_Party,
        SalesOrg: this.label.Sales_Organization,
        DistributionChannel: this.label.Distribution_Channel,
        Division: this.label.Division,
        CreatedBy: this.label.Created_by,
        CreatedOn: this.label.Created_on,
        ValidFrom: this.label.Valid_from,
        ValidTo: this.label.Valid_to,
        DocumentDate: this.label.Document_Dat,
        NetValue: this.label.Net_value,
        Currency: this.label.SD_Doc_Currency,
        RequestedDeliver: this.label.Requested_Deliver,
        SalesDocument: this.label.Sales_Document,
        Status: this.label.Status,
        ItemNo: this.label.Sales_Document_Item,
        MaterialNumber: this.label.Material_Number,
        Description: this.label.Description,
        NetValueCurrency: this.label.Net_Value_Of_Item_Currency,
        Quantity: this.label.Order_Quantity,
      }
    ];

    let exportObj = {
                        SoldToParty: (this.selectedSoldToPartyItems.length > 0) ? this.selectedSoldToPartyItems[0]['CustomerName']: '--',
                        ShipToParty: (this.selectedShipToPartyItems.length > 0) ? this.selectedShipToPartyItems[0]['CustomerName']: '--',
                        SalesOrg: (this.selectedSalesOrgItem.length > 0) ? this.selectedSalesOrgItem[0]['SalesOrg']: '--',
                        DistributionChannel: (this.inquiryDetails.DistributionChannel != '') ? this.inquiryDetails.DistributionChannel : '--' ,
                        Division: (this.inquiryDetails.Division != '') ? this.inquiryDetails.Division : '--',
                        CreatedBy: (this.inquiryDetails.CreatedBy != '') ? this.inquiryDetails.CreatedBy : '--',
                        CreatedOn: (this.inquiryDetails.CreatedDate != '') ? this.inquiryDetails.CreatedDate : '--',
                        ValidFrom: (this.inquiryDetails.ValidFrom != '') ? this.inquiryDetails.ValidFrom : '--',
                        ValidTo: (this.inquiryDetails.ValidTo != '') ? this.inquiryDetails.ValidTo : '--',
                        DocumentDate: (this.inquiryDetails.DocumentDate != '') ? this.inquiryDetails.DocumentDate : '--',
                        NetValue: (this.inquiryDetails.NetValue != '') ? this.inquiryDetails.NetValue : 0,
                        Currency: (this.selectedSdDocCurrencyItems.length > 0) ? this.selectedSdDocCurrencyItems[0]['text']: '--',
                        RequestedDeliver: (this.inquiryDetails.ReqDeliveryDate != '') ? this.inquiryDetails.ReqDeliveryDate : '--',
                        SalesDocument: (this.inquiryDetails.DocumentNumber != '') ? this.inquiryDetails.DocumentNumber : '--',
                        Status: (this.selectedStatusItems.length > 0) ? this.selectedStatusItems[0]['text']: '--',
                        ItemNo: '--',
                        MaterialNumber: '--',
                        Description: '--',
                        NetValueCurrency: '--',
                        Quantity: '--',
                      }
    
    if(this.invoiceItemDetails.length == 0){
      this.orderExportData.push(exportObj);
    }else{
      for (let i = 0; i < this.invoiceItemDetails.length; i++) {
         let exportObj = {
                        SoldToParty: (this.selectedSoldToPartyItems.length > 0) ? this.selectedSoldToPartyItems[0]['CustomerName']: '--',
                        ShipToParty: (this.selectedShipToPartyItems.length > 0) ? this.selectedShipToPartyItems[0]['CustomerName']: '--',
                        SalesOrg: (this.selectedSalesOrgItem.length > 0) ? this.selectedSalesOrgItem[0]['SalesOrg']: '--',
                        DistributionChannel: (this.inquiryDetails.DistributionChannel != '') ? this.inquiryDetails.DistributionChannel : '--' ,
                        Division: (this.inquiryDetails.Division != '') ? this.inquiryDetails.Division : '--',
                        CreatedBy: (this.inquiryDetails.CreatedBy != '') ? this.inquiryDetails.CreatedBy : '--',
                        CreatedOn: (this.inquiryDetails.CreatedDate != '') ? this.inquiryDetails.CreatedDate : '--',
                        ValidFrom: (this.inquiryDetails.ValidFrom != '') ? this.inquiryDetails.ValidFrom : '--',
                        ValidTo: (this.inquiryDetails.ValidTo != '') ? this.inquiryDetails.ValidTo : '--',
                        DocumentDate: (this.inquiryDetails.DocumentDate != '') ? this.inquiryDetails.DocumentDate : '--',
                        NetValue: (this.inquiryDetails.NetValue != '') ? this.inquiryDetails.NetValue : 0,
                        Currency: (this.selectedSdDocCurrencyItems.length > 0) ? this.selectedSdDocCurrencyItems[0]['text']: '--',
                        RequestedDeliver: (this.inquiryDetails.ReqDeliveryDate != '') ? this.inquiryDetails.ReqDeliveryDate : '--',
                        SalesDocument: (this.inquiryDetails.DocumentNumber != '') ? this.inquiryDetails.DocumentNumber : '--',
                        Status: (this.selectedStatusItems.length > 0) ? this.selectedStatusItems[0]['text']: '--',
                        ItemNo: (this.invoiceItemDetails[i].Item != '') ? this.invoiceItemDetails[i].Item: '--',
                        MaterialNumber: (this.invoiceItemDetails[i].Material != '') ? this.invoiceItemDetails[i].Material: '--',
                        Description: (this.invoiceItemDetails[i].Description != '') ? this.invoiceItemDetails[i].Description: '--',
                        NetValueCurrency: (this.invoiceItemDetails[i].NetValue != '') ? this.invoiceItemDetails[i].NetValue: '--',
                        Quantity: (this.invoiceItemDetails[i].OrderQunatity != '') ? this.invoiceItemDetails[i].OrderQunatity: '--',
                      }
    
                      this.orderExportData.push(exportObj);
      }
    }                
  
    
    
    console.log(this.orderExportData, 'order export')
  }

  getOrderItemListById(){
    this.spinner.show();
    // let param = {
    //   SalesOrderID: this.inquiryDetails.SalesOrderID
    // }
    this.inquiryQuote.getOrderItemListById(this.inquiryDetails.SalesOrderID).pipe()
      .subscribe((data: any) => {
        this.spinner.hide();
        this.invoiceItemDetails = data;
        console.log(this.invoiceItemDetails, 'get')
        this.exportExcelData();
      }, error => {
        this.spinner.hide()
        this.alertService.error(Language.ALERT.Title_Error, Language.ALERT.Went_Wrong_Message);
        // this.util.PrintLogs(varConstants.ERROR, this.className, this.getSingleInquiryDetails.name, error);
      });
  }

  getCustomerMasterList(){
    this.spinner.show();
    this.inquiryQuote.getCustomerMasterList().pipe()
      .subscribe((data: any) => {
        this.soldToPartyList =  data
        this.soldToPartyList.map((index) => {
          index.CustomerName = index.CustomerNumber+'-'+ index.CustomerName;
        });
        this.spinner.hide();
        if(this.editOrderId != null && this.editOrderId != ''){
          this.viewModeField = true;
          this.getSingleInquiryDetails();
        }
      }, error => {
        this.spinner.hide();
        this.alertService.error(Language.ALERT.Title_Error, Language.ALERT.Went_Wrong_Message);
        // this.util.PrintLogs(varConstants.ERROR, this.className, this.getSingleInquiryDetails.name, error);
      });
  }

  getMaterialMasterList(){
    this.inquiryQuote.getMaterialMasterList().pipe()
      .subscribe((data: any) => {
        this.materilMasterList =  data
      }, error => {
        this.alertService.error(Language.ALERT.Title_Error, Language.ALERT.Went_Wrong_Message);
        // this.util.PrintLogs(varConstants.ERROR, this.className, this.getSingleInquiryDetails.name, error);
      });
  }

  selectSoldToParty(item: any){
    console.log(item, 'sold')
    this.getCustomerDetails(item.CustomerNumber);
    this.deSelectSoldToParty();
  }

  selectShipToParty(item: any){
    
  }

  deSelectSoldToParty(){
    this.shipToPartyList = [];
    this.selectedShipToPartyItems = [];
    this.salesOrganizationList = [];
    this.selectedSalesOrgItem = [];
    this.inquiryDetails.Division = '';
    this.inquiryDetails.DistributionChannel = '';
  }

  selectSalesOrg(item: any){
    let tmp = this.salesOrganizationList.find(list => list.SalesOrg == item.SalesOrg);
    console.log(item)
    if(tmp != undefined){
      this.inquiryDetails.Division = tmp.Division;
      this.inquiryDetails.DistributionChannel = tmp.DistributionChannel;
    }
  }

  deSelectSalesOrg(item: any){
    this.inquiryDetails.Division = '';
    this.inquiryDetails.DistributionChannel = '';
  }

  getCustomerDetails(customerId, setValue = ''){
    let param = {
      customerId: customerId
    }
    this.spinner.show();
    this.inquiryQuote.getCustomerDetails(param).pipe()
      .subscribe((data: any) => {
       
        console.log(data, 'customer')
        this.shipToPartyList = data.PartnerData;
        this.shipToPartyList.map((index) => {
          index.CustomerName = index.BusinessPartnerCustomerNo+'-'+ index.CustomerName;
        });
        this.salesOrganizationList = data.CustomerSalesOrg;
        if(setValue == 'Set-Value'){
          let tmp = this.shipToPartyList.find(item => item.BusinessPartnerCustomerNo == this.inquiryDetails.ShipToParty);
          console.log(tmp, 'ssss', this.shipToPartyList, this.inquiryDetails.ShipToParty, this.selectedSoldToPartyItems)
          if(tmp != 'undefined'){
            this.selectedShipToPartyItems = [];
            this.selectedShipToPartyItems.push(tmp);
          }

          let tmp1 = this.salesOrganizationList.find(item => item.SalesOrg == this.inquiryDetails.SalesOrg);
          if(tmp1 != 'undefined'){
            this.selectedSalesOrgItem = [];
            this.selectedSalesOrgItem.push(tmp1);
          }
        }
        this.spinner.hide()
      }, error => {
        this.spinner.hide()
        this.alertService.error(Language.ALERT.Title_Error, Language.ALERT.Went_Wrong_Message);
      });
  }

  enableForm() {
    this.viewModeField = false;
    this.enableEditEnquiryForm = true;
  }

  ngOnDestroy(){
    localStorage.removeItem('editOrderId');
  }
  onSelectAll(items: any){
      console.log(items);
  }
  onDeSelectAll(items: any){
      console.log(items);
  }

  setNetValue(item: any){
    let charCode = (item.which) ? item.which : item.keyCode;
    if(charCode == 8 || charCode == 46){
      return true;
    }
    if (charCode != 190 && charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57)){
      console.log('first')
      return false;
    }
    if(this.inquiryDetails.NetValue.toLocaleString().split('.').length == 2 && charCode == 190){
      return false;
    }
    if(this.inquiryDetails.NetValue.toLocaleString().length > 15 || this.inquiryDetails.NetValue.toLocaleString().split('.').length > 2){
     return false;
    }
    console.log(this.inquiryDetails.NetValue.toLocaleString().split('.').length, 'item', charCode)
    return true;
  }

  numericValue(item: any){
    console.log(item, 'item', this.inquiryDetails.NetValue)
    let charCode = (item.which) ? item.which : item.keyCode;
    if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57)){
      console.log('first')
      return false;
    }
    // if(this.inquiryDetails.NetValue.toLocaleString().length > 15 || this.inquiryDetails.NetValue.toLocaleString().split('.').length > 1){
    //   return false;

    // }
    return true;
  }

  cancelEnquiryDetails(){
    if(this.editOrderId != '' && this.editOrderId != null && this.editOrderId != undefined){
      this.getSingleInquiryDetails();
    }else{
      this.inquiryDetails = {};
      this.selectedSoldToPartyItems = [];
      this.selectedShipToPartyItems = [];
      this.selectedSalesOrgItem = [];
      // this.selectedSdDocCurrencyItems = []
    }
  }

  saveEnquiryDetails(enquiryForm = null){
    console.log(enquiryForm, 'form',this.selectedShipToPartyItems)
    if(enquiryForm && enquiryForm.form.status == 'INVALID'){
      return false;
    }
    this.formSaveErrorMsg = '';
    // if(this.invoiceItemDetails.length == 0){
    //   this.formSaveErrorMsg = 'Minium one Line-Item is required';
    //   return false;
    // }
    let loginUserDetails = localStorage.getItem('logged_in_user_details');
    loginUserDetails = JSON.parse(loginUserDetails);
    if(this.editOrderId != '' && this.editOrderId != null && this.editOrderId != undefined){
      let param =  {
            "SalesOrderID": this.editOrderId,
            "UniversalClientId": 0,
            "CompanyCode": "String",
            "Client": "String",
            "DocumentNumber": (this.inquiryDetails.DocumentNumber == undefined) ? '': this.inquiryDetails.DocumentNumber,
            "OrderType": "",
            "SalesOrg": (this.selectedSalesOrgItem.length > 0) ? this.selectedSalesOrgItem[0]['SalesOrg']: '',
            "DistributionChannel": this.inquiryDetails.DistributionChannel,
            "Division": this.inquiryDetails.Division,
            "CreatedBy": this.inquiryDetails.Division,
            "CreatedDate": this.pipe.transform(this.inquiryDetails.CreatedDate,'yyyy-MM-dd'),
            "DocumentDate": this.pipe.transform(this.inquiryDetails.DocumentDate,'yyyy-MM-dd'),
            "NetValue": this.inquiryDetails.NetValue,
            "DocumentCurrency": (this.selectedSdDocCurrencyItems.length > 0) ? this.selectedSdDocCurrencyItems[0]['id']: '',
            "ValidFrom": this.pipe.transform(this.inquiryDetails.ValidFrom,'yyyy-MM-dd'),
            "ValidTo": this.pipe.transform(this.inquiryDetails.ValidTo, 'yyyy-MM-dd'),
            "ReqDeliveryDate": this.pipe.transform(this.inquiryDetails.ReqDeliveryDate,'yyyy-MM-dd'),
            "ShippingConditions": "",
            "PONo": "",
            "PODate": "",
            "SoldToParty": (this.selectedSoldToPartyItems.length > 0) ? this.selectedSoldToPartyItems[0]['CustomerNumber']: '',
            "PartnerFunction": "",
            "ShipToParty": (this.selectedShipToPartyItems.length > 0) ? this.selectedShipToPartyItems[0]['BusinessPartnerCustomerNo']: '',
            "RecordCreatedDate": this.pipe.transform(this.inquiryDetails.CreatedDate, 'yyyy-MM-dd'),
            "InquiryValidFrom": this.pipe.transform(this.inquiryDetails.ValidFrom, 'yyyy-MM-dd'),
            "Status": (this.selectedStatusItems.length > 0) ? this.selectedStatusItems[0]['id']: 0,
            "UpdatedDate": this.pipe.transform(this.currentDate,'yyyy-MM-dd'),
            "UpdatedBy": loginUserDetails['Id']
          }
      let parameter = {}
      parameter['salesInquiryHeader'] = param;
      // parameter['SalesOrderID'] = this.editOrderId;
      this.spinner.show();
      this.inquiryQuote.updateOrderHeaderDetails(param,this.editOrderId).pipe().subscribe((data: any) => {
        console.log('Response');
        this.viewModeField = true;
        this.spinner.hide();
        if(param.Status == 1 || param.Status == 4){
          this.router.navigate(['/orders/submitted']);
        }
        if(param.Status == 2){
          this.router.navigate(['/orders/open']);
        }
        this.enableEditEnquiryForm = false;
        // this.inquiryDetails = data;
        // this.inquiryDetailsLabel = this.label.TAB.Order_Details + '( '+this.inquiryDetails.SalesOrderID+' )';
        // this.addLineItemDetails(lineItems[0]);
        // this.editOrderId = this.inquiryDetails.SalesOrderID;
      }, error => {
        this.spinner.hide()
        this.alertService.error(Language.ALERT.Title_Error, Language.ALERT.Went_Wrong_Message);
      });
    }else{
      let parameter = {
          "Id": 0,
          "UniversalClientId": 0,
          "CompanyCode": "",
          "Client": "",
          "DocumentNumber": (this.inquiryDetails.DocumentNumber == undefined) ? '': this.inquiryDetails.DocumentNumber,
          "OrderType": "",
          "SalesOrg": (this.selectedSalesOrgItem.length > 0) ? this.selectedSalesOrgItem[0]['SalesOrg']: '',
          "DistributionChannel": this.inquiryDetails.DistributionChannel,
          "Division": this.inquiryDetails.Division,
          "CreatedBy": loginUserDetails['FirstName']+' '+loginUserDetails['LastName'],
          "CreatedDate": this.pipe.transform(this.inquiryDetails.CreatedDate, 'yyyy-MM-dd'),
          "DocumentDate": this.pipe.transform(this.inquiryDetails.DocumentDate,'yyyy-MM-dd'),
          "NetValue": this.inquiryDetails.NetValue,
          "DocumentCurrency": (this.selectedSdDocCurrencyItems.length > 0) ? this.selectedSdDocCurrencyItems[0]['item_id']: '',
          "ValidFrom": this.pipe.transform(this.inquiryDetails.ValidFrom,'yyyy-MM-dd'),
          "ValidTo": this.pipe.transform(this.inquiryDetails.ValidTo, 'yyyy-MM-dd'),
          "ReqDeliveryDate": this.pipe.transform(this.inquiryDetails.ReqDeliveryDate,'yyyy-MM-dd'),
          "ShippingConditions": "",
          "PONo": "",
          "PODate": "",
          "SoldToParty": (this.selectedSoldToPartyItems.length > 0) ? this.selectedSoldToPartyItems[0]['CustomerNumber']: '',
          "PartnerFunction": "",
          "ShipToParty": (this.selectedShipToPartyItems.length > 0) ? this.selectedShipToPartyItems[0]['BusinessPartnerCustomerNo']: '',
          "RecordCreatedDate": this.pipe.transform(this.inquiryDetails.CreatedDate, 'yyyy-MM-dd'),
          "InquiryValidFrom": this.pipe.transform(this.inquiryDetails.ValidFrom, 'yyyy-MM-dd'),
          "Status": (this.selectedStatusItems.length > 0) ? this.selectedStatusItems[0]['id']: 0,
          "UpdatedDate": "",
          "UpdatedBy": ""
      }
      this.spinner.show();
      this.inquiryQuote.addOrderHeaderDetails(parameter).pipe().subscribe((data: any) => {
        this.inquiryDetails = data;
        this.inquiryDetailsLabel = this.label.TAB.Order_Details + '( '+this.inquiryDetails.SalesOrderID+' )';
        // this.addLineItemDetails(lineItems[0]);
        this.editOrderId = this.inquiryDetails.SalesOrderID;
        this.inquiryDetails.CreatedDate = new Date(this.inquiryDetails.CreatedOn);
        this.spinner.hide()
      }, error => {
        this.spinner.hide()
        this.alertService.error(Language.ALERT.Title_Error, Language.ALERT.Went_Wrong_Message);
      });
    }
     
  }

  selectMaterialItem(item: any){
    console.log(item)
    let tmp = this.materilMasterList.find(list=> list.Material_ID == item.Material_ID);
    if(tmp != undefined){
      this.itemsDetails.Description = tmp.MaterialDescription;
    }
  }

  deleteEnquiryItemDetails(itemId){
    let param = {
       id: itemId
    }
    this.inquiryQuote.deleteOrderItemDetails(param).pipe().subscribe((data: any) => {
      this.getSingleInquiryDetails();
      this.alertService.success(Language.ALERT.Success, Language.ALERT.Delete_Message);
    }, error => {
      this.spinner.hide()
      this.alertService.error(Language.ALERT.Title_Error, Language.ALERT.Went_Wrong_Message);
    });
  }

  submitEnquiryDetails(enquiryForm, status = ''){
    //file management
    if(enquiryForm && enquiryForm.form.status == 'INVALID'){
      return false;
    }
    console.log('form submited')
    this.selectedStatusItems = [ { id: 1, text: 'Submited' }];
    if(status == 'accept'){
      this.selectedStatusItems = [ { id: 2, text: 'Open' }];
    }
    if(status == 'reject'){
      this.selectedStatusItems = [ { id: 4, text: 'Rejected' }];
    }
    this.saveEnquiryDetails(enquiryForm);
  }

  selectFile(event): void {
    this.selectedFile = event.target.files.item(0);
  }

  addDcoumentDetails(event){
    this.inquiryDocumentForm.submitted = true;
    console.log(this.invoiceItemSelectedItem, 'line item')
    if(!this.selectedFile || this.invoiceItemSelectedItem.length == 0 || !this.documentDetails.DocRefNo || !this.documentDetails.DocumentName || !this.documentDetails.Doctype){
      return false;
    }
    const formData: FormData = new FormData();
    formData.append('FileData', this.selectedFile);
    let lineItem = (this.invoiceItemSelectedItem.length > 0) ? this.invoiceItemSelectedItem[0]['SalesOrder_ItemID']: '';
    let param = 'LineItem='+lineItem+'&DocRefNo='+this.documentDetails.DocRefNo+'&DocumentName='+this.documentDetails.DocumentName+'&Doctype='+this.documentDetails.Doctype+'&DocumentID='+this.editOrderId;
    this.spinner.show();
    this.inquiryQuote.addOrderDocumentDetails(formData, param).pipe().subscribe((data: any) => {
      this.alertService.success(Language.ALERT.Success, Language.ALERT.Saved_Message);
      this.getInvoiceDocumentDetails();
    }, error => {
      this.spinner.hide()
      this.alertService.error(Language.ALERT.Title_Error, Language.ALERT.Went_Wrong_Message);
    });
    this.closeModal(event);
  }

  getInvoiceDocumentDetails(){
    this.spinner.show();
    let param = {
        DocumentID: this.editOrderId
    }
    this.inquiryQuote.getSalesOrderDocumentById(param).pipe()
    .subscribe((data: any) => {
      this.spinner.hide();
      this.invoiceDocumentDetails = data;
      console.log(this.invoiceDocumentDetails, 'get')
      this.exportExcelData();
    }, error => {
      this.spinner.hide()
      this.alertService.error(Language.ALERT.Title_Error, Language.ALERT.Went_Wrong_Message);
    });
  }

  editDocumentItem(item, index) {
    document.getElementById('add-document-items-modal').style.display = "block";
    document.getElementById('add-document-items-modal').classList.add("show");
    document.body.classList.add("modal-open");
    this.editDocumentItemIndex = index;
    this.documentDetails = {};
    console.log(item, index, 'okkk')
    this.documentDetails['documentName'] = item.documentName;
  }

  documentDownload(docName){
    this.spinner.show();
    let param = {
      AttachmentName: docName
    }
    this.inquiryQuote.documentDownload(param).pipe()
      .subscribe((data: any) => {
        if (data) {
          var blob = new Blob([data], { type: 'image/png' });
          download(blob, docName);
        }else {
          this.alertService.error(Language.ALERT.Title_Error, Language.ALERT.Went_Wrong_Message);
        }
        this.spinner.hide()
      }, error => {
        this.spinner.hide()
        this.alertService.error(Language.ALERT.Title_Error, Language.ALERT.Went_Wrong_Message);
        // this.util.PrintLogs(varConstants.ERROR, this.className, this.getSingledeliveryDetailsname, error);
      });
  }
}
