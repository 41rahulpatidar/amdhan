import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NewRoutingModule } from './new-routing.module';
import { PackagedModule } from '../../../../app/Packaged/packaged.module';
import { NewComponent } from './new.component';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';

@NgModule({
  declarations: [NewComponent],
  imports: [
    CommonModule,
    PackagedModule,
    NewRoutingModule,
    OwlDateTimeModule, 
   	OwlNativeDateTimeModule
  ]
})
export class NewModule { }
