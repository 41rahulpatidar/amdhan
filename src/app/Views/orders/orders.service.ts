import { Injectable } from '@angular/core';
import { RestApiService } from 'src/app/Service/rest-api.service';
import { apiUrl } from 'src/app/Other/apiUrl.constants';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class OrdersService {

  constructor(private restService: RestApiService) { }

  getOrdersHeaderData(param = null): Observable<any> {
    return this.restService.get(apiUrl.SALES_ORDERS_DATA, param);
  }

  getOrdersExportData(param = null): Observable<any> {
    return this.restService.get(apiUrl.EXPORT_UTILITY, param, 'blob');
  }

  getCustomerMasterList(): Observable<any> {
    return this.restService.get(apiUrl.CUSTOMER_MASTER);
  }

  updateOrderLineItemDetails(request,editId): Observable<any> {
    return this.restService.put(apiUrl.SALES_ORDER_ITEM_DATA+'/'+editId, request);
  }
  
  addOrderLineItemDetails(request): Observable<any> {
    return this.restService.post(apiUrl.SALES_ORDER_ITEM_DATA, request);
  }

  getSingleOrderData(orderId): Observable<any> {
    return this.restService.get(apiUrl.SALES_ORDERS_DATA+'/'+orderId);
  }

  getOrderItemListById(orderId): Observable<any> {
    return this.restService.get(apiUrl.GET_ORDER_ITEM_DATA_BY_SALES_ORDER_ID+'/'+orderId);
  }

  updateOrderHeaderDetails(request, editId): Observable<any> {
    return this.restService.put(apiUrl.SALES_ORDERS_DATA+'/'+editId, request);
  }

  addOrderHeaderDetails(request): Observable<any> {
    return this.restService.post(apiUrl.SALES_ORDERS_DATA, request);
  }

  deleteOrderItemDetails(request): Observable<any> {
    return this.restService.delete(apiUrl.SALES_ORDER_ITEM_DATA, request);
  }

  getMaterialMasterList(): Observable<any> {
    return this.restService.get(apiUrl.MATERIAL_MASTER_LIST);
  }

  getCustomerDetails(request): Observable<any> {
    return this.restService.get(apiUrl.CUSTOMER_DETAILS, request);
  }

  getSalesOrderDocumentById(request): Observable<any> {
    return this.restService.get(apiUrl.GET_INQUIRY_DOCUMENT, request);
  }

  addOrderDocumentDetails(request, param): Observable<any> {
    return this.restService.post(apiUrl.ADD_INQUIRY_DOCUMENT+'?'+param, request);
  }

  documentDownload(request): Observable<any> {
    return this.restService.get(apiUrl.DOCUMENT_DOWNLOAD, request,'blob');
  }
}
