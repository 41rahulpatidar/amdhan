import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SubmittedComponent } from './submitted.component';

const routes: Routes = [
  { path: "", component: SubmittedComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SubmittedRoutingModule { }
