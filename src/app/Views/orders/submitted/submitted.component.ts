import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { Language } from '../../../Other/translation.constants';
import { NgxSpinnerService } from 'ngx-spinner';
import { UtilService } from 'src/app/Service/util.service';
import { varConstants } from 'src/app/Other/variable.constants';
import { AlertService } from 'src/app/Service/alert.service';
import { Router } from '@angular/router';
import { map, tap } from 'rxjs/operators';
import {OrdersService} from '../orders.service'
import {DatePipe} from '@angular/common';
import { saveAs as download } from "file-saver";

@Component({
  selector: 'app-submitted',
  templateUrl: './submitted.component.html',
  styleUrls: ['./submitted.component.scss']
})
export class SubmittedComponent implements OnInit {

  label:any = Language;
  dtOptions: any;
  userData: any;
  submittedOrdersArray:any;
  className = SubmittedComponent.name;
  isMenuAccess: boolean = true;
  isAddEditPermission: boolean = false;
  orderFilter: any = {
    FromDate: new Date(),
    ToDate: new Date(),
    Keyword: ''
  }
  pipe = new DatePipe('en-US');
  datatable: any;

  constructor( 
    private router: Router,
    private spinner: NgxSpinnerService,
    private util: UtilService,
    private alertService: AlertService,
    private OrderService:OrdersService,
    private chRef: ChangeDetectorRef) {
      let currentDate = new Date();
      let previousDate = currentDate.setFullYear(currentDate.getFullYear() - 1);
      this.orderFilter.FromDate = new Date(previousDate);
      this.dtOptions = this.util.dtOptions;
    }

    ngOnInit() {
      localStorage.setItem('previousOrderSection', 'submitted');
      this.userData = JSON.parse(localStorage.getItem("logged_in_user_details"));

      //**** User Menu Access Start ****//
      if(this.isMenuAccess == false){
        this.router.navigate(["/dashboard"]);
      }
      if(this.userData.RoleType == '2'){
        this.isAddEditPermission = true;
      }
      // if(this.userData.RoleType == '3'){
      //   if(this.userData.Lst_RoleData1[0].ApprovalLevel == 'L01'){
      //     this.isAddEditPermission = false;
      //   }else{
      //     this.isAddEditPermission = true;
      //   }
      // }
    //**** User Menu Access End ****//

      this.util.tableHorizontalScroll('open_orders_table');
      this.getOrdersHeaderData();
    }
    getOrdersHeaderData(){
      let param = {
        FromDate: this.pipe.transform(this.orderFilter.FromDate,'yyyy-MM-dd'),
        ToDate: this.pipe.transform(this.orderFilter.ToDate,'yyyy-MM-dd'),
        Keyword: this.orderFilter.Keyword
      }
      this.OrderService.getOrdersHeaderData(param)
        .pipe(
          tap(data => { 
            this.util.PrintLogs(varConstants.INFO,this.className,this.getOrdersHeaderData.name,data);  
          }),
          map(data => {
            let newData;
            // If vendor ID is not null it will filter the data vendor accordingly
            // if (this.userData.RoleType == varConstants.ThirdPartyRoleType){
            //   if(vendorID){
            //     data = data.filter(result => result.Vendor == vendorID)  
            //   //data = data;
            //   }
            // }
  
            newData = data.filter(result => (result.Status == varConstants.isSubmitted || result.Status == varConstants.isRejected))
  
            for (const property in newData) {
              switch (newData[property]["Status"]) {
                case varConstants.isNew:
                  newData[property]["StatusToDisplay"] = Language.New;
                  break;
                case varConstants.isClosed:
                  newData[property]["StatusToDisplay"] = Language.Closed;
                  break;
                case varConstants.isOpen:
                  newData[property]["StatusToDisplay"] = Language.Open;
                  break;
                case varConstants.isSubmitted:
                  newData[property]["StatusToDisplay"] = Language.Submitted;
                  break;
                case varConstants.isRejected:
                  newData[property]["StatusToDisplay"] = Language.Rejected;
                  break;
                default:
                  newData[property]["StatusToDisplay"] = newData[property]["Status"];
              }
            }
            return newData;
          }),
        )
        .subscribe((data: any) => {
          if (Array.isArray(data)) {
            this.util.PrintLogs(varConstants.INFO, this.className, this.getOrdersHeaderData.name, data);
            // console.log(data);
            if ($.fn.dataTable.isDataTable('#submitted_orders_table')) {
              $('#submitted_orders_table').DataTable().clear().destroy();               
            }
            this.submittedOrdersArray = data;
            this.chRef.detectChanges();
            const table: any = $('table');
            this.datatable = table.DataTable();
            this.spinner.hide();
          }
        }, error => {
          this.spinner.hide()
          this.alertService.error(Language.ALERT.Title_Error, Language.ALERT.Went_Wrong_Message);
          this.util.PrintLogs(varConstants.ERROR, this.className, this.getOrdersHeaderData.name, error);
        });
    }

    getOrderExportData(){
    this.spinner.show();
      let param = {
        FromDate: this.pipe.transform(this.orderFilter.FromDate,'yyyy-MM-dd'),
        ToDate: this.pipe.transform(this.orderFilter.ToDate,'yyyy-MM-dd'),
        Keyword: this.orderFilter.Keyword,
        Status: 1,
        DocumentType: 'Order'
      }
  
    this.OrderService.getOrdersExportData(param).subscribe((data: any) => {
      this.spinner.hide();
      if (data) {
        var blob = new Blob([data], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8' });
        download(blob, 'Order_Header_Export' + new  Date().getTime() + '.xlsx');
      }
      else {
        this.alertService.error(Language.ALERT.Title_Error, Language.ALERT.Went_Wrong_Message);
      }
    }, error => {
      console.log(error, 'error')
      this.spinner.hide();
      this.alertService.error(Language.ALERT.Title_Error, Language.ALERT.Went_Wrong_Message);
   });
  }
  
    viewOrderDetails(order) {
      localStorage.setItem('editOrderId', order.SalesOrderID);
      order.from = "/orders/submitted";
      order.tab = this.label.SUB_TAB.Orders;
      order.mode = varConstants.isRegistered;
      this.router.navigate(["/orders/order-details"], { state: { data: order } });
    }
}
