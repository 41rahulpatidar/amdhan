import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PackagedModule } from 'src/app/Packaged/packaged.module';
import { SubmittedRoutingModule } from './submitted-routing.module';
import { SubmittedComponent } from './submitted.component';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';

@NgModule({
  declarations: [SubmittedComponent],
  imports: [
    CommonModule,
    PackagedModule,
    SubmittedRoutingModule,
    OwlDateTimeModule, 
   	OwlNativeDateTimeModule
  ]
})
export class SubmittedModule { }
