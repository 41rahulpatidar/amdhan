import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { Language } from 'src/app/Other/translation.constants';

const routes: Routes = [{
  path: '',
  data: {
    title: Language.TAB.Admin_Section,
    status: false
  },
  children: [
    {
      path: '',
      redirectTo: 'orders',
      pathMatch: 'full'
    },
    {
      path: 'open',
      loadChildren: () => import('./open/open.module').then(m => m.OpenModule)
    },
    {
      path: 'new',
      loadChildren: () => import('./new/new.module').then(m => m.NewModule)
    },
    {
      path: 'submitted',
      loadChildren: () => import('./submitted/submitted.module').then(m => m.SubmittedModule)
    },
    {
      path: 'closed',
      loadChildren: () => import('./closed/closed.module').then(m => m.ClosedModule)
    },
    {
      path: 'all',
      loadChildren: () => import('./all/all.module').then(m => m.AllModule)
    },
    {
      path: 'order-details',
      loadChildren: () => import('./order-details/order-details.module').then(m => m.OrderDetailsModule)
    },

  ]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class OrdersRoutingModule { }
