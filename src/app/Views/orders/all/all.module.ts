import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AllRoutingModule } from './all-routing.module';
import { AllComponent } from './all.component';
import { PackagedModule } from 'src/app/Packaged/packaged.module';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';

@NgModule({
  declarations: [AllComponent],
  imports: [
    CommonModule,
    AllRoutingModule,
    PackagedModule,
    OwlDateTimeModule, 
   	OwlNativeDateTimeModule
  ]
})
export class AllModule { }
