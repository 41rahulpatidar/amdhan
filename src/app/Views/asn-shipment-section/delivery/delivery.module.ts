import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DeliveryRoutingModule } from './delivery-routing.module';
import { PackagedModule } from '../../../../app/Packaged/packaged.module';
import { DeliveryComponent } from './delivery.component';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';

@NgModule({
  declarations: [DeliveryComponent],
  imports: [
    CommonModule,
    PackagedModule,
    DeliveryRoutingModule,
    OwlDateTimeModule, 
    OwlNativeDateTimeModule
  ]
})
export class DeliveryModule { }
