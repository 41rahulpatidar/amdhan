import { Component, OnInit,ChangeDetectorRef  } from '@angular/core';
import { Language } from '../../../Other/translation.constants';
import { NgxSpinnerService } from 'ngx-spinner';
import { UtilService } from 'src/app/Service/util.service';
import { varConstants } from 'src/app/Other/variable.constants';
import { AsnShipmentService } from '../asn-shipment.service';
import { AlertService } from 'src/app/Service/alert.service';
import { Router } from '@angular/router';
import { map, tap } from 'rxjs/operators';
import {DatePipe} from '@angular/common';
import { saveAs as download } from "file-saver";

@Component({
  selector: 'app-delivery',
  templateUrl: './delivery.component.html',
  styleUrls: ['./delivery.component.scss']
})
export class DeliveryComponent implements OnInit {

	label: any = Language;
  deliveryArray: any = [{fieldname: "demo"}];
  dtOptions: any;
  userData: any;
  currentSection: any;
  headerVisible: boolean = false;
  datatable: any;
  isMenuAccess: boolean = true;
  isAddEditPermission: boolean = true;
  inquiryFilter: any = {
    FromDate: new Date(),
    ToDate: new Date(),
    Keyword: ''
  }
  pipe = new DatePipe('en-US');

  constructor(
    private router: Router,
    private spinner: NgxSpinnerService,
    private util: UtilService,
    private asnShipmentService: AsnShipmentService,
    private alertService: AlertService,
    private chRef: ChangeDetectorRef) { 
    this.dtOptions = this.util.dtOptions;
  }

  ngOnInit() {
    let currentDate = new Date();
    let previousDate = currentDate.setFullYear(currentDate.getFullYear() - 1);
    this.inquiryFilter.FromDate = new Date(previousDate);
    
    this.userData = JSON.parse(localStorage.getItem("logged_in_user_details"));
    this.currentSection = JSON.parse(localStorage.getItem("current_section"));
    this.util.tableHorizontalScroll('delivery_header_table');
    this.getDeliveryHeaderData();

    //**** User Menu Access Start ****//
    if(this.isMenuAccess == false){
      this.router.navigate(["/dashboard"]);
    }
    if(this.userData.RoleType == '2'){
      this.isAddEditPermission = true;
    }
    if(this.userData.RoleType == '3'){
      if(this.userData.Lst_RoleData1[0].ApprovalLevel == 'L01'){
        this.isAddEditPermission = false;
      }else{
        this.isAddEditPermission = false;
      }
    }

    if(this.headerVisible == true){
      this.headerVisible = this.isAddEditPermission;
    }
    //**** User Menu Access End ****//

  }

  getDeliveryHeaderData() {
      this.spinner.show();
      let param = {
        FromDate: this.pipe.transform(this.inquiryFilter.FromDate,'yyyy-MM-dd'),
        ToDate: this.pipe.transform(this.inquiryFilter.ToDate,'yyyy-MM-dd'),
        Keyword: this.inquiryFilter.Keyword
      }
      this.asnShipmentService.getDeliveryHeaderData(param)
      .pipe(
        map(data => {
          return data;
        }),
      )
        .subscribe((data: any) => {
          this.spinner.hide();
          if (Array.isArray(data)) {
            if ($.fn.dataTable.isDataTable('#delivery_header_table')) {
              $('#delivery_header_table').DataTable().clear().destroy();               
            }
            this.deliveryArray = data;
            this.deliveryArray.map((index) => {
              let status = '';
              if(index.Status == 0){
                status = 'Draft';
              }else if(index.Status == 1){
                status = 'Submitted';
              }else if(index.Status == 2){
                status = 'Closed';
              }
              index.Status = status;
            });
            this.chRef.detectChanges();
            const table: any = $('table');
            this.datatable = table.DataTable();
          }
        }, error => {
          this.spinner.hide()
          this.alertService.error(Language.ALERT.Title_Error, Language.ALERT.Went_Wrong_Message);
       });
  } 
  
  getDeliveryExportData(){
    this.spinner.show();
      let param = {
        FromDate: this.pipe.transform(this.inquiryFilter.FromDate,'yyyy-MM-dd'),
        ToDate: this.pipe.transform(this.inquiryFilter.ToDate,'yyyy-MM-dd'),
        Keyword: this.inquiryFilter.Keyword,
        Status: 0,
        DocumentType: 'Delivery'
      }
  
    this.asnShipmentService.getDeliveryExportData(param).subscribe((data: any) => {
      this.spinner.hide();
      if (data) {
        var blob = new Blob([data], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8' });
        download(blob, 'Delivery_Header_Export' + new  Date().getTime() + '.xlsx');
      }
      else {
        this.alertService.error(Language.ALERT.Title_Error, Language.ALERT.Went_Wrong_Message);
      }
    }, error => {
      console.log(error, 'error')
      this.spinner.hide();
      this.alertService.error(Language.ALERT.Title_Error, Language.ALERT.Went_Wrong_Message);
   });
  }

  viewDeliveryDetails(delivery) {
    localStorage.setItem('editDeliveryId', delivery.DeliveryHeaderID);
    delivery.from = "/asn-shipment-section/delivery";
    delivery.tab = this.label.DELIVERY_TAB.Delivery;
    delivery.mode = varConstants.isRegistered;
    this.router.navigate(["/asn-shipment-section/delivery-details"], { state: { data: delivery } });
  }

}
