import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AsnShipmentRoutingModule } from './asn-shipment-section-routing.module';
// import { DeliveryComponent } from './delivery/delivery.component';
// import { DeliveryDetailsComponent } from './delivery-details/delivery-details.component';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    AsnShipmentRoutingModule,
    NgMultiSelectDropDownModule.forRoot(),
  ]
})
export class AsnShipmentSectionModule { }
