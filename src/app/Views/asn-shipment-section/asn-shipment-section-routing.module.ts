import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { Language } from 'src/app/Other/translation.constants';

const routes: Routes = [{
  path: '',
  data: {
    title: Language.TAB.Admin_Section,
    status: false
  },
  children: [
    {
      path: '',
      redirectTo: 'asn-shipment-section',
      pathMatch: 'full'
    },
    {
      path: 'delivery',
      loadChildren: () => import('./delivery/delivery.module').then(m => m.DeliveryModule)
    },
    {
      path: 'delivery-details',
      loadChildren: () => import('./delivery-details/delivery-details.module').then(m => m.DeliveryDetailsModule)
    }
  ]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AsnShipmentRoutingModule { }