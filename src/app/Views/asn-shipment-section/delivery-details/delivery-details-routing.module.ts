import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DeliveryDetailsComponent } from './delivery-details.component';

const routes: Routes = [
  { path: "", component: DeliveryDetailsComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DeliveryDetailsRoutingModule { }
