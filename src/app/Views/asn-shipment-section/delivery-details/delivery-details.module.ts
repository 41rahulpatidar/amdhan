import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DeliveryDetailsRoutingModule } from './delivery-details-routing.module';
import { PackagedModule } from '../../../../app/Packaged/packaged.module';
import { DeliveryDetailsComponent } from './delivery-details.component';
import { Angular2CsvModule } from 'angular2-csv';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';

@NgModule({
  declarations: [DeliveryDetailsComponent],
  imports: [
    CommonModule,
    PackagedModule,
    DeliveryDetailsRoutingModule,
    Angular2CsvModule,
    NgMultiSelectDropDownModule.forRoot(),
  ]
})
export class DeliveryDetailsModule { }
