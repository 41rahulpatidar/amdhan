import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { AlertService } from 'src/app/Service/alert.service';
import { Language } from '../../../Other/translation.constants';
import { varConstants } from 'src/app/Other/variable.constants';
import { NgxSpinnerService } from 'ngx-spinner';
import { UtilService } from 'src/app/Service/util.service';
import { AsnShipmentService } from '../asn-shipment.service';
import { IDropdownSettings } from 'ng-multiselect-dropdown';
import {DatePipe} from '@angular/common';
import { saveAs as download } from "file-saver";

@Component({
  selector: 'app-delivery-details',
  templateUrl: './delivery-details.component.html',
  styleUrls: ['./delivery-details.component.scss']
})
export class DeliveryDetailsComponent implements OnInit {

	@ViewChild('addInquiryForm', { static: true }) addInquiryForm: any;
  deliveryDetails: any = {};
  userData: any;
  tabsArray: any = [];
  label: any = Language;
 
  
  itemsDetails:any = {};
  viewModeField: boolean = false;
  editDeliveryId: any = '';
  

  formSaveErrorMsg: any = '';
  pipe = new DatePipe('en-US');
  deliveryDetailsLabel : any = '';
  currentDate: any = new Date();
  currentSection: any;
  visibaleButton: boolean= true;
  selectedFile: any = '';
  deliveryItemDetail: any = [];
  isMenuAccess: boolean = true;
  isAddEditPermission: boolean = true;
  options: any = {}
  deliveryExportData: any = [];
  inquiryDocumentForm: any = {};
  documentDetails: any = {};

  invoiceItemSelectedItem: any = [];
  invocieItemSetting: any = {};

  invoiceDocumentDetails: any = [];
  editDocumentItemIndex: any = '';

  constructor(private router: Router,
    private alertService: AlertService,
    private spinner: NgxSpinnerService,
    private asnShipmentService: AsnShipmentService,
    private util: UtilService,) { }

  ngOnInit() {
    this.invocieItemSetting = {
      singleSelection: true,
      idField: 'DeliveryItemID',
      textField: 'DeliveryItem',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      itemsShowLimit: 3,
      allowSearchFilter: true,
      closeDropDownOnSelection: true
    }
    this.options = {
      fieldSeparator: ',',
      quoteStrings: '"',
      decimalseparator: '.',
      showLabels: true,
      headers: [],
      showTitle: true,
      title: 'Delivery Details',
      useBom: false,
      removeNewLines: true
    };
    this.inquiryDocumentForm = {
      submitted: false
    }
    this.deliveryDetailsLabel = this.label.DELIVERY_TAB.Delivery_Details;
    this.userData = JSON.parse(localStorage.getItem("logged_in_user_details"));
    if (history.state.data) {
      this.deliveryDetails = history.state.data;
      this.tabsArray = [
        { id: "inquiry-items-section1", name: this.label.TAB.Inquiry_Items, isActiveClass: "active" },
        { id: "inquiry-items-section2", name: this.label.TAB.Documents, isActiveClass: "" }
      ]
    } else {
      this.router.navigate(['/dashboard']);
    }
    this.editDeliveryId = localStorage.getItem('editDeliveryId');
    if(this.editDeliveryId != undefined && this.editDeliveryId != null){
      this.getSingleDeliveryDetails();
    }else{
      this.router.navigate(['/dashboard']);
    }

    //**** User Menu Access Start ****//
    if(this.isMenuAccess == false){
      this.router.navigate(["/dashboard"]);
    }
    if(this.userData.RoleType == '2'){
      this.isAddEditPermission = true;
    }
    if(this.userData.RoleType == '3'){
      if(this.userData.Lst_RoleData1[0].ApprovalLevel == 'L01'){
        this.isAddEditPermission = false;
      }else{
        this.isAddEditPermission = false;
      }
    }
    //**** User Menu Access End ****//
    this.getInvoiceDocumentDetails();
  }
  
 

  openModal(id) {
    this.inquiryDocumentForm.submitted = false;
    this.itemsDetails = {}
    document.getElementById(id).style.display = "block";
    document.getElementById(id).classList.add("show");
    document.body.classList.add("modal-open");
  }

  closeModal(id) {
    document.getElementById(id).style.display = "none";
    document.getElementById(id).classList.remove("show");
    document.body.classList.remove("modal-open");
  }

  addInquiryItem(event){
   this.closeModal(event);
  }

  editInquiryItem(inquiryLineItem, index) {
    document.getElementById('add-delivery-items-modal').style.display = "block";
    document.getElementById('add-delivery-items-modal').classList.add("show");
    document.body.classList.add("modal-open");
   
    // this.itemsDetails['NetWeight'] = inquiryLineItem.NetWeight;
    // this.itemsDetails['WeightUnit'] = inquiryLineItem.WeightUnit;
  }


  getSingleDeliveryDetails(){
    this.spinner.show();
    this.asnShipmentService.getSingleDeliveryData(this.editDeliveryId).pipe()
      .subscribe((data: any) => {
        this.spinner.hide();
        data.from = this.deliveryDetails.from;
        this.deliveryDetails = data;
        this.deliveryDetails.PickingDate = this.pipe.transform(this.deliveryDetails.PickingDate,'MMM dd, yyyy');
        this.deliveryDetails.DeliveryDate = this.pipe.transform(this.deliveryDetails.DeliveryDate,'MMM dd, yyyy');
        this.deliveryDetails.TransportationPlanningDate = this.pipe.transform(this.deliveryDetails.TransportationPlanningDate,'MMM dd, yyyy');
        this.deliveryDetails.LoadingDate = this.pipe.transform(this.deliveryDetails.LoadingDate,'MMM dd, yyyy');
        this.deliveryDetails.PlannedGoodsMovementDate = this.pipe.transform(this.deliveryDetails.PlannedGoodsMovementDate,'MMM dd, yyyy');
        this.deliveryDetails.TimeOfDelivery = this.pipe.transform(this.deliveryDetails.TimeOfDelivery,'MMM d, y, h:mm a');
        // from
        console.log(this.deliveryDetails, 'delivery')
        this.spinner.hide();
        this.getSalesDeliveryItemListById();
      }, error => {
        this.spinner.hide()
        this.alertService.error(Language.ALERT.Title_Error, Language.ALERT.Went_Wrong_Message);
        // this.util.PrintLogs(varConstants.ERROR, this.className, this.getSingledeliveryDetailsname, error);
      });
  }

  exportExcelData(){
    this.deliveryExportData = [];
    this.deliveryExportData = [
      {
        Delivery: this.label.DELIVERY_TAB.Delivery,
        DeliveryType: this.label.DELIVERY_TAB.Delivery_Type,
        SoldToParty: this.label.DELIVERY_TAB.Sold_To_Party,
        ShipToParty: this.label.DELIVERY_TAB.Ship_To_Party,
        SalesOrganization: this.label.DELIVERY_TAB.Sales_Organization,
        DocumentCurrency: this.label.DELIVERY_TAB.SD_Document_Currency,
        ShippingConditions: this.label.DELIVERY_TAB.Shipping_Conditions,
        NetWeight: this.label.DELIVERY_TAB.Net_Weight,
        WeightUnit: this.label.DELIVERY_TAB.Weight_Unit,
        Volume: this.label.DELIVERY_TAB.Volume,
        VolumeUnit: this.label.DELIVERY_TAB.Volume_Unit,
        GoodsMovementDate: this.label.DELIVERY_TAB.Planned_Goods_Movement_Date,
        LoadingDate: this.label.DELIVERY_TAB.Loading_Date,
        PlanningDate: this.label.DELIVERY_TAB.Transportation_Planning_Date,
        DeliveryDate: this.label.DELIVERY_TAB.Delivery_Date,
        PickingDate: this.label.DELIVERY_TAB.Picking_Date,
        TimeOfDelivery: this.label.DELIVERY_TAB.Time_Of_Delivery,
        DeliveryItem: this.label.DELIVERY_TAB.Delivery_Item,
        ArticleNumber: this.label.DELIVERY_TAB.Article_Number,
        ArticleEntered: this.label.DELIVERY_TAB.Article_Entered,
        Site: this.label.DELIVERY_TAB.Site,
        ActualQuantityDelivered: this.label.DELIVERY_TAB.Actual_Quantity_Delivered,
        BaseUnitMeasure: this.label.DELIVERY_TAB.Base_Unit_Measure,
        NetWeightDetails: this.label.DELIVERY_TAB.Net_Weight,
        WeightUnitDetails: this.label.DELIVERY_TAB.Weight_Unit,
        VolumeDetails: this.label.DELIVERY_TAB.Volume,
        VolumeUnitDetails: this.label.DELIVERY_TAB.Volume_Unit,
        PartialDeliveryItemLevel: this.label.DELIVERY_TAB.Partial_Delivery_Item_Level,
      }
    ];

    let exportObj = {
      Delivery: (this.deliveryDetails.Delivery != '') ? this.deliveryDetails.Delivery : '--',
      DeliveryType: (this.deliveryDetails.DeliveryType != '') ? this.deliveryDetails.DeliveryType : '--',
      SoldToParty: (this.deliveryDetails.SoldToParty != '') ? this.deliveryDetails.SoldToParty : '--',
      ShipToParty: (this.deliveryDetails.ShipToParty != '') ? this.deliveryDetails.ShipToParty : '--',
      SalesOrganization: (this.deliveryDetails.SalesOrganization != '') ? this.deliveryDetails.SalesOrganization : '--',
      DocumentCurrency: (this.deliveryDetails.SDDocumentCurrency != '') ? this.deliveryDetails.SDDocumentCurrency : '--',
      ShippingConditions: (this.deliveryDetails.ShippingConditions != '') ? this.deliveryDetails.ShippingConditions : '--',
      NetWeight: (this.deliveryDetails.NetWeight != '') ? this.deliveryDetails.NetWeight : '--',
      WeightUnit: (this.deliveryDetails.WeightUnit != '') ? this.deliveryDetails.WeightUnit : '--',
      Volume: (this.deliveryDetails.Volume != '') ? this.deliveryDetails.Volume : '--',
      VolumeUnit: (this.deliveryDetails.VolumeUnit != '') ? this.deliveryDetails.VolumeUnit : '--',
      GoodsMovementDate: (this.deliveryDetails.PlannedGoodsMovementDate != '') ? this.deliveryDetails.PlannedGoodsMovementDate : '--',
      LoadingDate: (this.deliveryDetails.LoadingDate != '') ? this.deliveryDetails.LoadingDate : '--',
      PlanningDate: (this.deliveryDetails.TransportationPlanningDate != '') ? this.deliveryDetails.TransportationPlanningDate : '--',
      DeliveryDate: (this.deliveryDetails.DeliveryDate != '') ? this.deliveryDetails.DeliveryDate : '--',
      PickingDate: (this.deliveryDetails.PickingDate != '') ? this.deliveryDetails.PickingDate : '--',
      TimeOfDelivery: (this.deliveryDetails.TimeOfDelivery != '') ? this.deliveryDetails.TimeOfDelivery : '--',
      DeliveryItem: '--',
      ArticleNumber: '--',
      ArticleEntered: '--',
      Site: '--',
      ActualQuantityDelivered: '--',
      BaseUnitMeasure: '--',
      NetWeightDetails: '--',
      WeightUnitDetails: '--',
      VolumeDetails: '--',
      VolumeUnitDetails: '--',
      PartialDeliveryItemLevel: '--',
    }
    
    if(this.deliveryItemDetail.length == 0){
      this.deliveryExportData.push(exportObj);
    }else{
      for (let i = 0; i < this.deliveryItemDetail.length; i++) {
        let exportObj = {
          Delivery: (this.deliveryDetails.Delivery != '') ? this.deliveryDetails.Delivery : '--',
          DeliveryType: (this.deliveryDetails.DeliveryType != '') ? this.deliveryDetails.DeliveryType : '--',
          SoldToParty: (this.deliveryDetails.SoldToParty != '') ? this.deliveryDetails.SoldToParty : '--',
          ShipToParty: (this.deliveryDetails.ShipToParty != '') ? this.deliveryDetails.ShipToParty : '--',
          SalesOrganization: (this.deliveryDetails.SalesOrganization != '') ? this.deliveryDetails.SalesOrganization : '--',
          DocumentCurrency: (this.deliveryDetails.SDDocumentCurrency != '') ? this.deliveryDetails.SDDocumentCurrency : '--',
          ShippingConditions: (this.deliveryDetails.ShippingConditions != '') ? this.deliveryDetails.ShippingConditions : '--',
          NetWeight: (this.deliveryDetails.NetWeight != '') ? this.deliveryDetails.NetWeight : '--',
          WeightUnit: (this.deliveryDetails.WeightUnit != '') ? this.deliveryDetails.WeightUnit : '--',
          Volume: (this.deliveryDetails.Volume != '') ? this.deliveryDetails.Volume : '--',
          VolumeUnit: (this.deliveryDetails.VolumeUnit != '') ? this.deliveryDetails.VolumeUnit : '--',
          GoodsMovementDate: (this.deliveryDetails.PlannedGoodsMovementDate != '') ? this.deliveryDetails.PlannedGoodsMovementDate : '--',
          LoadingDate: (this.deliveryDetails.LoadingDate != '') ? this.deliveryDetails.LoadingDate : '--',
          PlanningDate: (this.deliveryDetails.TransportationPlanningDate != '') ? this.deliveryDetails.TransportationPlanningDate : '--',
          DeliveryDate: (this.deliveryDetails.DeliveryDate != '') ? this.deliveryDetails.DeliveryDate : '--',
          PickingDate: (this.deliveryDetails.PickingDate != '') ? this.deliveryDetails.PickingDate : '--',
          TimeOfDelivery: (this.deliveryDetails.TimeOfDelivery != '') ? this.deliveryDetails.TimeOfDelivery : '--',
          DeliveryItem: (this.deliveryItemDetail[i]['DeliveryItem'] != '') ? this.deliveryItemDetail[i]['DeliveryItem'] : '--',
          ArticleNumber: (this.deliveryItemDetail[i]['ArticleNumber'] != '') ? this.deliveryItemDetail[i]['ArticleNumber'] : '--',
          ArticleEntered: (this.deliveryItemDetail[i]['ArticleEntered'] != '') ? this.deliveryItemDetail[i]['ArticleEntered'] : '--',
          Site: (this.deliveryItemDetail[i]['Site'] != '') ? this.deliveryItemDetail[i]['Site'] : '--',
          ActualQuantityDelivered: (this.deliveryItemDetail[i]['ActualQuantityDelivered_in_sales_units_'] != '') ? this.deliveryItemDetail[i]['ActualQuantityDelivered_in_sales_units_'] : '--',
          BaseUnitMeasure: (this.deliveryItemDetail[i]['BaseUnitOfMeasure'] != '') ? this.deliveryItemDetail[i]['BaseUnitOfMeasure'] : '--',
          NetWeightDetails: (this.deliveryItemDetail[i]['NetWeight'] != '') ? this.deliveryItemDetail[i]['NetWeight'] : '--',
          WeightUnitDetails: (this.deliveryItemDetail[i]['WeightUnit'] != '') ? this.deliveryItemDetail[i]['WeightUnit'] : '--',
          VolumeDetails: (this.deliveryItemDetail[i]['Volume'] != '') ? this.deliveryItemDetail[i]['Volume'] : '--',
          VolumeUnitDetails: (this.deliveryItemDetail[i]['VolumeUnit'] != '') ? this.deliveryItemDetail[i]['VolumeUnit'] : '--',
          PartialDeliveryItemLevel: (this.deliveryItemDetail[i]['PartialDeliveryAtItemLevel'] != '') ? this.deliveryItemDetail[i]['PartialDeliveryAtItemLevel'] : '--',
        }

        this.deliveryExportData.push(exportObj);
      }
    }     
    console.log(this.deliveryExportData, 'delivery export1')
  }

  addDcoumentDetails(event){
    this.inquiryDocumentForm.submitted = true;
    if(!this.selectedFile || this.invoiceItemSelectedItem.length == 0 || !this.documentDetails.DocRefNo || !this.documentDetails.DocumentName || !this.documentDetails.Doctype){
      return false;
    }
    const formData: FormData = new FormData();
    formData.append('FileData', this.selectedFile);
    let lineItem = (this.invoiceItemSelectedItem.length > 0) ? this.invoiceItemSelectedItem[0]['DeliveryItemID']: '';
    let param = 'DocumentID='+this.editDeliveryId+'&LineItem='+lineItem+'&DocRefNo='+this.documentDetails.DocRefNo+'&DocumentName='+this.documentDetails.DocumentName+'&Doctype='+this.documentDetails.Doctype;
    this.spinner.show();
    this.asnShipmentService.addInvoiceDocumentDetails(formData, param).pipe().subscribe((data: any) => {
      this.alertService.success(Language.ALERT.Success, Language.ALERT.Saved_Message);
      this.getInvoiceDocumentDetails();
    }, error => {
      this.spinner.hide()
      this.alertService.error(Language.ALERT.Title_Error, Language.ALERT.Went_Wrong_Message);
    });
    this.closeModal(event);
  }

  getInvoiceDocumentDetails(){
    this.spinner.show();
    let param = {
        DocumentID: this.editDeliveryId
    }
    this.asnShipmentService.getSalesInquiryDocumentById(param).pipe()
    .subscribe((data: any) => {
      this.spinner.hide();
      this.invoiceDocumentDetails = data;
      console.log(this.invoiceDocumentDetails, 'get')
    }, error => {
      this.spinner.hide()
      this.alertService.error(Language.ALERT.Title_Error, Language.ALERT.Went_Wrong_Message);
    });
  }

  getSalesDeliveryItemListById(){
    this.spinner.show();
    let param = {
      DeliveryHeaderID: this.deliveryDetails.DeliveryHeaderID
    }
    this.asnShipmentService.getSalesDeliveryItemListById(param).pipe()
      .subscribe((data: any) => {
        this.spinner.hide();
        this.deliveryItemDetail = data;
        this.exportExcelData();
      }, error => {
        this.spinner.hide()
        this.alertService.error(Language.ALERT.Title_Error, Language.ALERT.Went_Wrong_Message);
        // this.util.PrintLogs(varConstants.ERROR, this.className, this.getSingledeliveryDetailsname, error);
      });
  }

  documentDownload(docName){
    this.spinner.show();
    let param = {
      AttachmentName: docName
    }
    this.asnShipmentService.documentDownload(param).pipe()
      .subscribe((data: any) => {
        if (data) {
          var blob = new Blob([data], { type: 'image/png' });
          download(blob, docName);
        }else {
          this.alertService.error(Language.ALERT.Title_Error, Language.ALERT.Went_Wrong_Message);
        }
        this.spinner.hide()
      }, error => {
        this.spinner.hide()
        this.alertService.error(Language.ALERT.Title_Error, Language.ALERT.Went_Wrong_Message);
        // this.util.PrintLogs(varConstants.ERROR, this.className, this.getSingledeliveryDetailsname, error);
      });
  }

  onChangeTab(tabDetails: any) {
    for (let i = 0; i < this.tabsArray.length; i++) {
      if (tabDetails.id == this.tabsArray[i].id) {
        this.tabsArray[i].isActiveClass = "active";
      } else {
        this.tabsArray[i].isActiveClass = "";
      }
    }
  }

  selectFile(event): void {
    this.selectedFile = event.target.files.item(0);
  }

  enableForm() {
    this.viewModeField = false;
  }

  ngOnDestroy(){
    localStorage.removeItem('editDeliveryId');
  }
  saveEnquiryDetails(){
  }
}
