import { Injectable } from '@angular/core';
import { RestApiService } from 'src/app/Service/rest-api.service';
import { apiUrl } from 'src/app/Other/apiUrl.constants';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AsnShipmentService {

  constructor(private restService: RestApiService) { }
  
  getDeliveryHeaderData(param): Observable<any> {
    return this.restService.get(apiUrl.SALES_INQUIRY_HEADER, param);
  }

  getDeliveryExportData(param): Observable<any> {
    return this.restService.get(apiUrl.EXPORT_UTILITY, param, 'blob');
  }

  getSingleDeliveryData(enquiryId): Observable<any> {
    return this.restService.get(apiUrl.SALES_INQUIRY_HEADER+'/'+enquiryId);
  }

  getSalesDeliveryItemListById(request): Observable<any> {
    return this.restService.get(apiUrl.DELIVERY_ITEM_BY_ID, request);
  }

  documentDownload(request): Observable<any> {
    return this.restService.get(apiUrl.DOCUMENT_DOWNLOAD, request, 'blob');
  }

  getCustomerDetails(request): Observable<any> {
    return this.restService.get(apiUrl.CUSTOMER_DETAILS, request);
  }

  getCustomerMasterList(): Observable<any> {
    return this.restService.get(apiUrl.CUSTOMER_MASTER);
  }


  getMaterialMasterList(): Observable<any> {
    return this.restService.get(apiUrl.MATERIAL_MASTER_LIST);
  }

  addInquiryHeaderDetails(request): Observable<any> {
    return this.restService.post(apiUrl.ADD_UPDATE_SALES_INQUIRY_HEADER, request);
  }

  updateInquiryHeaderDetails(request, editId): Observable<any> {
    return this.restService.put(apiUrl.ADD_UPDATE_SALES_INQUIRY_HEADER+'?SalesInquiryID='+editId, request);
  }

  addInquiryLineItemDetails(request): Observable<any> {
    return this.restService.post(apiUrl.ADD_UPDATE_SALES_INQUIRY_DETAILS, request);
  }

  updateInquiryLineItemDetails(request,editId): Observable<any> {
    return this.restService.put(apiUrl.ADD_UPDATE_SALES_INQUIRY_DETAILS+'?SalesInquiryItemID='+editId, request);
  }

  deleteSalesInquiryDetails(request): Observable<any> {
    return this.restService.delete(apiUrl.INQUIRY_DATA, request);
  }

  deleteSalesItemDetails(request): Observable<any> {
    return this.restService.delete(apiUrl.INQUIRY_DETAILS, request);
  }

  addInvoiceDocumentDetails(request, param): Observable<any> {
    return this.restService.post(apiUrl.ADD_INQUIRY_DOCUMENT+'?'+param, request);
  }

  getSalesInquiryDocumentById(request): Observable<any> {
    return this.restService.get(apiUrl.GET_INQUIRY_DOCUMENT, request);
  }
  

}
