import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { AlertService } from 'src/app/Service/alert.service';
import { AppServiceService } from 'src/app/Service/app-service.service';
import { UtilService } from 'src/app/Service/util.service';
import { Language } from '../../Other/translation.constants';
import { varConstants } from 'src/app/Other/variable.constants';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  label: any = Language;
  // tabsArray: any = [];
  permittedTilesArr: any = [];
  tabsArray: any = [];
  className = DashboardComponent.name;
  privateMessages: any = [];
  userDetails: any;

  constructor(private router: Router,
    private alertService: AlertService,
    private appService: AppServiceService,
    private spinner: NgxSpinnerService,
    private util: UtilService) {
  }

  ngOnInit() {
    this.userDetails = JSON.parse(localStorage.getItem("logged_in_user_details"));
    let customerPortalAccess = true;
    
    // if(this.userDetails.IsCustomerPortalAccess){
    //   this.preparePermittedTilesArr(varConstants.customerPortalRoleType);
    // }
    // else if(this.userDetails.IsVendorPortalAccess){
    //   this.preparePermittedTilesArr(varConstants.vendorPortalRoleType);
    // }
    // else if(this.userDetails.IsCataloguePortalAccess){
    //   this.preparePermittedTilesArr(varConstants.cataloguePortalRoleType);
    // }
    // else{
    //   this.preparePermittedTilesArr(varConstants.customerPortalRoleType);
    // }
    this.preparePermittedTilesArr(this.userDetails.RoleType);
    
    this.getPublicMessageDetails();
    this.getTileCount();
  }

  getTileCount(){
    this.spinner.show();
    this.appService.getDashboardTileCount().subscribe((data: any) => {
      // console.log(data)
      this.spinner.hide();
      this.tabsArray = [
          {
            id: "admin-section", name: this.label.TAB.Admin_Section, activeTab: "active", activeData: "", isAccessible: this.checkForPermission("admin-section"), cardsArray: [
              { url: "/admin-section/users", name: this.label.SUB_TAB.User_Managemnet, count: 0, isAccessible: this.checkForPermission("users"), icon: "fa fa-users", text: "Users" },
              //{ url: "/admin-section/roles", name: this.label.SUB_TAB.Roles, count: this.fetchCount(varConstants.isNew, varConstants.Purchase), isAccessible: this.checkForPermission("roles"), icon: "fa fa-unlock-alt", text: "Roles" },
            ]
          },
          {
            id: "masters-section", name: this.label.TAB.Masters, activeTab: "", activeData: "", isAccessible: this.checkForPermission("masters-section"), cardsArray: [
              { url: "/master-section/customer", name: this.label.SUB_TAB.Customer, count: 0, isAccessible: this.checkForPermission("master-section-customer"), icon: "fa fa-users", text: this.label.SUB_TAB.Customer , type: ''},
              { url: "/master-section/dealer", name: this.label.SUB_TAB.Dealer, count: 0, isAccessible: this.checkForPermission("master-section-dealer"), icon: "fa fa-briefcase", text: this.label.SUB_TAB.Material, type: '' },
              { url: "/master-section/material", name: this.label.SUB_TAB.Material, count: 0, isAccessible: this.checkForPermission("master-section-material"), icon: "fa fa-dropbox", text: this.label.SUB_TAB.Material , type: ''},
              { url: "/master-section/price-list", name: this.label.SUB_TAB.PriceList, count: 0, isAccessible: this.checkForPermission("master-section-price-list"), icon: "fa fa-list", text: this.label.SUB_TAB.PriceList , type: ''},
            ]
          },
          {
            id: "inquiry-section", name: this.label.TAB.Inquiry, activeTab: "", activeData: "", isAccessible: this.checkForPermission("inquiry-section"), cardsArray: [
              { url: "/inquiry-quotation-section/inquiry", name: this.label.SUB_TAB.New_Inquiry, count: data.NewInquiryCount, isAccessible: this.checkForPermission("new-inquiry-list"), icon: "fa fa-question-circle", text: this.label.SUB_TAB.New_Inquiry, type: 'NewInquiry' },
              { url: "/inquiry-quotation-section/inquiry", name: this.label.SUB_TAB.Close_Inquiry, count: data.ClosedInquiryCount, isAccessible: this.checkForPermission("close-inquiry-list"), icon: "fa fa-window-close", text: this.label.SUB_TAB.Close_Inquiry, type: 'CloseInquiry' },
            ]
          },
          {
            id: "quotation-section", name: this.label.TAB.Quotation, activeTab: "", activeData: "", isAccessible: this.checkForPermission("quotation-section"), cardsArray: [
             { url: "/inquiry-quotation-section/quotation", name: this.label.SUB_TAB.Create_Quotation, count: data.NewQuotationCount, isAccessible: this.checkForPermission("quotation-list"), icon: "fa fa-usd", text: this.label.SUB_TAB.Create_Quotation , type: ''},
             // { url: "/inquiry-quotation-section/quotation", name: this.label.SUB_TAB.Display_Quotation, count: data.SubmittedQuotationCount, isAccessible: this.checkForPermission("display-quotation-list"), icon: "fa fa-usd", text: this.label.SUB_TAB.Display_Quotation , type: ''},
             // { url: "/inquiry-quotation-section/quotation", name: this.label.SUB_TAB.Close_Quotation, count: data.ClosedQuotationCount, isAccessible: this.checkForPermission("close-quotation-list"), icon: "fa fa-usd", text: this.label.SUB_TAB.Close_Quotation , type: ''},
            ]
          },
          {
            id: "orders", name: this.label.TAB.Orders, activeTab: "", activeData: "", isAccessible: this.checkForPermission("orders"), cardsArray: [
              { url: "/orders/new", name: this.label.SUB_TAB.New, count: data.NewSalesOrderCount, isAccessible: this.checkForPermission("orders-new"), icon: "fa fa-pencil-square-o", text: this.label.SUB_TAB.New },
              { url: "/orders/submitted", name: this.label.SUB_TAB.Submitted, count: data.SubmittedSalesOrderCount, isAccessible: this.checkForPermission("orders-submitted"), icon: "fa fa-paper-plane", text: this.label.SUB_TAB.Submitted },
              { url: "/orders/open", name: this.label.SUB_TAB.Open, count: data.OpenSalesOrderCount, isAccessible: this.checkForPermission("orders-open"), icon: "fa fa-file-text-o", text: this.label.SUB_TAB.Open },
              { url: "/orders/closed", name: this.label.SUB_TAB.Closed, count: data.ClosedSalesOrderCount, isAccessible: this.checkForPermission("orders-close"), icon: "fa fa-window-close-o", text: this.label.SUB_TAB.Closed },
              { url: "/orders/all", name: this.label.SUB_TAB.All, count: data.AllSalesOrderCount, isAccessible: this.checkForPermission("orders-all"), icon: "fa fa-list-alt", text: this.label.SUB_TAB.All},
            ]
          },
          // {
          //   id: "sales-orders-section", name: this.label.TAB.Sales_Orders, activeTab: "", activeData: "", isAccessible: true, cardsArray: [
          //     { url: "/vendor-section/vendor", name: this.label.SUB_TAB.New, count: 0, isAccessible: true, icon: "fa fa-pencil-square-o", text: this.label.SUB_TAB.New , type: ''},
          //     { url: "/vendor-section/vendor", name: this.label.SUB_TAB.Open, count: 0, isAccessible: true, icon: "fa fa-file-text-o", text: this.label.SUB_TAB.Open, type: '' },
          //     { url: "/vendor-section/vendor", name: this.label.SUB_TAB.Closed, count: 0, isAccessible: true, icon: "fa fa-window-close-o", text: this.label.SUB_TAB.Closed , type: ''},
          //     { url: "/vendor-section/cr", name: this.label.SUB_TAB.All, count: 0, isAccessible: true, icon: "fa fa-list-alt", text: this.label.SUB_TAB.All, type: ''},
          //   ]
          // },
          {
            id: "asn-shipment-section", name: this.label.TAB.ASN_Shipment, activeTab: "", activeData: "", isAccessible: this.checkForPermission("asn-shipment-section"), cardsArray: [
              { url: "/asn-shipment-section/delivery", name: this.label.SUB_TAB.Delivery, count: data.NewDeliveryCount, isAccessible: this.checkForPermission("asn-shipment-section-delivery"), icon: "fa fa-truck", text: this.label.SUB_TAB.Delivery, type: ''},
            ]
          },
          {
            id: "invoice-payment", name: this.label.TAB.Invoice_Payemnts, activeTab: "", activeData: "", isAccessible: this.checkForPermission("invoice-payment"), cardsArray: [
              { url: "/invoice-payment/invoice", name: this.label.SUB_TAB.Invoice, count: data.NewSalesInvoiceCount, isAccessible: this.checkForPermission("invoice-payment-list"), icon: "fa fa-file-text-o", text: this.label.SUB_TAB.Invoice , type: ''},
              { url: "/invoice-payment/payment", name: this.label.SUB_TAB.Payemnts, count: 0, isAccessible: this.checkForPermission("payment-list"), icon: "fa fa-credit-card", text: this.label.SUB_TAB.Payemnts , type: ''},
            ]
          },
           {
            id: "marketing", name: this.label.TAB.Marketing, activeTab: "", activeData: "", isAccessible: this.checkForPermission("marketing-section"), cardsArray: [
             { url: "/marketing-section/message", name: this.label.SUB_TAB.Message, count: 0, isAccessible: this.checkForPermission("message-list"), icon: "fa fa-usd", text: this.label.SUB_TAB.Message , type: ''},
            ]
          },
          {
            id: "questionair", name: this.label.TAB.Questionair, activeTab: "", activeData: "", isAccessible: this.checkForPermission("quize-section"), cardsArray: [
              { url: "/quize-section/quize-master", name: this.label.QUIZE_TAB.Questionair_Master, count: 0, isAccessible: this.checkForPermission("quize-master"), icon: "fa fa-usd", text: this.label.QUIZE_TAB.Questionaire_Master , type: ''},
              { url: "/quize-section/quize-result", name: this.label.QUIZE_TAB.Quize, count: 0, isAccessible: this.checkForPermission("quize-result"), icon: "fa fa-usd", text: this.label.QUIZE_TAB.Quize , type: ''},
            ]
          },
          {
            id: "shopping", name: this.label.TAB.Shopping, activeTab: "", activeData: "", isAccessible: this.checkForPermission("shopping-section"), cardsArray: [
              { url: "/shopping/product-list", name: this.label.SUB_TAB.Product, count: 0, isAccessible: this.checkForPermission("shopping-product"), icon: "fa fa-question-circle", text: this.label.SUB_TAB.Product_Details, type: '' },
              { url: "/shopping/card-details", name: this.label.SUB_TAB.Cards, count: 0, isAccessible: this.checkForPermission("shopping-cards"), icon: "fa fa-window-close", text: this.label.SUB_TAB.Card_Details, type: '' },
            ]
          },
          {
            id: "reports-section", name: this.label.TAB.Reports, activeTab: "", activeData: "", isAccessible: this.checkForPermission("reports-section"), cardsArray: [
              { url: "/admin-section/users", name: this.label.TAB.Reports, count: 0, isAccessible: this.checkForPermission("admin-section-users"), icon: "fa fa-file-excel-o", text: this.label.TAB.Reports , type: ''},
            ]
          }
       ];
    })

     // console.log(this.tabsArray, 'tab array')
  }
  //This will check the permission to the tiles on roles basis
  checkForPermission(tileName) {
    //If the tile name is present in the array then it will be accessible/visible to user
    let isAllowed = true;
    isAllowed = this.permittedTilesArr.includes(tileName);

    // console.log(isAllowed, this.permittedTilesArr, 'check')
    return isAllowed;
  }

  preparePermittedTilesArr(role) {
    switch (role) {
      // case customer portal:
      case "1":
        this.permittedTilesArr = ["admin-section","users", "masters-section","master-section-customer", "master-section-dealer", "master-section-material", "master-section-price-list", "inquiry-section", "new-inquiry-list", "close-inquiry-list", "quotation-section", "quotation-list", "display-quotation-list","close-quotation-list", "orders", 
                                    "orders-new", "orders-submitted" ,"orders-open", "orders-close", "orders-all", "asn-shipment-section", "asn-shipment-section-delivery",
                                  "invoice-payment", "invoice-payment-list", "payment-list","reports-section", "admin-section-users","marketing-section", "message-list","quize-section", "quize-result","quize-master","shopping-section", "shopping-product","shopping-cards"];
      break;
      case "2":
        this.permittedTilesArr = ["users","masters-section","master-section-customer", "master-section-dealer", "master-section-material", "master-section-price-list", "inquiry-section", "new-inquiry-list", "close-inquiry-list", "quotation-section", "quotation-list",  "display-quotation-list","close-quotation-list", "orders", 
                                    "orders-open", "orders-submitted", "orders-close", "orders-all", "asn-shipment-section", "asn-shipment-section-delivery",
                                  "invoice-payment", "invoice-payment-list", "payment-list","reports-section", "admin-section-users","shopping-section", "shopping-product","shopping-cards"];
      break;
      case "3":
        this.permittedTilesArr = ["inquiry-section", "new-inquiry-list", "close-inquiry-list", "quotation-section", "quotation-list",  "display-quotation-list","close-quotation-list", "orders", 
                                    "orders-new", "orders-open","orders-submitted", "orders-close", "orders-all", "asn-shipment-section", "asn-shipment-section-delivery",
                                  "invoice-payment", "invoice-payment-list", "payment-list","reports-section", "admin-section-users","shopping-section", "shopping-product","shopping-cards"];
      break;
      default:
        this.permittedTilesArr = ["reports-section", "admin-section-users"];
      break;
    }
    // console.log(this.permittedTilesArr)
  }

  getPublicMessageDetails(){
    let msg = localStorage.getItem('privateMessages');
    let customerNo = this.userDetails.CustomerNo;
    if(customerNo != null && msg == 'true'){
      let param = {
        CustomerNo: customerNo
      }
      this.appService.getPrivateMessageDetails(param).subscribe((data: any) => {
        this.privateMessages = data;
        document.getElementById('user-messages').style.display = "block";
        document.getElementById('user-messages').style.opacity = "1";
        document.getElementById('user-messages').classList.add("show");
        document.body.classList.add("modal-open");
      })
    }
  }
  
  onChangeTab(tabDetails: any) {
    // console.log(this.tabsArray, tabDetails)
    for (let i = 0; i < this.tabsArray.length; i++) {
      if (tabDetails.id == this.tabsArray[i].id) {
        this.tabsArray[i].activeTab = "active";
        this.tabsArray[i].activeData = "show active";
      } else {
        this.tabsArray[i].activeTab = "";
        this.tabsArray[i].activeData = "";
      }
    }
  }

  onClickTile(index, tabId, routeUrl, type) {
    // console.log(index, tabId, routeUrl, type)
    let data:any = { section: "section" + index, tabId: tabId,type: type }
    localStorage.setItem("current_section", JSON.stringify(data));
    this.router.navigate([routeUrl]);
  }

  closeMessages(){
    localStorage.removeItem('privateMessages');
    document.getElementById('user-messages').style.display = "none";
    document.getElementById('user-messages').classList.remove("show");
    document.body.classList.remove("modal-open");
  }

  ngOnDestroy(){
    localStorage.removeItem('privateMessages');
  }
}
