import { NgModule } from '@angular/core';
import { DashboardComponent } from './dashboard.component';
import { DashboardRoutingModule } from './dashboard-routing.module';
import { PackagedModule } from '../../Packaged/packaged.module';
import { CommonModule } from '@angular/common';

@NgModule({
  declarations: [DashboardComponent],
  imports: [
    CommonModule,
    PackagedModule,
    DashboardRoutingModule
  ]
})
export class DashboardModule { }
