import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InvoicePaymentRoutingModule } from './invoice-payment-routing.module';
// import { DeliveryComponent } from './delivery/delivery.component';
// import { DeliveryDetailsComponent } from './delivery-details/delivery-details.component';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    InvoicePaymentRoutingModule,
    NgMultiSelectDropDownModule.forRoot(),
  ]
})
export class InvoicePaymentModule { }
