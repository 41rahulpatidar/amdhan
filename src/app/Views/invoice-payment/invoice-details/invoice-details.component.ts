import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { AlertService } from 'src/app/Service/alert.service';
import { Language } from '../../../Other/translation.constants';
import { varConstants } from 'src/app/Other/variable.constants';
import { NgxSpinnerService } from 'ngx-spinner';
import { UtilService } from 'src/app/Service/util.service';
import { InvoicePaymentService } from '../invoice-payment.service';
import { IDropdownSettings } from 'ng-multiselect-dropdown';
import {DatePipe} from '@angular/common';
import { saveAs as download } from "file-saver";

@Component({
  selector: 'app-invoice-details',
  templateUrl: './invoice-details.component.html',
  styleUrls: ['./invoice-details.component.scss']
})
export class InvoiceDetailsComponent implements OnInit {

	@ViewChild('addInquiryForm', { static: true }) addInquiryForm: any;
  invoiceDetails: any = {};
  userData: any;
  tabsArray: any = [];
  label: any = Language;
 
  documentDetails: any = {};
  itemsDetails:any = {};
  viewModeField: boolean = false;
  editInvoiceId: any = '';
  invoiceItemSelectedItem: any = [];
  inquiryDocumentForm: any = {};
  formSaveErrorMsg: any = '';
  pipe = new DatePipe('en-US');
  invoiceDetailsLabel : any = '';
  currentDate: any = new Date();
  currentSection: any;
  visibaleButton: boolean= true;
  selectedFile: any = '';
  deliveryItemDetail: any = [];
  isMenuAccess: boolean = true;
  isAddEditPermission: boolean = true;
  options: any = {}
  invoiceExportData: any = [];
  invoiceDocumentDetails: any = [];
  invocieItemSetting: any = {};

  constructor(private router: Router,
    private alertService: AlertService,
    private spinner: NgxSpinnerService,
    private invoicePaymentService: InvoicePaymentService,
    private util: UtilService,) { }

  ngOnInit() {
    this.options = {
      fieldSeparator: ',',
      quoteStrings: '"',
      decimalseparator: '.',
      showLabels: true,
      headers: [],
      showTitle: true,
      title: 'Invoice Details',
      useBom: false,
      removeNewLines: true
    };
    this.invocieItemSetting = {
      singleSelection: true,
      idField: 'Billingitem',
      textField: 'Billingitem',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      itemsShowLimit: 3,
      allowSearchFilter: true,
      closeDropDownOnSelection: true
    }
    this.inquiryDocumentForm = {
      submitted: false
    }
    this.invoiceDetailsLabel = this.label.DELIVERY_TAB.Delivery_Details;
    this.userData = JSON.parse(localStorage.getItem("logged_in_user_details"));
    if (history.state.data) {
      this.invoiceDetails = history.state.data;
      this.tabsArray = [
        { id: "inquiry-items-section1", name: this.label.TAB.Inquiry_Items, isActiveClass: "active" },
        { id: "inquiry-items-section2", name: this.label.TAB.Documents, isActiveClass: "" }
      ]
    } else {
      this.router.navigate(['/dashboard']);
    }
    this.editInvoiceId = localStorage.getItem('editInvoiceId');
    if(this.editInvoiceId != undefined && this.editInvoiceId != null){
      this.getSingleInvoiceDetails();
    }else{
      this.router.navigate(['/dashboard']);
    }

    //**** User Menu Access Start ****//
    if(this.isMenuAccess == false){
      this.router.navigate(["/dashboard"]);
    }
    if(this.userData.RoleType == '2'){
      this.isAddEditPermission = false;
    }
    if(this.userData.RoleType == '3'){
      if(this.userData.Lst_RoleData1[0].ApprovalLevel == 'L01'){
        this.isAddEditPermission = false;
      }else{
        this.isAddEditPermission = false;
      }
    }
    //**** User Menu Access End ****//
    this.getInvoiceDocumentDetails();
  }
  
 

  openModal(id) {
    this.inquiryDocumentForm.submitted = false;
    this.itemsDetails = {}
    this.documentDetails = {}
    document.getElementById(id).style.display = "block";
    document.getElementById(id).classList.add("show");
    document.body.classList.add("modal-open");
  }

  closeModal(id) {
    document.getElementById(id).style.display = "none";
    document.getElementById(id).classList.remove("show");
    document.body.classList.remove("modal-open");
  }

  addInquiryItem(event){
   this.closeModal(event);
  }

  editInquiryItem(inquiryLineItem, index) {
    document.getElementById('add-delivery-items-modal').style.display = "block";
    document.getElementById('add-delivery-items-modal').classList.add("show");
    document.body.classList.add("modal-open");
   
    // this.itemsDetails['NetWeight'] = inquiryLineItem.NetWeight;
    // this.itemsDetails['WeightUnit'] = inquiryLineItem.WeightUnit;
  }


  getSingleInvoiceDetails(){
    this.spinner.show();
    this.invoicePaymentService.getSingleInvoiceData(this.editInvoiceId).pipe()
      .subscribe((data: any) => {
        this.spinner.hide();
        data.from = this.invoiceDetails.from;
        this.invoiceDetails = data;
        // from
        this.spinner.hide();
        this.getSalesInvoiceItemListById();
      }, error => {
        this.spinner.hide()
        this.alertService.error(Language.ALERT.Title_Error, Language.ALERT.Went_Wrong_Message);
        // this.util.PrintLogs(varConstants.ERROR, this.className, this.getSingleInvoiceDetailsname, error);
      });
  }

  getSalesInvoiceItemListById(){
    this.spinner.show();
    let param = {
      InvoiceHeaderID: this.invoiceDetails.InvoiceHeaderID
    }
    this.invoicePaymentService.getSalesInvoiceItemListById(param).pipe()
      .subscribe((data: any) => {
        this.spinner.hide();
        this.deliveryItemDetail = data;
         this.exportExcelData();
      }, error => {
        this.spinner.hide()
        this.alertService.error(Language.ALERT.Title_Error, Language.ALERT.Went_Wrong_Message);
      });
  }

  exportExcelData(){
    this.invoiceExportData = [];
    this.invoiceExportData = [
      {
        BillingDocument: this.label.INVOICE_PAYMENT_TAB.Billing_Document,
        SoldToParty: this.label.INVOICE_PAYMENT_TAB.Sold_To_Party,
        Payer: this.label.INVOICE_PAYMENT_TAB.Payer,
        OrderNumber: this.label.INVOICE_PAYMENT_TAB.Customer_Purchase_Order_Number,
        SalesOrganization: this.label.INVOICE_PAYMENT_TAB.Sales_Organization,
        DocumentCurrency: this.label.INVOICE_PAYMENT_TAB.SD_Document_Currency,
        PaymentMethod: this.label.INVOICE_PAYMENT_TAB.Payment_Method,
        ShippingConditions: this.label.INVOICE_PAYMENT_TAB.Shipping_Conditions,
        NetValueCurrency: this.label.INVOICE_PAYMENT_TAB.Net_Value_Document_Currency,
        BillingItem: this.label.INVOICE_PAYMENT_TAB.Billing_Item,
        ActualBilledQuantity: this.label.INVOICE_PAYMENT_TAB.Actual_Billed_Quantity,
        BaseUnitMeasure: this.label.INVOICE_PAYMENT_TAB.Base_Unit_Measure,
        NetWeight: this.label.INVOICE_PAYMENT_TAB.Net_Weight,
        GrossWeight: this.label.INVOICE_PAYMENT_TAB.Gross_Weight,
        WeightUnit: this.label.INVOICE_PAYMENT_TAB.Weight_Unit,
        Volume: this.label.INVOICE_PAYMENT_TAB.Volume,
        VolumeUnit: this.label.INVOICE_PAYMENT_TAB.Volume_Unit,
        BillingCurrency: this.label.INVOICE_PAYMENT_TAB.Value_Billing_Document_Currency,
        ArticleNumber: this.label.INVOICE_PAYMENT_TAB.Article_Number,
        Site: this.label.INVOICE_PAYMENT_TAB.Site,
      }
    ];

    let exportObj = {
                        BillingDocument: (this.invoiceDetails.BillingDocument != '') ? this.invoiceDetails.BillingDocument : '--',
                        SoldToParty: (this.invoiceDetails.SoldToParty != '') ? this.invoiceDetails.SoldToParty : '--',
                        Payer: (this.invoiceDetails.Payer != '') ? this.invoiceDetails.Payer : '--',
                        OrderNumber: (this.invoiceDetails.CustomerPurchaseOrderNumber != '') ? this.invoiceDetails.CustomerPurchaseOrderNumber : '--',
                        SalesOrganization: (this.invoiceDetails.SalesOrganization != '') ? this.invoiceDetails.SalesOrganization : '--',
                        DocumentCurrency: (this.invoiceDetails.SDDocumentCurrency != '') ? this.invoiceDetails.SDDocumentCurrency : '--',
                        PaymentMethod: (this.invoiceDetails.PaymentMethod != '') ? this.invoiceDetails.PaymentMethod : '--',
                        ShippingConditions: (this.invoiceDetails.ShippingConditions != '') ? this.invoiceDetails.ShippingConditions : '--',
                        NetValueCurrency: (this.invoiceDetails.NetValueinDocumentCurrency != '') ? this.invoiceDetails.NetValueinDocumentCurrency : '--',
                        BillingItem: '--',
                        ActualBilledQuantity: '--',
                        BaseUnitMeasure: '--',
                        NetWeight: '--',
                        GrossWeight: '--',
                        WeightUnit: '--',
                        Volume: '--',
                        VolumeUnit: '--',
                        BillingCurrency: '--',
                        ArticleNumber: '--',
                        Site: '--',
                      }
    
    if(this.deliveryItemDetail.length == 0){
      this.invoiceExportData.push(exportObj);
    }else{
      for (let i = 0; i < this.deliveryItemDetail.length; i++) {
         let exportObj = {
          BillingDocument: (this.invoiceDetails.BillingDocument != '') ? this.invoiceDetails.BillingDocument : '--',
          SoldToParty: (this.invoiceDetails.SoldToParty != '') ? this.invoiceDetails.SoldToParty : '--',
          Payer: (this.invoiceDetails.Payer != '') ? this.invoiceDetails.Payer : '--',
          OrderNumber: (this.invoiceDetails.CustomerPurchaseOrderNumber != '') ? this.invoiceDetails.CustomerPurchaseOrderNumber : '--',
          SalesOrganization: (this.invoiceDetails.SalesOrganization != '') ? this.invoiceDetails.SalesOrganization : '--',
          DocumentCurrency: (this.invoiceDetails.SDDocumentCurrency != '') ? this.invoiceDetails.SDDocumentCurrency : '--',
          PaymentMethod: (this.invoiceDetails.PaymentMethod != '') ? this.invoiceDetails.PaymentMethod : '--',
          ShippingConditions: (this.invoiceDetails.ShippingConditions != '') ? this.invoiceDetails.ShippingConditions : '--',
          NetValueCurrency: (this.invoiceDetails.NetValueinDocumentCurrency != '') ? this.invoiceDetails.NetValueinDocumentCurrency : '--',
          BillingItem: (this.deliveryItemDetail[i].Billingitem != '') ? this.deliveryItemDetail[i].Billingitem: '--',
          ActualBilledQuantity: (this.deliveryItemDetail[i].Actualbilledquantity != '') ? this.deliveryItemDetail[i].Actualbilledquantity: '--',
          BaseUnitMeasure: (this.deliveryItemDetail[i].BaseUnitofMeasure != '') ? this.deliveryItemDetail[i].BaseUnitofMeasure: '--',
          NetWeight: (this.deliveryItemDetail[i].Netweight != '') ? this.deliveryItemDetail[i].Netweight: '--',
          GrossWeight: (this.deliveryItemDetail[i].Grossweight != '') ? this.deliveryItemDetail[i].Grossweight: '--',
          WeightUnit: (this.deliveryItemDetail[i].WeightUnit != '') ? this.deliveryItemDetail[i].WeightUnit: '--',
          Volume: (this.deliveryItemDetail[i].Volume != '') ? this.deliveryItemDetail[i].Volume: '--',
          VolumeUnit: (this.deliveryItemDetail[i].VolumeUnit != '') ? this.deliveryItemDetail[i].VolumeUnit: '--',
          BillingCurrency: (this.deliveryItemDetail[i].NetValueOfBillingItemInDocumentCurrency != '') ? this.deliveryItemDetail[i].NetValueOfBillingItemInDocumentCurrency: '--',
          ArticleNumber: (this.deliveryItemDetail[i].ArticleNumber != '') ? this.deliveryItemDetail[i].ArticleNumber: '--',
          Site: (this.deliveryItemDetail[i].Site != '') ? this.deliveryItemDetail[i].Site: '--',
        }

        this.invoiceExportData.push(exportObj);
      }
    }                
  
    
    
    console.log(this.invoiceExportData, 'order export')
  }

  addDcoumentDetails(event){
    this.inquiryDocumentForm.submitted = true;
    this.inquiryDocumentForm.submitted = true;
    if(!this.selectedFile || this.invoiceItemSelectedItem.length == 0 || !this.documentDetails.DocRefNo || !this.documentDetails.DocumentName || !this.documentDetails.Doctype){
      return false;
    }
    const formData: FormData = new FormData();
    formData.append('FileData', this.selectedFile);
    let lineItem = (this.invoiceItemSelectedItem.length > 0) ? this.invoiceItemSelectedItem[0]['InvoiceItemID']: '';
    let param = 'DocumentID='+this.editInvoiceId+'&LineItem='+lineItem+'&DocRefNo='+this.documentDetails.DocRefNo+'&DocumentName='+this.documentDetails.DocumentName+'&Doctype='+this.documentDetails.Doctype;
    this.spinner.show();
    this.invoicePaymentService.addInvoiceDocumentDetails(formData, param).pipe().subscribe((data: any) => {
      this.alertService.success(Language.ALERT.Success, Language.ALERT.Saved_Message);
      this.getInvoiceDocumentDetails();
    }, error => {
      this.spinner.hide()
      this.alertService.error(Language.ALERT.Title_Error, Language.ALERT.Went_Wrong_Message);
    });
    this.closeModal(event);
  }

  getInvoiceDocumentDetails(){
    this.spinner.show();
    let param = {
        DocumentID: this.editInvoiceId
    }
    this.invoicePaymentService.getSalesInquiryDocumentById(param).pipe()
    .subscribe((data: any) => {
      this.spinner.hide();
      this.invoiceDocumentDetails = data;
      console.log(this.invoiceDocumentDetails, 'get')
    }, error => {
      this.spinner.hide()
      this.alertService.error(Language.ALERT.Title_Error, Language.ALERT.Went_Wrong_Message);
    });
  }

  enableForm() {
    this.viewModeField = false;
  }

  onChangeTab(tabDetails: any) {
    for (let i = 0; i < this.tabsArray.length; i++) {
      if (tabDetails.id == this.tabsArray[i].id) {
        this.tabsArray[i].isActiveClass = "active";
      } else {
        this.tabsArray[i].isActiveClass = "";
      }
    }
  }

  selectFile(event): void {
    this.selectedFile = event.target.files.item(0);
  }


  ngOnDestroy(){
    localStorage.removeItem('editInvoiceId');
  }
  saveEnquiryDetails(){
  }

  documentDownload(docName){
    this.spinner.show();
    let param = {
      AttachmentName: docName
    }
    this.invoicePaymentService.documentDownload(param).pipe()
      .subscribe((data: any) => {
        if (data) {
          var blob = new Blob([data], { type: 'image/png' });
          download(blob, docName);
        }else {
          this.alertService.error(Language.ALERT.Title_Error, Language.ALERT.Went_Wrong_Message);
        }
        this.spinner.hide()
      }, error => {
        this.spinner.hide()
        this.alertService.error(Language.ALERT.Title_Error, Language.ALERT.Went_Wrong_Message);
        // this.util.PrintLogs(varConstants.ERROR, this.className, this.getSingledeliveryDetailsname, error);
      });
  }
}
