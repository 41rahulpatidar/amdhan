import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { InvoiceDetailsRoutingModule } from './invoice-details-routing.module';
import { PackagedModule } from '../../../../app/Packaged/packaged.module';
import { InvoiceDetailsComponent } from './invoice-details.component';
import { Angular2CsvModule } from 'angular2-csv';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';

@NgModule({
  declarations: [InvoiceDetailsComponent],
  imports: [
    CommonModule,
    PackagedModule,
    InvoiceDetailsRoutingModule,
    Angular2CsvModule,
    NgMultiSelectDropDownModule.forRoot(),
  ]
})
export class InvoiceDetailsModule { }
