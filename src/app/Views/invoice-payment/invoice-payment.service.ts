import { Injectable } from '@angular/core';
import { RestApiService } from 'src/app/Service/rest-api.service';
import { apiUrl } from 'src/app/Other/apiUrl.constants';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class InvoicePaymentService {

  constructor(private restService: RestApiService) { }
  
  getInvoiceHeaderData(param): Observable<any> {
    return this.restService.get(apiUrl.INVOICE_HEADER_DATA, param);
  }

  getInvoiceExportData(param): Observable<any> {
    return this.restService.get(apiUrl.EXPORT_UTILITY, param, 'blob');
  }

  getSingleInvoiceData(invoiceId): Observable<any> {
    return this.restService.get(apiUrl.INVOICE_HEADER_DATA+'/'+invoiceId);
  }

  getSalesInvoiceItemListById(request): Observable<any> {
    return this.restService.get(apiUrl.INVOICE_ITEM_BY_ID, request);
  }

  addInvoiceDocumentDetails(request, param): Observable<any> {
    return this.restService.post(apiUrl.ADD_INQUIRY_DOCUMENT+'?'+param, request);
  }

  getSalesInquiryDocumentById(request): Observable<any> {
    return this.restService.get(apiUrl.GET_INQUIRY_DOCUMENT, request);
  }

  documentDownload(request): Observable<any> {
    return this.restService.get(apiUrl.DOCUMENT_DOWNLOAD, request, 'blob');
  }

  getPaymentHeaderData(param): Observable<any> {
    return this.restService.get(apiUrl.PAYMENT_REPORT, param);
  }

  getPaymentExportData(param): Observable<any> {
    return this.restService.get(apiUrl.EXPORT_UTILITY, param, 'blob');
  }
}
