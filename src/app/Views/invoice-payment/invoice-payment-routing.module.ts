import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { Language } from 'src/app/Other/translation.constants';

const routes: Routes = [{
  path: '',
  data: {
    title: Language.TAB.Admin_Section,
    status: false
  },
  children: [
    {
      path: '',
      redirectTo: 'invoice-payment',
      pathMatch: 'full'
    },
    {
      path: 'invoice',
      loadChildren: () => import('./invoice/invoice.module').then(m => m.InvoiceModule)
    },
    {
      path: 'payment',
      loadChildren: () => import('./payment/payment.module').then(m => m.PaymentModule)
    },
    {
      path: 'invoice-details',
      loadChildren: () => import('./invoice-details/invoice-details.module').then(m => m.InvoiceDetailsModule)
    }
  ]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class InvoicePaymentRoutingModule { }