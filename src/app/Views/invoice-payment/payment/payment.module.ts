import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PaymentRoutingModule } from './payment-routing.module';
import { PackagedModule } from '../../../../app/Packaged/packaged.module';
import { PaymentComponent } from './payment.component';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';

@NgModule({
  declarations: [PaymentComponent],
  imports: [
    CommonModule,
    PackagedModule,
    PaymentRoutingModule,
    OwlDateTimeModule, 
    OwlNativeDateTimeModule
  ]
})
export class PaymentModule { }
