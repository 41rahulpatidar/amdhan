import { Component, OnInit,ChangeDetectorRef  } from '@angular/core';
import { Language } from '../../../Other/translation.constants';
import { NgxSpinnerService } from 'ngx-spinner';
import { UtilService } from 'src/app/Service/util.service';
import { varConstants } from 'src/app/Other/variable.constants';
import { InvoicePaymentService } from '../invoice-payment.service';
import { AlertService } from 'src/app/Service/alert.service';
import { Router } from '@angular/router';
import { map, tap } from 'rxjs/operators';
import {DatePipe} from '@angular/common';
import { saveAs as download } from "file-saver";

@Component({
  selector: 'app-invoice',
  templateUrl: './invoice.component.html',
  styleUrls: ['./invoice.component.scss']
})
export class InvoiceComponent implements OnInit {

	label: any = Language;
  invoiceArray: any = [{fieldname: "demo"}];
  dtOptions: any;
  userData: any;
  currentSection: any;
  headerVisible: boolean = false;
  datatable: any;
  isMenuAccess: boolean = true;
  isAddEditPermission: boolean = true;
  inquiryFilter: any = {
    FromDate: new Date(),
    ToDate: new Date(),
    Keyword: ''
  }
  pipe = new DatePipe('en-US');

  constructor(
    private router: Router,
    private spinner: NgxSpinnerService,
    private util: UtilService,
    private invoicePaymentService: InvoicePaymentService,
    private alertService: AlertService,
    private chRef: ChangeDetectorRef) { 
    this.dtOptions = this.util.dtOptions;
  }

  ngOnInit() {
    let currentDate = new Date();
    let previousDate = currentDate.setFullYear(currentDate.getFullYear() - 1);
    this.inquiryFilter.FromDate = new Date(previousDate);
    this.userData = JSON.parse(localStorage.getItem("logged_in_user_details"));
    this.currentSection = JSON.parse(localStorage.getItem("current_section"));
    this.util.tableHorizontalScroll('invoice_header_table');
    this.getInvoiceHeaderData();

    //**** User Menu Access Start ****//
    if(this.isMenuAccess == false){
      this.router.navigate(["/dashboard"]);
    }
    if(this.userData.RoleType == '2'){
      this.isAddEditPermission = false;
    }
    if(this.userData.RoleType == '3'){
      if(this.userData.Lst_RoleData1[0].ApprovalLevel == 'L01'){
        this.isAddEditPermission = false;
      }else{
        this.isAddEditPermission = false;
      }
    }

    if(this.headerVisible){
      this.headerVisible =  this.isAddEditPermission;
    }
    //**** User Menu Access End ****//

  }

  getInvoiceHeaderData() {
      this.spinner.show();
      let param = {
        FromDate: this.pipe.transform(this.inquiryFilter.FromDate,'yyyy-MM-dd'),
        ToDate: this.pipe.transform(this.inquiryFilter.ToDate,'yyyy-MM-dd'),
        Keyword: this.inquiryFilter.Keyword
      }
      this.invoicePaymentService.getInvoiceHeaderData(param)
      .pipe(
        map(data => {
          return data;
        }),
      )
        .subscribe((data: any) => {
          this.spinner.hide();
          if (Array.isArray(data)) {
            if ($.fn.dataTable.isDataTable('#invoice_header_table')) {
              $('#invoice_header_table').DataTable().clear().destroy();               
            }
            this.invoiceArray = data;
            this.chRef.detectChanges();
            const table: any = $('table');
            this.datatable = table.DataTable();
          }
        }, error => {
          this.spinner.hide()
          this.alertService.error(Language.ALERT.Title_Error, Language.ALERT.Went_Wrong_Message);
       });
  } 

  getInvoiceExportData(){
    this.spinner.show();
      let param = {
        FromDate: this.pipe.transform(this.inquiryFilter.FromDate,'yyyy-MM-dd'),
        ToDate: this.pipe.transform(this.inquiryFilter.ToDate,'yyyy-MM-dd'),
        Keyword: this.inquiryFilter.Keyword,
        Status: 0,
        DocumentType: 'Invoice'
      }
    let filepath = 'Inquiry-Header.xlsx';
  
    this.invoicePaymentService.getInvoiceExportData(param).subscribe((data: any) => {
      this.spinner.hide();
      if (data) {
        var blob = new Blob([data], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8' });
        download(blob, 'Invoice_Header_Export' + new  Date().getTime() + '.xlsx');
      }
      else {
        this.alertService.error(Language.ALERT.Title_Error, Language.ALERT.Went_Wrong_Message);
      }

    }, error => {
      console.log(error, 'error')
      this.spinner.hide();
      this.alertService.error(Language.ALERT.Title_Error, Language.ALERT.Went_Wrong_Message);
   });
  }
  
  viewInvoiceDetails(invoice) {
    localStorage.setItem('editInvoiceId', invoice.InvoiceHeaderID);
    invoice.from = "/invoice-payment/invoice";
    invoice.tab = this.label.INVOICE_PAYMENT_TAB.Invoice;
    invoice.mode = varConstants.isRegistered;
    this.router.navigate(["/invoice-payment/invoice-details"], { state: { data: invoice } });
  }

}
