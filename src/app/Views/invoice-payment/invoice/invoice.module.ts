import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { InvoiceRoutingModule } from './invoice-routing.module';
import { PackagedModule } from '../../../../app/Packaged/packaged.module';
import { InvoiceComponent } from './invoice.component';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';

@NgModule({
  declarations: [InvoiceComponent],
  imports: [
    CommonModule,
    PackagedModule,
    InvoiceRoutingModule,
    OwlDateTimeModule, 
    OwlNativeDateTimeModule
  ]
})
export class InvoiceModule { }
