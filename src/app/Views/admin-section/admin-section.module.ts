import { NgModule } from '@angular/core';
import { AdminSectionRoutingModule } from './admin-section-routing.module';


@NgModule({
  declarations: [],
  imports: [
    AdminSectionRoutingModule
  ]
})
export class AdminSectionModule { }
