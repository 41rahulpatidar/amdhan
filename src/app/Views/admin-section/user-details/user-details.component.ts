import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { AlertService } from 'src/app/Service/alert.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { UtilService } from 'src/app/Service/util.service';
import { Language } from '../../../Other/translation.constants';
import { varConstants } from 'src/app/Other/variable.constants';
import { AdminSectionService } from '../admin-section.service';
import { map, tap } from 'rxjs/operators';

@Component({
  selector: 'app-user-details',
  templateUrl: './user-details.component.html',
  styleUrls: ['./user-details.component.scss']
})
export class UserDetailsComponent implements OnInit {

  @ViewChild('addUserForm', { static: true }) addUserForm: any;
  selectedRoles = "";
  userDetails: any;
  usersDetailsArray: any = [];
  label: any = Language;
  varConstants: any = varConstants;
  isSelectAll: boolean = false;
  dtOptions: any;
  userData: any;
  tabsArray: any = [];
  roleArray: any = [];
  subtotal: 0;
  formMode: any = varConstants.Edit;
  isFormInAddMode: boolean = false;
  vendorID: any;
  showApproveBttn: boolean = true;
  addUserData: any = {
    Id: 0,
    FirstName: "",
    LastName: "",
    Username: "",
    Email: "",
    Password: ""
  }
  roleDetails: any = {};
  itemsDetails: any = {};
  RoleTypeArray: any = [];
  isUserIsAdminUser:any;
  isEdit: any = {
    userDetailsEditModeEnable: false
  };
  className = UserDetailsComponent.name;
  roles: any = [];
  isRolesSelected: boolean = false;
  isCustomerPortalDisabled: boolean = true;

  constructor(private router: Router,
    private alertService: AlertService,
    private spinner: NgxSpinnerService,
    private util: UtilService,
    private adminService: AdminSectionService
  ) {
    this.dtOptions = util.dtOptions;
  }

  ngOnInit() {
    this.userData = JSON.parse(localStorage.getItem("logged_in_user_details"));
    //If user is Third party then only we will show the buttons
    this.RoleTypeArray = [{ value: Number(varConstants.adminUser), type: varConstants.roletype1 }, { value: Number(varConstants.businessUser), type: varConstants.roletype2 }, { value: Number(varConstants.thirdParty), type: varConstants.roletype3 }];
    if (this.userData.RoleType == varConstants.adminUser){
        this.isUserIsAdminUser = true;
        //this.RoleTypeArray = [{ value: Number(varConstants.AdminRoleType), type: varConstants.roletype1 }, { value: Number(varConstants.BusinessUserRoleType), type: varConstants.roletype2 }];  
    }
    
    if (history.state.data) {
      this.tabsArray = [
        { id: "user-section1", name: this.label.TAB.Roles, isActiveClass: "active" },
      ]
      if (history.state.data['mode'] == varConstants.isRegistered) {
        this.userDetails = history.state.data;
        this.roleDetails['RoleTypeID'] = this.userDetails.RoleTypeID;
        //Once this comp loads this will fill the header
        this.getUserDetails(this.userDetails.Id);
      } else {
        this.enableForm('addUserForm');
        this.userDetails = history.state.data;
        this.showApproveBttn = false;
        this.isFormInAddMode = true;
        this.formMode = this.userDetails.mode;
        this.isEdit.userDetailsEditModeEnable = true;
        this.addUserData.IsCustomerPortalAccess = true;
        this.isCustomerPortalDisabled = true;
        setTimeout(() => {
          this.spinner.hide();
        }, 1500);
      }
    } else {
      this.router.navigate(['/dashboard']);
    }
  }

  onChangeTab(tabDetails: any) {
    for (let i = 0; i < this.tabsArray.length; i++) {
      if (tabDetails.id == this.tabsArray[i].id) {
        this.tabsArray[i].isActiveClass = "active";
      } else {
        this.tabsArray[i].isActiveClass = "";
      }
    }
  }

  getUserDetails(id) {
    //header
    this.adminService.getUserDataById(id)
      .subscribe((data: any) => {
        if (data) {
          this.addUserData = data;
          //show approve bttn for draft status record
          this.showApproveBttn = (this.addUserData['Status'] == varConstants.isDrafted) ? true : false;
          //get roles on the basis of roletypeid
          this.getRoleList(this.addUserData.RoleTypeID);
        }
      }, error => {
        this.spinner.hide();
        //this.alertService.error(Language.ALERT.Title_Error, Language.ALERT.Went_Wrong_Message);
        console.log(error, "error")
        this.util.PrintLogs(varConstants.ERROR, this.className, this.getRoleList.name, error);
      });
  }

  enableForm(form) {
    if (form == 'addUserForm') {
      this.isEdit.userDetailsEditModeEnable = true;
    }
  }

  saveFormDetails() {
    this.roles = [];
    let currentDate = new Date();
    for (let i = 0; i < this.roleArray.length; i++) {
      if (this.roleArray[i].isChecked) {
        this.roles.push(this.roleArray[i]);
      }
    }
    this.addUserData["UserRoleMapped"] = this.roles;
    this.addUserData['Client'] = varConstants.Client;
    this.addUserData['VendorID'] = this.userData.vendorID;
    this.addUserData['UpdateAt'] = currentDate;
    this.addUserData['UpdateBy'] = this.userData.Id;
    this.addUserData['SelectedRoles'] = this.selectedRoles.slice(0, -1);
    this.addUserData["CreatedBy"] = this.userData.Id;
    this.addUserData["CreatedDate"] = this.userData.CreatedDate ? this.userData.CreatedDate : currentDate;
    this.addUserData["RoleTypeID"] = Number(this.roleDetails.RoleTypeID);
    this.addUserData["Status"] = this.addUserData["Status"] ? this.addUserData["Status"] : 0;
    this.addUserData['IsActive'] = this.addUserData['IsActive'] ? true : false;
    this.addUserData['IsCustomerPortalAccess'] = this.addUserData['IsCustomerPortalAccess'] ? true : false;
    this.saveUserDetails();
  }

  getRoleList(RoleTypeID) {
    this.roleArray = [];
    this.spinner.show();
    let param = {
      IsCustomerPortalAccess: true
    }
    this.adminService.getRoleData(param).pipe(
      map(data => {
        //filter on the basis of role type selection
        let newData = data.filter(result => result.RoleTypeID == RoleTypeID)
        for (const property in newData) {
          newData[property]["isChecked"] = false;
          //if(property == 'id' && newData['id'] == this.userData.Sele)
          switch (newData[property]["RoleTypeID"]) {
            case Number(varConstants.adminUser):
              newData[property]["RoleTypeDesc"] = Language.AdminRoleType;
              break;
            case Number(varConstants.businessUser):
              newData[property]["RoleTypeDesc"] = Language.BusinessUserRoleType;
              break;
            case Number(varConstants.thirdParty):
              newData[property]["RoleTypeDesc"] = Language.ThirdPartyRoleType;
              break;
            default:
              newData[property]["RoleTypeDesc"] = newData[property]["RoleTypeID"];
          }
        }
        return newData;
      })
    )
      .subscribe((data: any) => {
        this.spinner.hide();
        if (Array.isArray(data)) {
          this.roleArray = data;

          //to make row selected
          if (this.addUserData.UserRoleMapped) {
            this.addUserData.UserRoleMapped.forEach(element => {
              this.onSelectRow(this.roleArray.findIndex(x => x.Id === element.RoleId));
            });
          }

        }
      }, error => {
        this.spinner.hide();
        this.alertService.error(Language.ALERT.Title_Error, Language.ALERT.Went_Wrong_Message);
        this.util.PrintLogs(varConstants.ERROR, this.className, this.getRoleList.name, error);
      });
  }

  onSelectRow(index) {
    if (index > -1) {
      if (this.roleArray[index].isChecked) {
        this.roleArray[index].isChecked = false;
      } else {
        this.roleArray[index]['isChecked'] = true;
      }
      this.isRoleSelected();
    }

  }

  saveUserDetails() {
    if (this.formMode == varConstants.Edit) {//If form is in edit mode it will go for update
      this.isFormInAddMode = false;
      this.spinner.show()
      this.adminService.updateUserDetails(this.addUserData.Id, this.addUserData).subscribe((data: any) => {
        if (data) {
          this.isEdit.userDetailsEditModeEnable = false;
          this.alertService.success(Language.ALERT.Title_Success, Language.ALERT.Saved_Message);
          this.util.PrintLogs(varConstants.INFO, this.className, this.saveUserDetails.name, this.formMode + ' | ' + data);
        }
        else {
          this.alertService.success(Language.ALERT.Title_Success, Language.ALERT.Saved_Message);
          this.util.PrintLogs(varConstants.INFO, this.className, this.saveUserDetails.name, this.formMode + ' | ' + data);
          this.router.navigate([this.userDetails.from]);
        }
        this.spinner.hide()
      }, error => {
        this.spinner.hide()
        this.alertService.error(Language.ALERT.Title_Error, Language.ALERT.Went_Wrong_Message);
        this.util.PrintLogs(varConstants.ERROR, this.className, this.saveUserDetails.name, this.formMode + ' | ' + error);
      });
    } else {//It will go for save
      this.isFormInAddMode = true;
      this.spinner.show()
      this.adminService.saveUserDetails(this.addUserData).subscribe((data: any) => {
        if (data) {
          this.isEdit.userDetailsEditModeEnable = false;
          this.isFormInAddMode = false;
          this.formMode = varConstants.Edit;
          this.addUserData.Id = data.Id;
          this.addUserData = data;
          this.alertService.success(Language.ALERT.Title_Success, Language.ALERT.Saved_Message);
          this.util.PrintLogs(varConstants.INFO, this.className, this.saveUserDetails.name, this.formMode + ' | ' + data);
        } else {
          this.alertService.success(Language.ALERT.Title_Success, Language.ALERT.Saved_Message);
          this.util.PrintLogs(varConstants.INFO, this.className, this.saveUserDetails.name, this.formMode + ' | ' + data);
          this.router.navigate([this.userDetails.from]);
        }
        this.spinner.hide();
      }, error => {
        this.spinner.hide();
        if (error.status == 300) {
          this.alertService.error(Language.ALERT.Title_Error, Language.ALERT.EmailOccupiedAlready);
          this.util.PrintLogs(varConstants.ERROR, this.className, this.saveUserDetails.name, this.formMode + ' | ' + error);
        } else {
          this.alertService.error(Language.ALERT.Title_Error, Language.ALERT.Went_Wrong_Message);
          this.util.PrintLogs(varConstants.ERROR, this.className, this.saveUserDetails.name, this.formMode + ' | ' + error);
        }
      });
    }
  }

  isRoleSelected() {
    this.isRolesSelected = false;
    this.selectedRoles = "";
    this.roleArray.forEach(element => {
      if (element.isChecked == true) {
        this.isRolesSelected = true;
        this.selectedRoles = this.selectedRoles + "" + element.Id + ","
      }
    });
  }

  onRoleTypeChange() {
    this.getRoleList(this.roleDetails.RoleTypeID);
    this.roleArray.forEach(element => {
      this.isRolesSelected = false;
    });
  }

  onClickApproved() {
    let approveObj = { "profileId": this.userDetails.Id, 
                      "status": varConstants.UserProfileAccept, 
                      "userName": (this.userData.Vendor_Details['Username']) ? this.userData.Vendor_Details['Username'] : "" };
                      this.spinner.show();
      this.adminService.approveUser(approveObj).subscribe((data: any) => {
        if (data) {
          if(data == varConstants.isProfileApproved){
            this.alertService.success(Language.ALERT.Title_Success, Language.ALERT.Record_Approved_Sucess_Message);
            this.util.PrintLogs(varConstants.INFO, this.className, this.onClickApproved.name, this.formMode + ' | ' + data);
            this.getUserDetails(this.userDetails.Id);
          }
        } 
        this.spinner.hide();
      }, error => {
        this.spinner.hide();
          this.alertService.error(Language.ALERT.Title_Error, Language.ALERT.Went_Wrong_Message);
          this.util.PrintLogs(varConstants.ERROR, this.className, this.onClickApproved.name, this.formMode + ' | ' + error);
      });
}
}
