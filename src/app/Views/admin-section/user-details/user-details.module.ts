import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UserDetailsRoutingModule } from './user-details-routing.module';
import { UserDetailsComponent } from './user-details.component';
import { PackagedModule } from 'src/app/Packaged/packaged.module';


@NgModule({
  declarations: [UserDetailsComponent],
  imports: [
    CommonModule,
    PackagedModule,
    UserDetailsRoutingModule
  ]
})
export class UserDetailsModule { }
