import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { Language } from 'src/app/Other/translation.constants';


const routes: Routes = [{
  path: '',
  data: {
    title: Language.TAB.Admin_Section,
    status: false
  },
  children: [
    {
      path: '',
      redirectTo: 'users',
      pathMatch: 'full'
    },
    {
      path: 'users',
      loadChildren: () => import('./users/users.module').then(m => m.UsersModule)
    },
    {
      path: 'roles',
      loadChildren: () => import('./roles/roles.module').then(m => m.RolesModule)
    },
    {
      path: 'user-details',
      loadChildren: () => import('./user-details/user-details.module').then(m => m.UserDetailsModule)
    },
  ]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminSectionRoutingModule { }
