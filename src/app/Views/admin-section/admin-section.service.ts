import { Injectable } from '@angular/core';
import { RestApiService } from 'src/app/Service/rest-api.service';
import { apiUrl } from 'src/app/Other/apiUrl.constants';
import { Observable } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class AdminSectionService {

  constructor(private restService: RestApiService) {
  
  }
  getUserData(data): Observable<any> {
    return this.restService.get(apiUrl.USER_PROFILE, data);
  }
  getRoleData(param): Observable<any> {
    return this.restService.get(apiUrl.USER_ROLES, param);
  }
  deleteUser(data): Observable<any> {
    return this.restService.delete(apiUrl.USER_PROFILE+'/'+data, data);
  }
  deleteRole(data): Observable<any> {
    return this.restService.delete(apiUrl.USER_ROLES+'/'+data, data);
  }
  saveUserDetails(data){
    return this.restService.post(apiUrl.USER_PROFILE+ '?SelectedRoles=' + data.SelectedRoles, data);
  }
  updateUserDetails(Id, data){
    return this.restService.post(apiUrl.USER_PROFILE_UPDATE + '?id=' + Id, data);
  }
  getUserDataById(Id) {
    return this.restService.get(apiUrl.USER_PROFILE + '/' + Id);
  }
  saveRoleDetails(data){
    return this.restService.post(apiUrl.USER_ROLES, data);
  }
  updateRoleDetails(Id, data){
    return this.restService.put(apiUrl.USER_ROLES + '/' + Id, data);
  }
  
  approveUser(data): Observable<any> {
    return this.restService.post(apiUrl.USER_PROFILE_ACCEPT_REJECT+'?profileId='+data.profileId+'&status='+data.status+'&userName='+data.userName, data);
  }
  // getGoodReceiptData(): Observable<any> {
  //   return this.restService.get(apiUrl.ASN_GOODRECEIPT_LIST);
  // }
}
