import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UsersRoutingModule } from './users-routing.module';
import { UsersComponent } from './users.component';
import { PackagedModule } from '../../../../app/Packaged/packaged.module';


@NgModule({
  declarations: [UsersComponent],
  imports: [
    CommonModule,
    PackagedModule,
    UsersRoutingModule
  ]
})
export class UsersModule { }
