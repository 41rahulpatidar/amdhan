import { Component, OnInit } from '@angular/core';
import { Language } from '../../../Other/translation.constants';
import { varConstants } from '../../../Other/variable.constants';
import { NgxSpinnerService } from 'ngx-spinner';
import { AlertService } from '../../../Service/alert.service';
import { Router } from '@angular/router';
import { UtilService } from '../../../Service/util.service';
import { AdminSectionService } from '../admin-section.service';
import { map, tap } from 'rxjs/operators';
@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit {
  label: any = Language;
  usersArray: any = [];
  roleArray: any = [];
  dtOptions: any;
  isSelectAll: boolean = false;
  className = UsersComponent.name;
  vendorID: any;
  tabsArray: any [];
  userDetails: any = {};
  formMode: any;
  roleDetails: any = {
    Id: 0,
    RoleName: "",
    Description: "",
    RoleTypeID: "",
    ApprovalLevel: "",
    IsCustomerPortal: true
  };
  userData:  any = {};
  RoleTypeArray: any = [{ value: 1, type: varConstants.roletype1}, {value: 2, type: varConstants.roletype2 }, {value: 3, type: varConstants.roletype3 }];
  approvalLevelArray: any = ["L01", "L02", "L03"];
  constructor(private router: Router,
    private alertService: AlertService,
    private spinner: NgxSpinnerService,
    private adminService: AdminSectionService,
    private util: UtilService,
  ) {
    this.dtOptions = util.dtOptions;
    this.spinner.show();
    let isVendorIDRecevied = new Promise((resolve,reject)=>{
        return resolve(this.util.getLoggedInVendorID());
    });
    isVendorIDRecevied.then(res=>{
      this.vendorID = res;
    }).catch(res=>{
      this.spinner.hide()
      this.alertService.error(Language.ALERT.Title_Error, Language.ALERT.Went_Wrong_Message);
      this.util.PrintLogs(varConstants.ERROR, this.className, this.ngOnInit.name, res);
    });
  }

  ngOnInit() {
    this.userData = JSON.parse(localStorage.getItem("logged_in_user_details"));
    this.getUserList();
    this.getRoleList();
    this.tabsArray = [
      { id: "section1", name: this.label.SUB_TAB.Users, isActiveClass: "active" },
      { id: "section2", name: this.label.SUB_TAB.Roles, isActiveClass: "" },
    ];
  }

  onChangeTab(tabDetails: any) {
    for (let i = 0; i < this.tabsArray.length; i++) {
      if (tabDetails.id == this.tabsArray[i].id) {
        this.tabsArray[i].isActiveClass = "active";
      } else {
        this.tabsArray[i].isActiveClass = "";
      }
    }
  }

  openModal(id, data) {
    this.roleDetails = {};
    if(data) {
      this.roleDetails = data;
      this.formMode = varConstants.Edit;
    }else{
      this.roleDetails.IsCustomerPortal = true;
    }
    document.getElementById(id).style.display = "block";
    document.getElementById(id).classList.add("show");
    document.body.classList.add("modal-open");
  }

  onClickOK() {
    if(this.roleDetails.RoleName != "" && this.roleDetails.Description != "" && this.roleDetails.RoleTypeID != "" && this.roleDetails.ApprovalLevel != "") {
      let date = new Date();
      this.roleDetails["Id"] = this.roleDetails["Id"] ? this.roleDetails["Id"] : 0;
      this.roleDetails['UpdatedDate'] = date;
      this.roleDetails['UpdatedBy'] = this.userData.Id;
      this.roleDetails["CreatedBy"] = this.userData["CreatedBy"] ? this.userData["CreatedBy"] : this.userData.Id;
      this.roleDetails["CreatedDateTime"] = this.roleDetails.CreatedDate ? this.roleDetails.CreatedDate : date;
      this.roleDetails["IsCustomerPortal"] = this.roleDetails.IsCustomerPortal;
      this.saveRoleData();
    }
  }

  getUserList() {
    this.usersArray = [];
    this.spinner.show();
    let param = {
      IsCustomerPortalAccess: true
    }
    this.adminService.getUserData(param)
    .pipe(
      tap(data => { 
        this.util.PrintLogs(varConstants.INFO,this.className,this.getUserList.name,data);  
      }),
      map(data => {
        let newData;
        //If vendor ID is not null it will filter the data vendor accordingly
        if (this.userData.RoleType == varConstants.thirdParty){
          if(this.vendorID){
            data = data.filter(result => result.Vendor == this.vendorID)  
          }
        }
        newData = data;
        for (const property in newData) {
          switch (newData[property]["Status"]) {
            case varConstants.isDrafted:
              newData[property]["StatusToDisplay"] = Language.Draft;
              break;
            case varConstants.isApproved:
              newData[property]["StatusToDisplay"] = Language.Approved;
              break;
            default:
              newData[property]["StatusToDisplay"] = newData[property]["Status"];
          }
        }
        return newData;
      })
    )
    .subscribe((data: any) => {
      if (Array.isArray(data)) {
        this.usersArray = data;
        setTimeout(()=>{
          this.spinner.hide();
        }, 50);
      }
    }, error => {
      this.spinner.hide();
      //this.alertService.error(Language.ALERT.Title_Error, Language.ALERT.Went_Wrong_Message);
      console.log(error, "error")
      this.util.PrintLogs(varConstants.ERROR, this.className, this.getUserList.name, error);
    });
  }

  getRoleList() {
    this.roleArray = [];
    this.spinner.show()
    let param = {
      IsCustomerPortalAccess: true
    }
    this.adminService.getRoleData(param)
    .pipe(
      tap(data => { 
        this.util.PrintLogs(varConstants.INFO,this.className,this.getRoleList.name,data);  
      }),
      map(data => {
        let newData;
       //If vendor ID is not null it will filter the data vendor accordingly
       if (this.userData.RoleType == varConstants.thirdParty){
        if(this.vendorID){
          data = data.filter(result => result.Vendor == this.vendorID)  
        }
      }
      newData = data;
        return newData;
      }),
    )
    .subscribe((data: any) => {
      if (Array.isArray(data)) {
        this.roleArray = data;
        setTimeout(()=>{
          this.spinner.hide();
        }, 50);
      }
    }, error => {
      this.spinner.hide();
      //this.alertService.error(Language.ALERT.Title_Error, Language.ALERT.Went_Wrong_Message);
      console.log(error, "error")
      this.util.PrintLogs(varConstants.ERROR, this.className, this.getRoleList.name, error);
    });
  }

  getRoleType(id) {
      return varConstants['roletype' + id];
  }

  refreshData(){
    this.usersArray = [];
    this.roleArray = [];
    this.getUserList();
    this.getRoleList();
  }

  onSelectAll(isSelectAll) {
    if (isSelectAll == false) {
      this.isSelectAll = true;
      for (let i = 0; i < this.roleDetails.length; i++) {
        this.roleDetails[i].isChecked = true;
      }
    } else {
      this.isSelectAll = false;
      for (let i = 0; i < this.roleDetails.length; i++) {
        this.roleDetails[i].isChecked = false;
      }
    }
  }

  onDeleteUser(user) {
    let isConfirm = this.alertService.warning('', Language.ALERT.Confirm_Delete_Message);
    isConfirm.then((result: any) => {
      if (result.value) {
        this.spinner.show()
        this.adminService.deleteUser(user.Id).subscribe((data: any) => {
          if (data) {
            this.alertService.success(Language.ALERT.Title_Success, Language.ALERT.Delete_Message);
            this.util.PrintLogs(varConstants.INFO, this.className, this.onDeleteUser.name, data);
            this.refreshData();
          } else {
            this.alertService.error(Language.ALERT.Title_Error, Language.ALERT.Went_Wrong_Message);
            this.util.PrintLogs(varConstants.ERROR, this.className, this.onDeleteUser.name, data);
          }
          this.spinner.hide()
        }, error => {
          this.spinner.hide()
          this.alertService.error(Language.ALERT.Title_Error, Language.ALERT.Went_Wrong_Message);
          this.util.PrintLogs(varConstants.ERROR, this.className, this.onDeleteUser.name, error);
        });
      }
    });
  }

  onDeleteRole(role){
    let isConfirm = this.alertService.warning('', Language.ALERT.Confirm_Delete_Message);
    isConfirm.then((result: any) => {
      if (result.value) {
        this.spinner.show()
        this.adminService.deleteRole(role.Id).subscribe((data: any) => {
          if (data) {
            this.alertService.success(Language.ALERT.Title_Success, Language.ALERT.Delete_Message);
            this.util.PrintLogs(varConstants.INFO, this.className, this.onDeleteRole.name, data);
            this.refreshData();
          } else {
            this.alertService.error(Language.ALERT.Title_Error, Language.ALERT.Went_Wrong_Message);
            this.util.PrintLogs(varConstants.ERROR, this.className, this.onDeleteRole.name, data);
          }
          this.spinner.hide()
        }, error => {
          this.spinner.hide()
          this.alertService.error(Language.ALERT.Title_Error, Language.ALERT.Went_Wrong_Message);
          this.util.PrintLogs(varConstants.ERROR, this.className, this.onDeleteRole.name, error);
        });
      }
    });
  }

  viewUserDetails(user) {
    user.from = "/admin-section/users";
    user.tab = this.label.SUB_TAB.User_Managemnet;
    user.mode = varConstants.isRegistered;
    this.router.navigate(["/admin-section/user-details"], { state: { data: user } });
  }

  adduser() {
    let user: any = {};
    user.from = "/admin-section/users";
    user.tab = this.label.SUB_TAB.User_Managemnet;
    user.mode = varConstants.isNew;
    this.router.navigate(["/admin-section/user-details"], { state: { data: user } });
  }
  saveRoleData() {
    if(this.roleDetails["Id"]){//If form is in edit mode it will go for update
      this.spinner.show()
      this.adminService.updateRoleDetails(this.roleDetails.Id, this.roleDetails).subscribe((data: any) => {
        if (data) {
          this.alertService.success(Language.ALERT.Title_Success, Language.ALERT.Saved_Message);
          this.util.PrintLogs(varConstants.INFO, this.className, this.saveRoleData.name, this.formMode+' | '+data);
          this.refreshData();
          this.closeModal('add-role-modal');
        }
        else{
          this.alertService.success(Language.ALERT.Title_Success, Language.ALERT.Saved_Message);
          this.util.PrintLogs(varConstants.INFO, this.className, this.saveRoleData.name, this.formMode+' | '+data);
          this.refreshData();
          this.closeModal('add-role-modal');
        }
        this.spinner.hide()
      }, error => {
        if(error.status == 204) {
          this.alertService.success(Language.ALERT.Title_Success, Language.ALERT.Saved_Message);
          this.util.PrintLogs(varConstants.INFO, this.className, this.saveRoleData.name, this.formMode+' | '+error);
          this.closeModal('add-role-modal');
        } else{
          this.spinner.hide()
          this.alertService.error(Language.ALERT.Title_Error, Language.ALERT.Went_Wrong_Message);
          this.util.PrintLogs(varConstants.ERROR, this.className, this.saveRoleData.name, this.formMode+' | '+error);
        }
      });
    }else{//It will go for save
      this.spinner.show()
      this.adminService.saveRoleDetails(this.roleDetails).subscribe((data: any) => {
        if (data) {
          this.formMode = varConstants.Edit;
          this.roleDetails.Id = data.Id;
          this.roleDetails = data;
          this.alertService.success(Language.ALERT.Title_Success, Language.ALERT.Saved_Message);
          this.util.PrintLogs(varConstants.INFO, this.className, this.saveRoleData.name, this.formMode+' | '+data);
          this.refreshData();
          this.closeModal('add-role-modal');
        } else {
          this.alertService.success(Language.ALERT.Title_Success, Language.ALERT.Saved_Message);
          this.util.PrintLogs(varConstants.INFO, this.className, this.saveRoleData.name, this.formMode+' | '+data);
          this.closeModal('add-role-modal');
        }
        this.spinner.hide();
      }, error => {
          this.spinner.hide();
          this.alertService.error(Language.ALERT.Title_Error, Language.ALERT.Went_Wrong_Message);
          this.util.PrintLogs(varConstants.ERROR, this.className, this.saveRoleData.name, this.formMode+' | '+error);
      });
    }
  }
  closeModal(id) {
    document.getElementById(id).style.display = "none";
    document.getElementById(id).classList.remove("show");
    document.body.classList.remove("modal-open");
   
  }
}
