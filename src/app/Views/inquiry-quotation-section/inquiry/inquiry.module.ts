import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { InquiryRoutingModule } from './inquiry-routing.module';
import { PackagedModule } from '../../../../app/Packaged/packaged.module';
import { InquiryComponent } from './inquiry.component';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';

@NgModule({
  declarations: [InquiryComponent],
  imports: [
    CommonModule,
    PackagedModule,
    InquiryRoutingModule,
    OwlDateTimeModule, 
    OwlNativeDateTimeModule
  ]
})
export class InquiryModule { }
