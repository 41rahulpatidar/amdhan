import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MastersSectionRoutingModule } from './inquiry-quotation-section-routing.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    MastersSectionRoutingModule,
  ]
})
export class InquiryQuotationSectionModule { }
