import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { AlertService } from 'src/app/Service/alert.service';
import { Language } from '../../../Other/translation.constants';
import { varConstants } from 'src/app/Other/variable.constants';
import { NgxSpinnerService } from 'ngx-spinner';
import { UtilService } from 'src/app/Service/util.service';
import { InquiryQuotationService } from '../inquiry-quotation.service';
import { IDropdownSettings } from 'ng-multiselect-dropdown';
import {DatePipe} from '@angular/common';
import { saveAs as download } from "file-saver";

@Component({
  selector: 'app-inquiry-details',
  templateUrl: './inquiry-details.component.html',
  styleUrls: ['./inquiry-details.component.scss']
})
export class InquiryDetailsComponent implements OnInit {
  

  @ViewChild('addInquiryForm', { static: true }) addInquiryForm: any;
  inquiryDetails: any = {};
  userData: any;
  tabsArray: any = [];
  label: any = Language;
  isEdit: any = {
    inquiryDetailsEditModeEnable: false
  };
  addInquiryData: any = {
    fieldname: "",
    FirstName: "",
    LastName: "",
    Username: "",
    Email: "",
    Password: ""
  }
  inquiryLineItemsDetails: any = [];
  itemsDetails:any = {};
  viewModeField: boolean = false;
  editInquiryId: any = '';
  dropdownStatusList: any = [];
  selectedStatusItems: any = [];
  dropdownSettingsStatus:IDropdownSettings;

  invoiceItemDetails: any = [];
  editInquiryItemIndex: any = '';
  documentDetails: any = {};
  invoiceDocumentDetails: any = [];
  editDocumentItemIndex: any = '';
  validToMin: any;

  soldToPartyList: any = [];
  selectedSoldToPartyItems: any = [];
  dropdownSoldToPartySettings: any = {};

  shipToPartyList: any = [];
  selectedShipToPartyItems: any = [];
  dropdownShipToPartySettings: any = {};

  sdDocCurrencyList: any = [];
  selectedSdDocCurrencyItems: any = [];
  dropdownSdDocCurrencySettings: any = {};

  salesOrganizationList: any = [];
  selectedSalesOrgItem: any = [];
  salesOrgSetting: any = {};

  materilMasterList: any = [];
  materialMasterSelectItem: any = [];
  materialMasterSetting: any = {};

  invoiceItemSelectedItem: any = [];
  invocieItemSetting: any = {};

  formSaveErrorMsg: any = '';
  pipe = new DatePipe('en-US');
  inquiryDetailsLabel : any = '';
  currentDate: any = new Date();
  currentSection: any;
  visibaleButton: boolean= true;
  selectedFile: any = '';
  inquiryLineItemForm: any = {};
  inquiryDocumentForm: any = {};
  isMenuAccess: boolean = true;
  isAddEditPermission: boolean = true;
  showApproveBttn: boolean = false;
  options: any = {}
  inquiryExportData: any = [];
  questionsList: any = [];
  quizeDetails: any = {};

  constructor(private router: Router,
    private alertService: AlertService,
    private spinner: NgxSpinnerService,
    private inquiryQuote: InquiryQuotationService,
    private util: UtilService,) { }

  ngOnInit() {
    this.options = {
      fieldSeparator: ',',
      quoteStrings: '"',
      decimalseparator: '.',
      showLabels: true,
      headers: [],
      showTitle: true,
      title: 'Inquiry Details',
      useBom: false,
      removeNewLines: true
    };

    this.inquiryLineItemForm = {
      submitted: false
    }
    this.inquiryDocumentForm = {
      submitted: false
    }
    this.currentSection = JSON.parse(localStorage.getItem("current_section"));
    if(this.currentSection.type == 'CloseInquiry'){
      this.visibaleButton = false;
    }
    this.inquiryDetailsLabel = this.label.TAB.Inquiry_Details;
    this.selectedStatusItems = [ { id: 0, text: 'Draft' }];
    this.getCustomerMasterList();
    this.dropdownStatusList = [
      { id: 0, text: 'Draft' },
      { id: 1, text: 'Submited' },
    ];
    this.sdDocCurrencyList = [{id: 'INR', text: 'INR'}];
    this.selectedSdDocCurrencyItems = [{id: 'INR', text: 'INR'}];
    this.dropdownSettingsStatus = {
      singleSelection: true,
      idField: 'id',
      textField: 'text',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      allowSearchFilter: true,
      closeDropDownOnSelection: true
    };

    this.materialMasterSetting = {
      singleSelection: true,
      idField: 'Material_ID',
      textField: 'MaterialNumber',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      allowSearchFilter: true,
      closeDropDownOnSelection: true
    }
    this.dropdownSoldToPartySettings = {
      singleSelection: true,
      idField: 'CustomerNumber',
      textField: 'CustomerName',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      allowSearchFilter: true,
      closeDropDownOnSelection: true
    };

    this.dropdownShipToPartySettings = {
      singleSelection: true,
      idField: 'BusinessPartnerCustomerNo',
      textField: 'CustomerName',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      allowSearchFilter: true,
      closeDropDownOnSelection: true
    };

    this.dropdownSdDocCurrencySettings = {
      singleSelection: true,
      idField: 'id',
      textField: 'text',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      itemsShowLimit: 3,
      allowSearchFilter: true,
      closeDropDownOnSelection: true
    }

    this.salesOrgSetting = {
      singleSelection: true,
      idField: 'SalesOrg',
      textField: 'SalesOrg',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      itemsShowLimit: 3,
      allowSearchFilter: true,
      closeDropDownOnSelection: true
    }

    this.invocieItemSetting = {
      singleSelection: true,
      idField: 'SalesInquiryItemID',
      textField: 'Item',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      itemsShowLimit: 3,
      allowSearchFilter: true,
      closeDropDownOnSelection: true
    }

    this.editInquiryId = localStorage.getItem('editInquiryId');
    
    console.log(this.editInquiryId, 'edit enquiry')
    this.itemsDetails.Material_Number = '';
    this.userData = JSON.parse(localStorage.getItem("logged_in_user_details"));
    if (history.state.data) {
      this.inquiryDetails = history.state.data;
      this.tabsArray = [
        { id: "inquiry-items-section1", name: this.label.TAB.Inquiry_Items, isActiveClass: "active" },
        { id: "inquiry-items-section2", name: this.label.TAB.Documents, isActiveClass: "" }
      ]
      if (history.state.data['mode'] == varConstants.isRegistered) {
        //this.inquiryDetails = history.state.data;
      } else {
        //this.enableForm('addInquiryForm');
      }
    } else {
      this.router.navigate(['/dashboard']);
    }
    //**** User Menu Access Start ****//
    if(this.isMenuAccess == false){
      this.router.navigate(["/dashboard"]);
    }

    if(this.userData.RoleType == '2'){
      this.isAddEditPermission = false;
    }

    if(this.userData.RoleType == '3'){
      if(this.userData.Lst_RoleData1[0].ApprovalLevel == 'L01'){
        this.isAddEditPermission = false;
      }else{
        this.isAddEditPermission = true;
      }
    }

    //**** User Menu Access End ****//
    this.getMaterialMasterList();
    this.getInvoiceDocumentDetails();
    setTimeout(()=> {
      this.inquiryDetails.CreatedDate = new Date();
    },500);
  }
  
  onChangeTab(tabDetails: any) {
    for (let i = 0; i < this.tabsArray.length; i++) {
      if (tabDetails.id == this.tabsArray[i].id) {
        this.tabsArray[i].isActiveClass = "active";
      } else {
        this.tabsArray[i].isActiveClass = "";
      }
    }
  }

  openModal(id) {
    this.questionsList = [];
    this.inquiryLineItemForm.submitted = false;
    this.inquiryDocumentForm.submitted = false;
    this.itemsDetails = {}
    document.getElementById(id).style.display = "block";
    document.getElementById(id).classList.add("show");
    document.body.classList.add("modal-open");
    if(id == 'add-inquiry-items-modal'){
      this.materialMasterSelectItem = [];
      this.editInquiryItemIndex = '';
      for (let i = 0; i <= 100; i++) {
        let item = (i+1)* 10;
        let tmp = this.invoiceItemDetails.find(list => list.Item == item);
        console.log(tmp, 'item', item, this.invoiceItemDetails)
        if(tmp == undefined){
          this.itemsDetails.Item = item;
          break;
        }
      }
    }

    if(id == 'add-document-items-modal'){
      this.editDocumentItemIndex = '';
      this.documentDetails = {};
    }
  }

  closeModal(id) {
    document.getElementById(id).style.display = "none";
    document.getElementById(id).classList.remove("show");
    document.body.classList.remove("modal-open");
  }

  addInquiryItem(event){
    console.log(this.quizeDetails);
    
    this.inquiryLineItemForm.submitted = true;
    if(!this.itemsDetails.Item || this.materialMasterSelectItem.length == 0 || !this.itemsDetails.ShortText || !this.itemsDetails.NetValue || !this.itemsDetails.OrderQuantity){
      return false;
    }
    let loginUserDetails = localStorage.getItem('logged_in_user_details');
    loginUserDetails = JSON.parse(loginUserDetails);
    if(this.itemsDetails['SalesInquiryItemID'] != undefined && this.itemsDetails['SalesInquiryItemID'] != '0' && this.itemsDetails['SalesInquiryItemID'] != 0){
    let itemDetails = {
                        "SalesInquiryItemID": this.itemsDetails['SalesInquiryItemID'],
                        "UniversalClientId": 0,
                        "CompanyCode": 0,
                        "Client": "",
                        "DocumentNo": "",
                        "InquiryNo": 0,
                        "Item": this.itemsDetails['Item'],
                        "MaterialNo": (this.materialMasterSelectItem.length == 0) ? '': this.materialMasterSelectItem[0]['Material_ID'],
                        "ShortText": this.itemsDetails['ShortText'],
                        "NetValue": this.itemsDetails['NetValue'],
                        "Currency": 0,
                        "OrderQuantity": Number(this.itemsDetails['OrderQuantity']),
                        "NetWeight": 0,
                        "WeightUnit": "",
                        "CreatedBy": loginUserDetails['FirstName']+' '+loginUserDetails['FirstName'],
                        "CreatedDate": this.pipe.transform(this.inquiryDetails.CreatedDate, 'yyyy-MM-dd'),
                        "UpdatedBy": "",
                        "Updateddate": "",
                        "SalesInquiryID": this.editInquiryId,
                        "Status": 0
                      }
    let paramQuize = []
    for(let key in this.quizeDetails){
      console.log(key, this.quizeDetails[key])
      let obj = {
         "QuestionAns_ID": 0,
        "Answer": this.quizeDetails[key],
        "Question_ID": key,
        "InsertedBy": this.userData.Id,
        "SalesInquiryItemID": this.itemsDetails['SalesInquiryItemID']
      }
      paramQuize.push(obj);
    }
    console.log(paramQuize, 'paramQuize')
      this.spinner.show();
       let param = {
        Inquiry: itemDetails,
        itemDetails: paramQuize
      }
      this.inquiryQuote.updateInquiryLineItemDetails(param,this.itemsDetails['SalesInquiryItemID']).pipe().subscribe((data: any) => {
        this.alertService.success(Language.ALERT.Success, Language.ALERT.Saved_Message);
        this.getSalesInquiryItemListById();
      }, error => {
        this.spinner.hide()
        this.alertService.error(Language.ALERT.Title_Error, Language.ALERT.Went_Wrong_Message);
      });
    }else{

      let itemDetails = {
                          "SalesInquiryItemID": 0,
                          "UniversalClientId": 0,
                          "CompanyCode": 0,
                          "Client": "",
                          "DocumentNo": "",
                          "InquiryNo": 0,
                          "Item": this.itemsDetails['Item'],
                          "MaterialNo": (this.materialMasterSelectItem.length == 0) ? '': this.materialMasterSelectItem[0]['Material_ID'],
                          "ShortText": this.itemsDetails['ShortText'],
                          "NetValue": this.itemsDetails['NetValue'],
                          "Currency": 0,
                          "OrderQuantity": this.itemsDetails['OrderQuantity'],
                          "NetWeight": 0,
                          "WeightUnit": "",
                          "CreatedBy": loginUserDetails['FirstName']+' '+loginUserDetails['FirstName'],
                          "CreatedDate": this.pipe.transform(this.inquiryDetails.CreatedDate, 'yyyy-MM-dd'),
                          "UpdatedBy": "",
                          "Updateddate": "",
                          "SalesInquiryID": this.editInquiryId,
                          "Status": 0
                        }
      let paramQuize = []
      for(let key in this.quizeDetails){
        console.log(key, this.quizeDetails[key])
        let obj = {
          "QuestionAns_ID": 0,
          "Answer": this.quizeDetails[key],
          "Question_ID": key,
          "InsertedBy": this.userData.Id,
          "SalesInquiryItemID": 0
        }
        paramQuize.push(obj);
      }
      let param = {
        Inquiry: itemDetails,
        itemDetails: paramQuize
      }
      this.spinner.show();
      this.inquiryQuote.addInquiryLineItemDetails(param).pipe().subscribe((data: any) => {
        this.alertService.success(Language.ALERT.Success, Language.ALERT.Saved_Message);
        this.getSalesInquiryItemListById();
      }, error => {
        this.spinner.hide()
        this.alertService.error(Language.ALERT.Title_Error, Language.ALERT.Went_Wrong_Message);
      });
    }
    this.closeModal(event);
  }

  editInquiryItem(inquiryLineItem, index) {
    this.questionsList = [];
    document.getElementById('add-inquiry-items-modal').style.display = "block";
    document.getElementById('add-inquiry-items-modal').classList.add("show");
    document.body.classList.add("modal-open");
    this.editInquiryItemIndex = index;
    this.itemsDetails['Item'] = inquiryLineItem.Item;
    // this.itemsDetails['MaterialNo'] = inquiryLineItem.MaterialNo;
    this.materialMasterSelectItem = [];
    let tmp = this.materilMasterList.find(list => list.Material_ID == inquiryLineItem.MaterialNo);
    if(tmp != undefined){
      this.materialMasterSelectItem.push(tmp);
      this.selectMaterialItem(tmp);
    }
    if(inquiryLineItem.SalesInquiryItemID != undefined && inquiryLineItem.SalesInquiryItemID != null && inquiryLineItem.SalesInquiryItemID != ''){
      this.itemsDetails['SalesInquiryItemID'] = inquiryLineItem.SalesInquiryItemID;
    }else{
      this.itemsDetails['SalesInquiryItemID'] = 0;
    }
    this.itemsDetails['ShortText'] = inquiryLineItem.ShortText;
    this.itemsDetails['NetValue'] = inquiryLineItem.NetValue;
    this.itemsDetails['OrderQuantity'] = inquiryLineItem.OrderQuantity;
    // this.itemsDetails['NetWeight'] = inquiryLineItem.NetWeight;
    // this.itemsDetails['WeightUnit'] = inquiryLineItem.WeightUnit;
  }
  removeInquiryItem(inquiryLineItem, index){
    this.invoiceItemDetails.splice(index,1);
  }
  addDcoumentDetails(event){
    this.inquiryDocumentForm.submitted = true;
    if(!this.selectedFile || this.invoiceItemSelectedItem.length == 0 || !this.documentDetails.DocRefNo || !this.documentDetails.DocumentName || !this.documentDetails.Doctype){
      return false;
    }
    const formData: FormData = new FormData();
    formData.append('FileData', this.selectedFile);
    let lineItem = (this.invoiceItemSelectedItem.length > 0) ? this.invoiceItemSelectedItem[0]['SalesInquiryItemID']: '';
    let param = 'DocumentID='+this.editInquiryId+'&LineItem='+lineItem+'&DocRefNo='+this.documentDetails.DocRefNo+'&DocumentName='+this.documentDetails.DocumentName+'&Doctype='+this.documentDetails.Doctype;
    this.spinner.show();
    this.inquiryQuote.addInvoiceDocumentDetails(formData, param).pipe().subscribe((data: any) => {
      this.alertService.success(Language.ALERT.Success, Language.ALERT.Saved_Message);
      this.getInvoiceDocumentDetails();
    }, error => {
      this.spinner.hide()
      this.alertService.error(Language.ALERT.Title_Error, Language.ALERT.Went_Wrong_Message);
    });
    this.closeModal(event);
  }

  getInvoiceDocumentDetails(){
    this.spinner.show();
    if(this.editInquiryId != null || this.editInquiryId != undefined){
      let param = {
          DocumentID: this.inquiryDetails.SalesInquiryID
      }
      this.inquiryQuote.getSalesInquiryDocumentById(param).pipe()
      .subscribe((data: any) => {
        this.spinner.hide();
        this.invoiceDocumentDetails = data;
        console.log(this.invoiceDocumentDetails, 'get')
      }, error => {
        this.spinner.hide()
        this.alertService.error(Language.ALERT.Title_Error, Language.ALERT.Went_Wrong_Message);
      });
    }
  }

  editDocumentItem(item, index) {
    document.getElementById('add-document-items-modal').style.display = "block";
    document.getElementById('add-document-items-modal').classList.add("show");
    document.body.classList.add("modal-open");
    this.editDocumentItemIndex = index;
    this.documentDetails = {};
    console.log(item, index, 'okkk')
    this.documentDetails['documentName'] = item.documentName;
  }

  getSingleInquiryDetails(){
    this.spinner.show();
    this.inquiryQuote.getSingleInquiryData(this.editInquiryId).pipe()
      .subscribe((data: any) => {
        this.spinner.hide();
        this.inquiryDetails = data;
        this.inquiryDetailsLabel = this.label.TAB.Inquiry_Details + '( '+this.inquiryDetails.SalesInquiryID+' )';
        if(this.inquiryDetails.Status == 1 || this.inquiryDetails.Status == 'Submited'){
          this.selectedStatusItems = [ { id: 1, text: 'Submited' }];
        }else{
          this.selectedStatusItems = [ { id: 0, text: 'Draft' }];
        }
        this.selectedSoldToPartyItems = [];
        let tmp = this.soldToPartyList.find(item => item.CustomerNumber == this.inquiryDetails.SoldToParty)
        if(tmp != undefined){
          this.selectedSoldToPartyItems.push(tmp);
          this.getCustomerDetails(tmp.CustomerNumber, 'Set-Value');
        }
        // this.invoiceItemDetails = this.inquiryDetails.SalesInquiryDetails;
        this.getSalesInquiryItemListById();
        console.log(this.invoiceItemDetails, 'get')
        this.getInvoiceDocumentDetails();
      }, error => {
        this.spinner.hide()
        this.alertService.error(Language.ALERT.Title_Error, Language.ALERT.Went_Wrong_Message);
        // this.util.PrintLogs(varConstants.ERROR, this.className, this.getSingleInquiryDetails.name, error);
      });
  }

  getSalesInquiryItemListById(){
    this.spinner.show();
    let param = {
      SalesInquiryID: this.inquiryDetails.SalesInquiryID
    }
    this.inquiryQuote.getSalesInquiryItemListById(param).pipe()
      .subscribe((data: any) => {
        this.spinner.hide();
        this.invoiceItemDetails = data;
        console.log(this.invoiceItemDetails, 'get')
        this.exportExcelData();
      }, error => {
        this.spinner.hide()
        this.alertService.error(Language.ALERT.Title_Error, Language.ALERT.Went_Wrong_Message);
        // this.util.PrintLogs(varConstants.ERROR, this.className, this.getSingleInquiryDetails.name, error);
      });
  }

  exportExcelData(){
    this.inquiryExportData = [];
    this.inquiryExportData = [
      {
        SoldToParty: this.label.Sold_To_Party,
        ShipToParty: this.label.Ship_to_Party,
        SalesOrg: this.label.Sales_Organization,
        DistributionChannel: this.label.Distribution_Channel,
        Division: this.label.Division,
        CreatedBy: this.label.Created_by,
        CreatedOn: this.label.Created_on,
        ValidFrom: this.label.Valid_from,
        ValidTo: this.label.Valid_to,
        DocumentDate: this.label.Document_Dat,
        NetValue: this.label.Net_value,
        Currency: this.label.SD_Doc_Currency,
        RequestedDeliver: this.label.Requested_Deliver,
        SalesDocument: this.label.Sales_Document,
        Status: this.label.Status,
        ItemNo: this.label.Sales_Document_Item,
        MaterialNumber: this.label.Material_Number,
        Description: this.label.Description,
        NetValueCurrency: this.label.Net_Value_Of_Item_Currency,
        Quantity: this.label.Order_Quantity,
      }
    ];

    let exportObj = {
                        SoldToParty: (this.selectedSoldToPartyItems.length > 0) ? this.selectedSoldToPartyItems[0]['CustomerName']: '--',
                        ShipToParty: (this.selectedShipToPartyItems.length > 0) ? this.selectedShipToPartyItems[0]['CustomerName']: '--',
                        SalesOrg: (this.selectedSalesOrgItem.length > 0) ? this.selectedSalesOrgItem[0]['SalesOrg']: '--',
                        DistributionChannel: (this.inquiryDetails.DistributionChannel != '') ? this.inquiryDetails.DistributionChannel : '--' ,
                        Division: (this.inquiryDetails.Division != '') ? this.inquiryDetails.Division : '--',
                        CreatedBy: (this.inquiryDetails.CreatedBy != '') ? this.inquiryDetails.CreatedBy : '--',
                        CreatedOn: (this.inquiryDetails.CreatedDate != '') ? this.inquiryDetails.CreatedDate : '--',
                        ValidFrom: (this.inquiryDetails.ValidFrom != '') ? this.inquiryDetails.ValidFrom : '--',
                        ValidTo: (this.inquiryDetails.ValidTo != '') ? this.inquiryDetails.ValidTo : '--',
                        DocumentDate: (this.inquiryDetails.DocumentDate != '') ? this.inquiryDetails.DocumentDate : '--',
                        NetValue: (this.inquiryDetails.NetValue != '') ? this.inquiryDetails.NetValue : 0,
                        Currency: (this.selectedSdDocCurrencyItems.length > 0) ? this.selectedSdDocCurrencyItems[0]['text']: '--',
                        RequestedDeliver: (this.inquiryDetails.ReqDeliveryDate != '') ? this.inquiryDetails.ReqDeliveryDate : '--',
                        SalesDocument: (this.inquiryDetails.DocumentNumber != '') ? this.inquiryDetails.DocumentNumber : '--',
                        Status: (this.selectedStatusItems.length > 0) ? this.selectedStatusItems[0]['text']: '--',
                        ItemNo: '--',
                        MaterialNumber: '--',
                        Description: '--',
                        NetValueCurrency: '--',
                        Quantity: '--',
                      }
    
    if(this.invoiceItemDetails.length == 0){
      this.inquiryExportData.push(exportObj);
    }else{
      for (let i = 0; i < this.invoiceItemDetails.length; i++) {
         let exportObj = {
                        SoldToParty: (this.selectedSoldToPartyItems.length > 0) ? this.selectedSoldToPartyItems[0]['CustomerName']: '--',
                        ShipToParty: (this.selectedShipToPartyItems.length > 0) ? this.selectedShipToPartyItems[0]['CustomerName']: '--',
                        SalesOrg: (this.selectedSalesOrgItem.length > 0) ? this.selectedSalesOrgItem[0]['SalesOrg']: '--',
                        DistributionChannel: (this.inquiryDetails.DistributionChannel != '') ? this.inquiryDetails.DistributionChannel : '--' ,
                        Division: (this.inquiryDetails.Division != '') ? this.inquiryDetails.Division : '--',
                        CreatedBy: (this.inquiryDetails.CreatedBy != '') ? this.inquiryDetails.CreatedBy : '--',
                        CreatedOn: (this.inquiryDetails.CreatedDate != '') ? this.inquiryDetails.CreatedDate : '--',
                        ValidFrom: (this.inquiryDetails.ValidFrom != '') ? this.inquiryDetails.ValidFrom : '--',
                        ValidTo: (this.inquiryDetails.ValidTo != '') ? this.inquiryDetails.ValidTo : '--',
                        DocumentDate: (this.inquiryDetails.DocumentDate != '') ? this.inquiryDetails.DocumentDate : '--',
                        NetValue: (this.inquiryDetails.NetValue != '') ? this.inquiryDetails.NetValue : 0,
                        Currency: (this.selectedSdDocCurrencyItems.length > 0) ? this.selectedSdDocCurrencyItems[0]['text']: '--',
                        RequestedDeliver: (this.inquiryDetails.RequestedDeliveryDate != '') ? this.inquiryDetails.RequestedDeliveryDate : '--',
                        SalesDocument: (this.inquiryDetails.DocumentNo != '') ? this.inquiryDetails.DocumentNo : '--',
                        Status: (this.selectedStatusItems.length > 0) ? this.selectedStatusItems[0]['text']: '--',
                        ItemNo: (this.invoiceItemDetails[i].Item != null) ? this.invoiceItemDetails[i].Item: '--',
                        MaterialNumber: (this.invoiceItemDetails[i].MaterialNo != null) ? this.invoiceItemDetails[i].MaterialNo: '--',
                        Description: (this.invoiceItemDetails[i].ShortText != null) ? this.invoiceItemDetails[i].ShortText: '--',
                        NetValueCurrency: (this.invoiceItemDetails[i].NetValue != null) ? this.invoiceItemDetails[i].NetValue: '--',
                        Quantity: (this.invoiceItemDetails[i].OrderQuantity != null) ? this.invoiceItemDetails[i].OrderQuantity: '--',
                      }
            console.log(this.invoiceItemDetails[i], exportObj, this.inquiryDetails)
                      this.inquiryExportData.push(exportObj);
      }
    }                
  
    
    
    console.log(this.inquiryExportData, 'order export')
  }

  getCustomerMasterList(){
    this.spinner.show();
    this.inquiryQuote.getCustomerMasterList().pipe()
      .subscribe((data: any) => {
        this.soldToPartyList =  data
        this.soldToPartyList.map((index) => {
          index.CustomerName = index.CustomerNumber+'-'+ index.CustomerName;
        });
        this.spinner.hide();
        if(this.editInquiryId != null && this.editInquiryId != ''){
          this.viewModeField = true;
          this.getSingleInquiryDetails();
        }
      }, error => {
        this.spinner.hide();
        this.alertService.error(Language.ALERT.Title_Error, Language.ALERT.Went_Wrong_Message);
        // this.util.PrintLogs(varConstants.ERROR, this.className, this.getSingleInquiryDetails.name, error);
      });
  }

  getMaterialMasterList(){
    this.inquiryQuote.getMaterialMasterList().pipe()
      .subscribe((data: any) => {
        this.materilMasterList =  data
      }, error => {
        this.alertService.error(Language.ALERT.Title_Error, Language.ALERT.Went_Wrong_Message);
        // this.util.PrintLogs(varConstants.ERROR, this.className, this.getSingleInquiryDetails.name, error);
      });
  }

  selectSoldToParty(item: any){
    console.log(item, 'sold')
    this.getCustomerDetails(item.CustomerNumber);
    this.deSelectSoldToParty();
  }

  selectShipToParty(item: any){
    
  }

  deSelectSoldToParty(){
    this.shipToPartyList = [];
    this.selectedShipToPartyItems = [];
    this.salesOrganizationList = [];
    this.selectedSalesOrgItem = [];
    this.inquiryDetails.Division = '';
    this.inquiryDetails.DistributionChannel = '';
  }

  selectSalesOrg(item: any){
    let tmp = this.salesOrganizationList.find(list => list.SalesOrg == item.SalesOrg);
    console.log(item)
    if(tmp != undefined){
      this.inquiryDetails.Division = tmp.Division;
      this.inquiryDetails.DistributionChannel = tmp.DistributionChannel;
    }
  }

  deSelectSalesOrg(item: any){
    this.inquiryDetails.Division = '';
    this.inquiryDetails.DistributionChannel = '';
  }

  getCustomerDetails(customerId, setValue = ''){
    let param = {
      customerId: customerId
    }
    this.spinner.show();
    this.inquiryQuote.getCustomerDetails(param).pipe()
      .subscribe((data: any) => {
       
        console.log(data, 'customer')
        this.shipToPartyList = data.PartnerData;
        this.shipToPartyList.map((index) => {
          index.CustomerName = index.BusinessPartnerCustomerNo+'-'+ index.CustomerName;
        });
        this.salesOrganizationList = data.CustomerSalesOrg;
        if(setValue == 'Set-Value'){
          let tmp = this.shipToPartyList.find(item => item.BusinessPartnerCustomerNo == this.inquiryDetails.SoldToParty);
          if(tmp != 'undefined'){
            this.selectedShipToPartyItems = [];
            this.selectedShipToPartyItems.push(tmp);
          }

          let tmp1 = this.salesOrganizationList.find(item => item.SalesOrg == this.inquiryDetails.SalesOrganization);
          if(tmp1 != 'undefined'){
            this.selectedSalesOrgItem = [];
            this.selectedSalesOrgItem.push(tmp1);
          }
        }
        this.spinner.hide()
      }, error => {
        this.spinner.hide()
        this.alertService.error(Language.ALERT.Title_Error, Language.ALERT.Went_Wrong_Message);
      });
  }

  enableForm() {
    this.viewModeField = false;
  }

  ngOnDestroy(){
    localStorage.removeItem('editInquiryId');
  }
  onSelectAll(items: any){
      console.log(items);
  }
  onDeSelectAll(items: any){
      console.log(items);
  }

  setNetValue(item: any){
    let charCode = (item.which) ? item.which : item.keyCode;
    console.log(charCode, item)
    // // if(charCode == 8 || charCode == 46){
    // //   return true;
    // // }
    // // if (charCode != 190 && charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57)){
    // //   return false;
    // // }
    // // if(this.inquiryDetails.NetValue.toLocaleString().split('.').length == 2 && charCode == 190){
    // //   return false;
    // // }
    // // if(this.inquiryDetails.NetValue.toLocaleString().length > 15 || this.inquiryDetails.NetValue.toLocaleString().split('.').length > 2){
    // //  return false;
    // // }
    let key = item.key;
    if((this.inquiryDetails.NetValue == undefined || this.inquiryDetails.NetValue == null) && key == '.'){
      return false;
    }
    let keysArr = ['0', '1', '2','3','4','5','6','7','8','9','.', 'Backspace','Delete'];
    console.log(this.inquiryDetails.NetValue, keysArr.indexOf(key), this.inquiryDetails.NetValue.toLocaleString().split('.'))
    if(keysArr.indexOf(key) == -1){
      return false;
    }
    if(this.inquiryDetails.NetValue != undefined && this.inquiryDetails.NetValue != '' && this.inquiryDetails.NetValue.toLocaleString().split('.').length > 2){
      return false;
    }

    if(this.inquiryDetails.NetValue.toLocaleString().length > 15 || this.inquiryDetails.NetValue.toLocaleString().split('.').length > 2){
     return false;
    }
    console.log(this.inquiryDetails.NetValue.toLocaleString(), 'ss', this.inquiryDetails.NetValue.toLocaleString().includes('.'))
    if(key == '.' && this.inquiryDetails.NetValue.toLocaleString().includes('.') == true){
     return false;
    }
    return true;
  }

  numericValue(item: any){
    console.log(item, 'item', this.inquiryDetails.NetValue)
    let charCode = (item.which) ? item.which : item.keyCode;
    if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57)){
      console.log('first')
      return false;
    }
    // if(this.inquiryDetails.NetValue.toLocaleString().length > 15 || this.inquiryDetails.NetValue.toLocaleString().split('.').length > 1){
    //   return false;

    // }
    return true;
  }

  cancelEnquiryDetails(){
    if(this.editInquiryId != '' && this.editInquiryId != null && this.editInquiryId != undefined){
      this.getSingleInquiryDetails();
    }else{
      this.inquiryDetails = {};
      this.selectedSoldToPartyItems = [];
      this.selectedShipToPartyItems = [];
      this.selectedSalesOrgItem = [];
      // this.selectedSdDocCurrencyItems = []
    }
  }

  saveEnquiryDetails(enquiryForm = null){
    console.log(enquiryForm, 'form')
    if(enquiryForm && enquiryForm.form.status == 'INVALID'){
      return false;
    }
    this.formSaveErrorMsg = '';
    // if(this.invoiceItemDetails.length == 0){
    //   this.formSaveErrorMsg = 'Minium one Line-Item is required';
    //   return false;
    // }
    let loginUserDetails = localStorage.getItem('logged_in_user_details');
    loginUserDetails = JSON.parse(loginUserDetails);
    if(this.editInquiryId != '' && this.editInquiryId != null && this.editInquiryId != undefined){
      let param =  {
            "SalesInquiryID": this.editInquiryId,
            "UniversalClientId": 0,
            "CompanyCode": "String",
            "Client": "String",
            "DocumentNo": (this.inquiryDetails.DocumentNo == undefined) ? '': this.inquiryDetails.DocumentNo,
            "OrderType": "",
            "SalesOrganization": (this.selectedSalesOrgItem.length > 0) ? this.selectedSalesOrgItem[0]['SalesOrg']: '',
            "DistributionChannel": this.inquiryDetails.DistributionChannel,
            "Division": this.inquiryDetails.Division,
            "CreatedBy": this.inquiryDetails.Division,
            "CreatedDate": this.pipe.transform(this.inquiryDetails.CreatedDate,'yyyy-MM-dd'),
            "DocumentDate": this.pipe.transform(this.inquiryDetails.DocumentDate,'yyyy-MM-dd'),
            "NetValue": this.inquiryDetails.NetValue,
            "DocumentCurrency": (this.selectedSdDocCurrencyItems.length > 0) ? this.selectedSdDocCurrencyItems[0]['id']: '',
            "ValidFrom": this.pipe.transform(this.inquiryDetails.ValidFrom,'yyyy-MM-dd'),
            "ValidTo": this.pipe.transform(this.inquiryDetails.ValidTo, 'yyyy-MM-dd'),
            "RequestedDeliveryDate": this.pipe.transform(this.inquiryDetails.RequestedDeliveryDate,'yyyy-MM-dd'),
            "ShippingConditions": "",
            "PONo": "",
            "PODate": "",
            "SoldToParty": (this.selectedSoldToPartyItems.length > 0) ? this.selectedSoldToPartyItems[0]['CustomerNumber']: '',
            "PartnerFunction": "",
            "ShipToParty": (this.selectedShipToPartyItems.length > 0) ? this.selectedShipToPartyItems[0]['BusinessPartnerCustomerNo']: '',
            "RecordCreatedDate": this.pipe.transform(this.inquiryDetails.CreatedDate, 'yyyy-MM-dd'),
            "InquiryValidFrom": this.pipe.transform(this.inquiryDetails.ValidFrom, 'yyyy-MM-dd'),
            "Status": (this.selectedStatusItems.length > 0) ? this.selectedStatusItems[0]['id']: 0,
            "UpdatedDate": this.pipe.transform(this.currentDate,'yyyy-MM-dd'),
            "UpdatedBy": loginUserDetails['Id']
          }
      let parameter = {}
      parameter['salesInquiryHeader'] = param;
      // parameter['SalesInquiryID'] = this.editInquiryId;
      this.spinner.show();
      this.inquiryQuote.updateInquiryHeaderDetails(param,this.editInquiryId).pipe().subscribe((data: any) => {
        console.log('Response');
        this.spinner.hide();
        this.viewModeField = true;
        // this.inquiryDetails = data;
        // this.inquiryDetailsLabel = this.label.TAB.Inquiry_Details + '( '+this.inquiryDetails.SalesInquiryID+' )';
        // this.addLineItemDetails(lineItems[0]);
        // this.editInquiryId = this.inquiryDetails.SalesInquiryID;
      }, error => {
        this.spinner.hide()
        this.alertService.error(Language.ALERT.Title_Error, Language.ALERT.Went_Wrong_Message);
      });
    }else{
      let parameter = {
          "Id": 0,
          "UniversalClientId": 0,
          "CompanyCode": "",
          "Client": "",
          "DocumentNo": (this.inquiryDetails.DocumentNo == undefined) ? '': this.inquiryDetails.DocumentNo,
          "OrderType": "",
          "SalesOrganization": (this.selectedSalesOrgItem.length > 0) ? this.selectedSalesOrgItem[0]['SalesOrg']: '',
          "DistributionChannel": this.inquiryDetails.DistributionChannel,
          "Division": this.inquiryDetails.Division,
          "CreatedBy": loginUserDetails['Id'],
          "CreatedDate": this.pipe.transform(this.inquiryDetails.CreatedDate, 'yyyy-MM-dd'),
          "DocumentDate": this.pipe.transform(this.inquiryDetails.DocumentDate,'yyyy-MM-dd'),
          "NetValue": this.inquiryDetails.NetValue,
          "DocumentCurrency": (this.selectedSdDocCurrencyItems.length > 0) ? this.selectedSdDocCurrencyItems[0]['item_id']: '',
          "ValidFrom": this.pipe.transform(this.inquiryDetails.ValidFrom,'yyyy-MM-dd'),
          "ValidTo": this.pipe.transform(this.inquiryDetails.ValidTo, 'yyyy-MM-dd'),
          "RequestedDeliveryDate": this.pipe.transform(this.inquiryDetails.RequestedDeliveryDate,'yyyy-MM-dd'),
          "ShippingConditions": "",
          "PONo": "",
          "PODate": "",
          "SoldToParty": (this.selectedSoldToPartyItems.length > 0) ? this.selectedSoldToPartyItems[0]['CustomerNumber']: '',
          "PartnerFunction": "",
          "ShipToParty": (this.selectedShipToPartyItems.length > 0) ? this.selectedShipToPartyItems[0]['BusinessPartnerCustomerNo']: '',
          "RecordCreatedDate": this.pipe.transform(this.inquiryDetails.CreatedDate, 'yyyy-MM-dd'),
          "InquiryValidFrom": this.pipe.transform(this.inquiryDetails.ValidFrom, 'yyyy-MM-dd'),
          "Status": (this.selectedStatusItems.length > 0) ? this.selectedStatusItems[0]['id']: 0,
          "UpdatedDate": "",
          "UpdatedBy": ""
      }
      this.spinner.show();
      this.inquiryQuote.addInquiryHeaderDetails(parameter).pipe().subscribe((data: any) => {
        this.inquiryDetails = data;
        this.viewModeField = true;
        this.inquiryDetailsLabel = this.label.TAB.Inquiry_Details + '( '+this.inquiryDetails.SalesInquiryID+' )';
        // this.addLineItemDetails(lineItems[0]);
        this.editInquiryId = this.inquiryDetails.SalesInquiryID;
        this.spinner.hide()
      }, error => {
        this.spinner.hide()
        this.alertService.error(Language.ALERT.Title_Error, Language.ALERT.Went_Wrong_Message);
      });
    }
     
  }

  selectMaterialItem(item: any){
    console.log(item)
    let tmp = this.materilMasterList.find(list=> list.Material_ID == item.Material_ID);
    if(tmp != undefined){
      this.itemsDetails.ShortText = tmp.MaterialDescription;
    }
    let param = {
       id: item.Material_ID
    }
    this.inquiryQuote.getQuestionByMaterial(param).pipe().subscribe((data: any) => {
         for (let i = 0; i < data.length; i++) {
            data[i]['QuestionOptionArr'] = data[i]['QuestionOption'].split(',');
            data[i]['currentIndex'] = i+1;
          }
        this.questionsList = data;
        console.log(this.questionsList, 'ques')
    }, error => {
      this.spinner.hide()
      this.alertService.error(Language.ALERT.Title_Error, Language.ALERT.Went_Wrong_Message);
    });
  }

  deSelectMaterialItem(item: any){
    this.questionsList = [];
  }

  deleteEnquiryHeaderDetails(){
    let param = {
       SalesInquiryID: this.editInquiryId
    }
    this.inquiryQuote.deleteSalesInquiryDetails(param).pipe().subscribe((data: any) => {
      this.alertService.success(Language.ALERT.Success, Language.ALERT.Delete_Message);
      this.router.navigate(['/inquiry-quotation-section/inquiry']);
    }, error => {
      this.spinner.hide()
      this.alertService.error(Language.ALERT.Title_Error, Language.ALERT.Went_Wrong_Message);
    });
  }

  deleteEnquiryItemDetails(itemId){
    let param = {
       SalesInquiryItemID: itemId
    }
    this.inquiryQuote.deleteSalesItemDetails(param).pipe().subscribe((data: any) => {
      this.getSingleInquiryDetails();
      this.alertService.success(Language.ALERT.Success, Language.ALERT.Delete_Message);
    }, error => {
      this.spinner.hide()
      this.alertService.error(Language.ALERT.Title_Error, Language.ALERT.Went_Wrong_Message);
    });
  }

  submitEnquiryDetails(enquiryForm){
    //file management
    if(enquiryForm && enquiryForm.form.status == 'INVALID'){
      return false;
    }
    console.log('form submited')
    this.selectedStatusItems = [ { id: 1, text: 'Submited' }];
    this.saveEnquiryDetails(enquiryForm);
  }

  selectFile(event): void {
    this.selectedFile = event.target.files.item(0);
  }

  documentDownload(docName){
    this.spinner.show();
    let param = {
      AttachmentName: docName
    }
    this.inquiryQuote.documentDownload(param).pipe()
      .subscribe((data: any) => {
        if (data) {
          var blob = new Blob([data], { type: 'image/png' });
          download(blob, docName);
        }else {
          this.alertService.error(Language.ALERT.Title_Error, Language.ALERT.Went_Wrong_Message);
        }
        this.spinner.hide()
      }, error => {
        this.spinner.hide()
        this.alertService.error(Language.ALERT.Title_Error, Language.ALERT.Went_Wrong_Message);
        // this.util.PrintLogs(varConstants.ERROR, this.className, this.getSingledeliveryDetailsname, error);
      });
  }
   
  onChangeCheckBoxEvent(event: any, id, value){
    console.log(event, 'item', id, 'id', value, 'value')
    if(this.quizeDetails[id] == undefined){
      this.quizeDetails[id] = [];
    }
    if(this.quizeDetails[id] != ""){
      this.quizeDetails[id] = this.quizeDetails[id].split(',');
    }
    if(event.target.checked){
      if(this.quizeDetails[id].indexOf(value) == -1){
        this.quizeDetails[id].push(value);
      }
    }else{
      if(this.quizeDetails[id].indexOf(value) != -1){
        let index = this.quizeDetails[id].indexOf(value);
        this.quizeDetails[id].splice(index, 1);
      }
    }
    this.quizeDetails[id] = this.quizeDetails[id].join();
    console.log(this.quizeDetails)
  }
}
