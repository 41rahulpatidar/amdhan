import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { InquiryDetailsRoutingModule } from './inquiry-details-routing.module';
import { PackagedModule } from '../../../../app/Packaged/packaged.module';
import { InquiryDetailsComponent } from './inquiry-details.component';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';
// import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown';
// import { FormsModule } from '@angular/forms';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { Angular2CsvModule } from 'angular2-csv';

@NgModule({
  declarations: [InquiryDetailsComponent],
  imports: [
    CommonModule,
    InquiryDetailsRoutingModule,
    PackagedModule,
    OwlDateTimeModule, 
   	OwlNativeDateTimeModule,
     NgMultiSelectDropDownModule.forRoot(),
     Angular2CsvModule
   	// AngularMultiSelectModule,
   	// FormsModule
  ]
})
export class InquiryDetailsModule { }
