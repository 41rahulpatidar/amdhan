import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { InquiryDetailsComponent } from './inquiry-details.component';

const routes: Routes = [
  { path: "", component: InquiryDetailsComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class InquiryDetailsRoutingModule { }
