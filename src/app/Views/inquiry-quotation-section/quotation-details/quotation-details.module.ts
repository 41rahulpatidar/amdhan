import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { QuotationDetailsRoutingModule } from './quotation-details-routing.module';
import { PackagedModule } from '../../../../app/Packaged/packaged.module';
import { QuotationDetailsComponent } from './quotation-details.component';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';
import { Angular2CsvModule } from 'angular2-csv';

@NgModule({
  declarations: [QuotationDetailsComponent],
  imports: [
    CommonModule,
    PackagedModule,
    QuotationDetailsRoutingModule,
    NgMultiSelectDropDownModule.forRoot(),
    OwlDateTimeModule, 
   	OwlNativeDateTimeModule,
   	Angular2CsvModule
  ]
})
export class QuotationDetailsModule { }
