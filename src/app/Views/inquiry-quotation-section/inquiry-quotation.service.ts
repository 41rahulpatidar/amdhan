import { Injectable } from '@angular/core';
import { RestApiService } from 'src/app/Service/rest-api.service';
import { apiUrl } from 'src/app/Other/apiUrl.constants';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class InquiryQuotationService {

  constructor(private restService: RestApiService) { }
  
  getInquiryHeaderData(param): Observable<any> {
    return this.restService.get(apiUrl.INQUIRY_DATA,param);
  }
  getInquiryExportData(param): Observable<any> {
    return this.restService.get(apiUrl.EXPORT_UTILITY,param, 'blob');
  }
  getSingleInquiryData(enquiryId): Observable<any> {
    return this.restService.get(apiUrl.INQUIRY_DATA+'/'+enquiryId);
  }

  getCustomerDetails(request): Observable<any> {
    return this.restService.get(apiUrl.CUSTOMER_DETAILS, request);
  }

  getCustomerMasterList(): Observable<any> {
    return this.restService.get(apiUrl.CUSTOMER_MASTER);
  }

  getSalesInquiryItemListById(request): Observable<any> {
    return this.restService.get(apiUrl.INQUIRY_ITEM_BY_ID, request);
  }

  getMaterialMasterList(): Observable<any> {
    return this.restService.get(apiUrl.MATERIAL_MASTER_LIST);
  }

  addInquiryHeaderDetails(request): Observable<any> {
    return this.restService.post(apiUrl.ADD_UPDATE_SALES_INQUIRY_HEADER, request);
  }

  updateInquiryHeaderDetails(request, editId): Observable<any> {
    return this.restService.put(apiUrl.ADD_UPDATE_SALES_INQUIRY_HEADER+'?SalesInquiryID='+editId, request);
  }

  addInquiryLineItemDetails(request): Observable<any> {
    return this.restService.post(apiUrl.POST_SALES_INQUIRY_WITH_QUES_ANS, request);
  }

  updateInquiryLineItemDetails(request,editId): Observable<any> {
    return this.restService.put(apiUrl.PUT_SALES_INQUIRY_WITH_QUES_ANS+'?SalesInquiryItemID='+editId, request);
  }

  deleteSalesInquiryDetails(request): Observable<any> {
    return this.restService.delete(apiUrl.INQUIRY_DATA, request);
  }

  deleteSalesItemDetails(request): Observable<any> {
    return this.restService.delete(apiUrl.INQUIRY_DETAILS, request);
  }

  addInvoiceDocumentDetails(request, param): Observable<any> {
    return this.restService.post(apiUrl.ADD_INQUIRY_DOCUMENT+'?'+param, request);
  }

  getSalesInquiryDocumentById(request): Observable<any> {
    return this.restService.get(apiUrl.GET_INQUIRY_DOCUMENT, request);
  }
  
  documentDownload(request): Observable<any> {
    return this.restService.get(apiUrl.DOCUMENT_DOWNLOAD, request, 'blob');
  }


  getQuoteHeaderData(param): Observable<any> {
    return this.restService.get(apiUrl.QUOTE_HEADER,param);
  }
  getQuoteExportData(param): Observable<any> {
    return this.restService.get(apiUrl.EXPORT_UTILITY,param, 'blob');
  }
  getSingleQuoteData(quoteId): Observable<any> {
    return this.restService.get(apiUrl.QUOTE_HEADER+'/'+quoteId);
  }
  getSalesQuoteItemListById(request): Observable<any> {
    return this.restService.get(apiUrl.QUOTE_ITEM_DETAILS_BY_HEADER_ID, request);
  }
  addQuoteHeaderDetails(request): Observable<any> {
    return this.restService.post(apiUrl.QUOTE_HEADER, request);
  }

  updateQuoteHeaderDetails(request, editId): Observable<any> {
    return this.restService.put(apiUrl.QUOTE_HEADER+'?id='+editId, request);
  }

  addQuoteLineItemDetails(request): Observable<any> {
    return this.restService.post(apiUrl.QUOTE_ITEM_DETAILS, request);
  }

  updateQuoteLineItemDetails(request,editId): Observable<any> {
    return this.restService.put(apiUrl.QUOTE_ITEM_DETAILS+'?id='+editId, request);
  }

  deleteQuoteItemDetails(request): Observable<any> {
    return this.restService.delete(apiUrl.QUOTE_ITEM_DETAILS, request);
  }
   getQuestionByMaterial(request): Observable<any> {
    return this.restService.get(apiUrl.QUESTION_BY_MATERIAL, request);
  }

}
