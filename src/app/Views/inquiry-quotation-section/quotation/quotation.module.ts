import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { QuotationRoutingModule } from './quotation-routing.module';
import { PackagedModule } from '../../../../app/Packaged/packaged.module';
import { QuotationComponent } from './quotation.component';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';


@NgModule({
  declarations: [QuotationComponent],
  imports: [
    CommonModule,
    PackagedModule,
    QuotationRoutingModule,
    OwlDateTimeModule, 
    OwlNativeDateTimeModule
  ]
})
export class QuotationModule { }
