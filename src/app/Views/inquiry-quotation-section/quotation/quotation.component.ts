import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { Language } from '../../../Other/translation.constants';
import { NgxSpinnerService } from 'ngx-spinner';
import { UtilService } from 'src/app/Service/util.service';
import { varConstants } from 'src/app/Other/variable.constants';
import { InquiryQuotationService } from '../inquiry-quotation.service';
import { AlertService } from 'src/app/Service/alert.service';
import { Router } from '@angular/router';
import { map, tap } from 'rxjs/operators';
import {DatePipe} from '@angular/common';
import { saveAs as download } from "file-saver";

@Component({
  selector: 'app-quotation',
  templateUrl: './quotation.component.html',
  styleUrls: ['./quotation.component.scss']
})
export class QuotationComponent implements OnInit {
	label: any = Language;
	quoteArray: any = [{fieldname: "demo"}];
	dtOptions: any;
	className = QuotationComponent.name;
	userData: any;
	currentSection: any;
	headerVisible: boolean = true;
	datatable: any;
  isMenuAccess: boolean = true;
  isAddEditPermission: boolean = true;
  quoteFilter: any = {
    FromDate: new Date(),
    ToDate: new Date(),
    Keyword: ''
  }
  pipe = new DatePipe('en-US');


	constructor(
    private router: Router,
    private spinner: NgxSpinnerService,
    private util: UtilService,
    private inquiryQuote: InquiryQuotationService,
    private alertService: AlertService,
    private chRef: ChangeDetectorRef) { 
    this.dtOptions = this.util.dtOptions;
	}

	ngOnInit() {
    let currentDate = new Date();
    let previousDate = currentDate.setFullYear(currentDate.getFullYear() - 1);
    this.quoteFilter.FromDate = new Date(previousDate);
    this.userData = JSON.parse(localStorage.getItem("logged_in_user_details"));
    this.currentSection = JSON.parse(localStorage.getItem("current_section"));
    if(this.currentSection.type == 'CloseInquiry'){
      this.headerVisible = false;
    }
    //**** User Menu Access Start ****//
    if(this.isMenuAccess == false){
      this.router.navigate(["/dashboard"]);
    }

    if(this.userData.RoleType == '2'){
      this.isAddEditPermission = true;
    }

    if(this.userData.RoleType == '3'){
      if(this.userData.Lst_RoleData1[0].ApprovalLevel == 'L01'){
        this.isAddEditPermission = false;
      }else{
        this.isAddEditPermission = false;
      }
    }

    if(this.headerVisible == true){
      this.headerVisible = this.isAddEditPermission;
    }
    //**** User Menu Access End ****//
    this.util.tableHorizontalScroll('quotation_header_table');
    this.getQuoteHeaderData();
  }

  getQuoteHeaderData() {
      this.spinner.show();
      let param = {
        FromDate: this.pipe.transform(this.quoteFilter.FromDate,'yyyy-MM-dd'),
        ToDate: this.pipe.transform(this.quoteFilter.ToDate,'yyyy-MM-dd'),
        Keyword: this.quoteFilter.Keyword
      }
      this.inquiryQuote.getQuoteHeaderData(param)
      .pipe(
        tap(data => {
          this.util.PrintLogs(varConstants.INFO, this.className, this.getQuoteHeaderData.name, data);
        }),
        //Filter data with Status as '0' (Draft)
        //Filter data with Status as '4' (Invited)
        map(data => {
          //let newData = data.filter(result => result.Status == varConstants.isNew || result.Status == varConstants.isInvited)
          //below code will put the readable values into the array object

          let newData = data;
          for (const property in newData) {
            switch (newData[property]["Status"]) {
              case varConstants.isNew:
                newData[property]["StatusToDisplay"] = Language.Draft;
                break;
              case varConstants.isNull:
                newData[property]["StatusToDisplay"] = Language.Draft;
                break;
              case varConstants.isBlank:
                newData[property]["StatusToDisplay"] = Language.Draft;
                break;
              case varConstants.isSubmitted:
                newData[property]["StatusToDisplay"] = Language.Awaiting;
                break;
              default:
                newData[property]["StatusToDisplay"] = newData[property]["Status"];
            }
          }
          return newData;
        }),
      )
        .subscribe((data: any) => {
          this.spinner.hide();
          if (Array.isArray(data)) {
            if ($.fn.dataTable.isDataTable('#quotation_header_table')) {
              $('#quotation_header_table').DataTable().clear().destroy();               
            }
            this.quoteArray = data;
            this.chRef.detectChanges();
            const table: any = $('table');
            this.datatable = table.DataTable();
          }
        }, error => {
          this.spinner.hide()
          this.alertService.error(Language.ALERT.Title_Error, Language.ALERT.Went_Wrong_Message);
          this.util.PrintLogs(varConstants.ERROR, this.className, this.getQuoteHeaderData.name, error);
        });
  } 

  getQuoteExportData(){
    this.spinner.show();
      let param = {
        FromDate: this.pipe.transform(this.quoteFilter.FromDate,'yyyy-MM-dd'),
        ToDate: this.pipe.transform(this.quoteFilter.ToDate,'yyyy-MM-dd'),
        Keyword: this.quoteFilter.Keyword,
        Status: 0,
        DocumentType: 'Quotation'
      }
  
    this.inquiryQuote.getQuoteExportData(param).subscribe((data: any) => {
      this.spinner.hide();
      if (data) {
        var blob = new Blob([data], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8' });
        download(blob, 'Quotation_Header_Export' + new  Date().getTime() + '.xlsx');
      }
      else {
        this.alertService.error(Language.ALERT.Title_Error, Language.ALERT.Went_Wrong_Message);
      }
    }, error => {
      console.log(error, 'error')
      this.spinner.hide();
      this.alertService.error(Language.ALERT.Title_Error, Language.ALERT.Went_Wrong_Message);
   });
  }
  
  viewInquiryDetails(quotation) {
    localStorage.setItem('editQuoteId', quotation.Quoation_Header_ID);
    quotation.from = "/inquiry-quotation-section/quotation";
    quotation.tab = this.label.SUB_TAB.Quotation;
    quotation.mode = varConstants.isRegistered;
    this.router.navigate(["/inquiry-quotation-section/quotation-details"], { state: { data: quotation } });
  }

}
