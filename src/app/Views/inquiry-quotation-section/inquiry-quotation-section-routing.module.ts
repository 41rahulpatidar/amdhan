import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { Language } from 'src/app/Other/translation.constants';

const routes: Routes = [{
  path: '',
  data: {
    title: Language.TAB.Admin_Section,
    status: false
  },
  children: [
    {
      path: '',
      redirectTo: 'inquiry-quotation-section',
      pathMatch: 'full'
    },
    {
      path: 'inquiry',
      loadChildren: () => import('./inquiry/inquiry.module').then(m => m.InquiryModule)
    },
    {
      path: 'quotation',
      loadChildren: () => import('./quotation/quotation.module').then(m => m.QuotationModule)
    },
    {
      path: 'inquiry-details',
      loadChildren: () => import('./inquiry-details/inquiry-details.module').then(m => m.InquiryDetailsModule)
    },
     {
      path: 'quotation-details',
      loadChildren: () => import('./quotation-details/quotation-details.module').then(m => m.QuotationDetailsModule)
    }
  ]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MastersSectionRoutingModule { }