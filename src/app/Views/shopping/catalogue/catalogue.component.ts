import { Component, OnInit } from '@angular/core';
import { UtilService } from 'src/app/Service/util.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { AlertService } from 'src/app/Service/alert.service';
import { Router } from '@angular/router';
import { varConstants } from 'src/app/Other/variable.constants';
import { map, tap } from 'rxjs/operators';
import { Language } from 'src/app/Other/translation.constants';
@Component({
  selector: 'app-catalogue',
  templateUrl: './catalogue.component.html',
  styleUrls: ['./catalogue.component.scss']
})
export class CatalogueComponent implements OnInit {
  label: any = Language;
  catalogueList: any = [];
  dtOptions: any;
  isSelectAll: boolean = false;
  className = CatalogueComponent.name;
  vendorID:any;
  Status:any = "New";
  userData: any;
  constructor(private router: Router,
    private alertService: AlertService,
    private spinner: NgxSpinnerService,
    private util: UtilService,
  ) {
    this.dtOptions = util.dtOptions;
  }

  ngOnInit() {
    this.userData = JSON.parse(localStorage.getItem("logged_in_user_details"));

    
  }
}
