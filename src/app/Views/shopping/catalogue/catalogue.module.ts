import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CatalogueRoutingModule } from './catalogue-routing.module';
import { CatalogueComponent } from './catalogue.component';
import { PackagedModule } from 'src/app/Packaged/packaged.module';

@NgModule({
  declarations: [CatalogueComponent],
  imports: [
    CommonModule,
    PackagedModule,
    CatalogueRoutingModule
  ]
})
export class CatalogueModule { }
