import { Component, OnInit } from '@angular/core';
import { UtilService } from 'src/app/Service/util.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { AlertService } from 'src/app/Service/alert.service';
import { Router } from '@angular/router';
import { varConstants } from 'src/app/Other/variable.constants';
import { map, tap } from 'rxjs/operators';
import { Language } from 'src/app/Other/translation.constants';
@Component({
  selector: 'app-card-details',
  templateUrl: './card-details.component.html',
  styleUrls: ['./card-details.component.scss']
})
export class CardDetailsComponent implements OnInit {
  label: any = Language;
  dtOptions: any;
  isSelectAll: boolean = false;
  className = CardDetailsComponent.name;
  vendorID: any;
  userData: any;
  dataLoaded: boolean = false;

  productList: object[] = [];

  imageUrl: any = 'http://amdhanvendors.azurewebsites.net/CatalogueFiles/CatalogueImages/';
  constructor(private router: Router,
    private alertService: AlertService,
    private spinner: NgxSpinnerService,
    private util: UtilService,
  ) {
    this.dtOptions = util.dtOptions;
  }

  ngOnInit() {
    this.userData = JSON.parse(localStorage.getItem("logged_in_user_details"));

   
  }

}
