import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CardDetailsRoutingModule } from './card-details-routing.module';
import { CardDetailsComponent } from './card-details.component';
import { PackagedModule } from 'src/app/Packaged/packaged.module';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';

@NgModule({
  declarations: [CardDetailsComponent],
  imports: [
    CommonModule,
    PackagedModule,
    CardDetailsRoutingModule,
    NgMultiSelectDropDownModule.forRoot()
  ]
})
export class CardDetailsModule { }
