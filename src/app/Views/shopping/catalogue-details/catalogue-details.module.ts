import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CatalogueDetailsRoutingModule } from './catalogue-details-routing.module';
import { PackagedModule } from 'src/app/Packaged/packaged.module';
import { CatalogueDetailsComponent } from './catalogue-details.component';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';

@NgModule({
  declarations: [CatalogueDetailsComponent],
  imports: [
    CommonModule,
    PackagedModule,
    CatalogueDetailsRoutingModule,
    NgMultiSelectDropDownModule.forRoot()
  ]
})
export class CatalogueDetailsModule { }
