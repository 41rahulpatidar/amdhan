import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { AlertService } from 'src/app/Service/alert.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { UtilService } from 'src/app/Service/util.service';
import { Language } from '../../../Other/translation.constants';
import { varConstants } from 'src/app/Other/variable.constants';
import { map, tap } from 'rxjs/operators';
import { saveAs as download } from "file-saver";
import { IDropdownSettings } from 'ng-multiselect-dropdown';
import { VirtualTimeScheduler } from 'rxjs';
import { FormBuilder, FormGroup } from "@angular/forms";
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-catalogue-details',
  templateUrl: './catalogue-details.component.html',
  styleUrls: ['./catalogue-details.component.scss']
})
export class CatalogueDetailsComponent implements OnInit {
  form: FormGroup;
  
  constructor(private router: Router,
    private alertService: AlertService,
    private spinner: NgxSpinnerService,
    private util: UtilService,
    public fb: FormBuilder,
    private http: HttpClient
  ) {
    
  }

  ngOnInit() {
    
  }
}
