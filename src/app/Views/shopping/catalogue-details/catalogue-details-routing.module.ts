import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CatalogueDetailsComponent } from './catalogue-details.component';

const routes: Routes = [{
  path : "",
  component: CatalogueDetailsComponent
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CatalogueDetailsRoutingModule { }
