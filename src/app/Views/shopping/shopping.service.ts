import { Injectable } from '@angular/core';
import { RestApiService } from 'src/app/Service/rest-api.service';
import { apiUrl } from 'src/app/Other/apiUrl.constants';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ShoppingService {

  constructor(private restService: RestApiService) { }

  addToCart(requestData) {
    return this.restService.post(apiUrl.CATALOG_ADD_TO_CART, requestData);
  }

  getCatalogueProductList() {
    return this.restService.get(apiUrl.CATALOGUE_PRODUCT_LIST);
  }

  getCatalogueFilterItem() {
    return this.restService.get(apiUrl.CATALOGUE_FILTER_ITEM);
  }

  getProductItemByFilter(parameter) {
    return this.restService.get(apiUrl.PRODUCT_ITEM_BY_FILTER, parameter);
  }
  // getCatalogueList(): Observable<any> {
  //   return this.restService.get(apiUrl.CATALOGUE_LIST);
  // }

  // getSingleCatalogueDetails(catalogueId): Observable<any> {
  //   return this.restService.get(apiUrl.CATALOGUE_BY_ID + '/' + catalogueId);
  // }

  // saveCatalogueHeaders(data) {
  //   return this.restService.post(apiUrl.GENERATE_CATALOGUE, data);
  // }

  // updateCatalogueHeaders(CatalogueID, data) {
  //   return this.restService.put(apiUrl.GENERATE_CATALOGUE + '/' + CatalogueID, data);
  // }

  // deleteCatalogue(data) {
  //   return this.restService.delete(apiUrl.CATALOGUE_LIST, data);
  // }

  // getSupplierMasterList() {
  //   return this.restService.get(apiUrl.VENDOR_HEADER);
  // }

  // getMaterialGroupMaster() {
  //   return this.restService.get(apiUrl.MASTER_DATA_TABLE + '/M_MaterialGroup');
  // }

  // getCountryMaster() {
  //   return this.restService.get(apiUrl.MASTER_DATA_TABLE + '/M_Country');
  // }

  // getRegionMaster(CountryCode) {
  //   return this.restService.get(apiUrl.REGION_LIST + '?CountryCode=' + CountryCode);
  // }

  // getCityMaster(RegionCode) {
  //   return this.restService.get(apiUrl.MASTER_DATA_TABLE + '/M_City?RegionCode=' + RegionCode);
  // }

  // deleteCatalogueItem(catalogueId) {
  //   return this.restService.get(apiUrl.CATALOGUE_ITEM + '/' + catalogueId,);
  // }

  // addCatalogueItem(request) {
  //   return this.restService.post(apiUrl.CATALOGUE_ITEM, request);
  // }


  // getCartItems(param) {
  //   return this.restService.get(apiUrl.CATALOG_CART_ITEMS, param);
  // }

  // updateCatalogueItem(lineItemId, request) {
  //   return this.restService.put(apiUrl.CATALOGUE_ITEM + '/' + lineItemId, request);
  // }

  // getCatalogueItemDetails(catalogueId) {
  //   return this.restService.get(apiUrl.CATALOG_ITEM_NEW + '?CatalogueID=' + catalogueId);
  // }

  // getMaterialGroupListByCatalogId(catalogueId) {
  //   return this.restService.get(apiUrl.MATERIAL_GROUP_LIST_BY_CATALOG_ID + '?CatelogueID=' + catalogueId);
  // }

  // getIrrespectiveMaterial(catalogueGroup) {
  //   return this.restService.get(apiUrl.IRRESPECTIVE_MATERIAL_BY_GROUP + '?MaterialGroups=' + catalogueGroup);
  // }

  // downloadCatalogueItemDetails(catalogueId) {
  //   let param = {
  //     CatalogueID: catalogueId,
  //     AttachmentName: 'RFQ_RFQ-219_NA219_test-xl.xlsx'
  //   }
  //   return this.restService.get(apiUrl.DOWNLOAD_CATALOG_ITEM_LIST, param, 'blob');
  // }

  // uploadCatalogueItemDetails(catalogueId, formData) {
  //   return this.restService.post(apiUrl.UPLOAD_CATALOG_ITEM_LIST + '?CatalogueID=' + catalogueId, formData);
  // }

  
}
