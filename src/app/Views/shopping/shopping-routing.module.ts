import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { Language } from 'src/app/Other/translation.constants';

const routes: Routes = [{
  path: '',
  data: {
    title: Language.TAB.Admin_Section,
    status: false
  },
  children: [
    {
      path: '',
      redirectTo: 'shopping',
      pathMatch: 'full'
    },
    // {
    //   path: 'catalogue',
    //   loadChildren: () => import('./catalogue/catalogue.module').then(m => m.CatalogueModule)
    // },
    {
      path: 'product-list',
      loadChildren: () => import('./product-list/product-list.module').then(m => m.ProductListModule)
    },
    // {
    //   path: 'catalogue-details',
    //   loadChildren: () => import('./catalogue-details/catalogue-details.module').then(m => m.CatalogueDetailsModule)
    // },
    {
      path: 'card-details',
      loadChildren: () => import('./card-details/card-details.module').then(m => m.CardDetailsModule)
    }
  ]
}];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ShoppingRoutingModule { }
