import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProductListRoutingModule } from './product-list-routing.module';
import { ProductListComponent } from './product-list.component';
import { PackagedModule } from 'src/app/Packaged/packaged.module';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';

@NgModule({
  declarations: [ProductListComponent],
  imports: [
    CommonModule,
    PackagedModule,
    ProductListRoutingModule,
    NgMultiSelectDropDownModule.forRoot()
  ]
})
export class ProductListModule { }
