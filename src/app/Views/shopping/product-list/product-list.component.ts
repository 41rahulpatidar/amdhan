import { Component, OnInit } from '@angular/core';
import { UtilService } from 'src/app/Service/util.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { AlertService } from 'src/app/Service/alert.service';
import { Router } from '@angular/router';
import { varConstants } from 'src/app/Other/variable.constants';
import { map, tap } from 'rxjs/operators';
import { Language } from 'src/app/Other/translation.constants';
import { ShoppingService } from './../shopping.service';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.scss']
})
export class ProductListComponent implements OnInit {
  label: any = Language;
  dtOptions: any;
  isSelectAll: boolean = false;
  className = ProductListComponent.name;
  vendorID: any;
  userData: any;
  universalSearch: any = '';
  dataLoaded: boolean = false;

  searchItemList: any = [];
  searchItemSetting: any = {};
  selectedSearchItem: any = [];

  productList: any = [];
  relatedProductList: any = [];
  sidebarItemList: any = [];
  leftSidebarFilter: any = {}
  filterItemList: any = [];
  tmpItemName: any = '';
  tmpStorage: any = '';
  currentStorageName: any = '';
  previousStorageName: any = '';
  imageUrl: any = 'http://amdhanvendors.azurewebsites.net/CatalogueFiles/CatalogueImages/';

  constructor(private router: Router,
    private alertService: AlertService,
    private spinner: NgxSpinnerService,
    private util: UtilService,
    private shoppingService: ShoppingService
  ) {
  
  }

  ngOnInit() {
    this.userData = JSON.parse(localStorage.getItem("logged_in_user_details"));
    this.searchItemList = [{ id: 'Product', name: 'Product' }, { id: 'Category', name: 'Category' }, { id: 'Manufactures', name: 'Manufactures' }, { id: 'Supplier', name: 'Supplier' }];
    this.searchItemSetting = {
      singleSelection: true,
      idField: 'id',
      textField: 'name',
      selectAllText: 'Search Product',
      unSelectAllText: 'UnSelect Product',
      itemsShowLimit: 1,
      allowSearchFilter: true
    };
    this.getCatalogueFilterItem();
    this.getProductList();
  }

  selectProduct(item: any) {
    const selectedFilter=this.selectedSearchItem[0]['id'];
    console.log(this.selectedSearchItem, this.filterItemList)
    if (this.filterItemList[selectedFilter] && this.filterItemList[selectedFilter].length) {
      this.selectedSearchItem= [];

      this.alertService.error(this.label.ALERT.Title_Error,'Filter already selected for '+selectedFilter);
    }
  }

  deSelectProduct(item: any) {

  }

  parseItems(data) {
    return JSON.parse(data);
  }


  getProductList() {

    this.shoppingService.getCatalogueProductList().subscribe((data: any) => {
      this.spinner.hide()
      data.objProdlist_new.forEach(row => {
        row.PriceScaleInfo = row.PriceScaleInfo ? JSON.parse(row.PriceScaleInfo) : [];
        row.count = 1;
      });
      data.RelatedobjProdlist.forEach(row => {
        row.PriceScaleInfo = row.PriceScaleInfo ? JSON.parse(row.PriceScaleInfo) : [];
        row.count=1;
      });
      this.productList = data.objProdlist_new;
      this.dataLoaded = true;
      this.relatedProductList = data.RelatedobjProdlist;
    }, error => {
      this.spinner.hide()
      this.alertService.error(Language.ALERT.Title_Error, Language.ALERT.Went_Wrong_Message);
    });
  }

  getCatalogueFilterItem() {
    this.shoppingService.getCatalogueFilterItem().subscribe((data: any) => {
      this.spinner.hide();
      this.sidebarItemList = data;
    }, error => {
      this.spinner.hide();
      this.alertService.error(Language.ALERT.Title_Error, Language.ALERT.Went_Wrong_Message);
    });
  }

  searchProductList(searchType = null) {
    if (this.selectedSearchItem.length == 0 && searchType == null) {
      this.selectedSearchItem = [{ id: 'Product', name: 'Product' }];
    }
    let parameter = {
      Product: '',
      Category: '',
      Country: '',
      Manufacturer: '',
      Region: '',
      SubCategory: '',
      SupplierName: ''
    }

    if (this.selectedSearchItem.length > 0 && this.universalSearch != '') {
      this.currentStorageName = '';
      if (this.selectedSearchItem[0]['id'] == 'Product') {
        parameter.Product = this.universalSearch;
      }
      if (this.selectedSearchItem[0]['id'] == 'Category') {
        if (this.filterItemList['Category'] == undefined) {
          this.filterItemList['Category'] = [];
        }
        console.log(this.filterItemList, 'checjk again')
        // this.filterItemList['Category'].push(this.universalSearch);
        this.currentStorageName = 'Category';
        parameter.Category = this.universalSearch;
      }
      if (this.selectedSearchItem[0]['id'] == 'Manufactures') {
        if (this.filterItemList['Manufactures'] == undefined) {
          this.filterItemList['Manufactures'] = [];
        }
        // this.filterItemList['Manufactures'].push(this.universalSearch);
        this.currentStorageName = 'Manufactures';
        parameter.Manufacturer = this.universalSearch;
      }
      if (this.selectedSearchItem[0]['id'] == 'Supplier') {
        if (this.filterItemList['Supplier'] == undefined) {
          this.filterItemList['Supplier'] = [];
        }
        // this.filterItemList['Supplier'].push(this.universalSearch);
        this.currentStorageName = 'Supplier';
        parameter.SupplierName = this.universalSearch;
      }
      if (this.tmpStorage && this.tmpItemName) {
        let tmpArr = [];
        for (let i = 0; i < this.filterItemList[this.tmpStorage].length; i++) {
          let tmp = this.filterItemList[this.tmpStorage][i].toLowerCase();
          tmpArr.push(tmp);
        }
        let index = tmpArr.indexOf(this.tmpItemName);
        if (index > -1) {
          this.filterItemList[this.tmpStorage] = this.filterItemList[this.tmpStorage].splice(index, 1);
        }
        console.log(tmpArr, this.tmpStorage, this.tmpItemName, index, this.filterItemList[this.tmpStorage]);
      }
      this.tmpItemName = this.universalSearch;
      this.tmpStorage = this.currentStorageName;
    }
    else {
      this.tmpItemName = '';
      this.tmpStorage = '';
    }
    parameter.Category = (this.filterItemList['Category'] && this.filterItemList['Category'].length) ? this.filterItemList['Category'].join() : parameter.Category;
    parameter.Manufacturer = (this.filterItemList['Manufactures'] && this.filterItemList['Manufactures'].length) ? this.filterItemList['Manufactures'].join() : parameter.Manufacturer;
    parameter.SupplierName = (this.filterItemList['Supplier'] && this.filterItemList['Supplier'].length) ? this.filterItemList['Supplier'].join() : parameter.SupplierName;
    parameter.Country = !this.filterItemList['Country'] ? parameter.Country : this.filterItemList['Country'].join();
    parameter.Region = !this.filterItemList['Region'] ? parameter.Region : this.filterItemList['Region'].join();
    parameter.SubCategory = !this.filterItemList['SubCategory'] ? parameter.SubCategory : this.filterItemList['SubCategory'].join();
    this.spinner.show();
    const valueObj = Object.entries(parameter).reduce((a, [k, v]) => (v ? (a[k] = v, a) : a), {})
    this.shoppingService.getProductItemByFilter(valueObj).subscribe((data: any) => {
      this.spinner.hide();
      data.forEach(row => {
        row.PriceScaleInfo = row.PriceScaleInfo ? JSON.parse(row.PriceScaleInfo) : [];
        row.count = 1;
      });
      this.productList = data;
    }, error => {
      this.spinner.hide()
      this.alertService.error(Language.ALERT.Title_Error, Language.ALERT.Went_Wrong_Message);
    });
  }

  changeFilterEvent(item) {

    let filter = item.target.value.split(' - ');
    if (this.filterItemList[filter[0]] == undefined) {
      this.filterItemList[filter[0]] = [];
    }
    if (item.target.checked == true) {
      this.filterItemList[filter[0]].push(filter[1]);
    } else {
      let index = this.filterItemList[filter[0]].indexOf(filter[1]);
      if (index > -1) {
        this.filterItemList[filter[0]].splice(index, 1);
      }
    }
    console.log(this.filterItemList)
  }

  removeQuantity(productId, productQuantity) {
    console.log($('#' + productQuantity + '-' + productId).text())
    let quantityNumber = $('#' + productQuantity + '-' + productId).text();
    let qtyNumber = Number(quantityNumber);
    if (qtyNumber > 1) {
      qtyNumber = qtyNumber - 1;
      $('#' + productQuantity + '-' + productId).text(qtyNumber);
    }
  }

  addQuantity(productId, productQuantity) {
    let quantityNumber = $('#' + productQuantity + '-' + productId).text();
    let qtyNumber = Number(quantityNumber);
    qtyNumber = qtyNumber + 1;
    $('#' + productQuantity + '-' + productId).text(qtyNumber);
  }

  addToCardItem(productId, productQuantity, CatalogueID, productName,
    SupplierNo, SupplierName, netPrice, count,item) {
    let quantityNumber = $('#' + productQuantity + '-' + productId).text();
    let qtyNumber = Number(quantityNumber);

    let param = {
      "CartID": 0,
      "CatalogueID": CatalogueID,
      "ProductName": productName,
      "SupplierNo": SupplierNo,
      "SupplierName": SupplierName,
      "Price": netPrice,
      "Quantity": count,
      "TotalAmount": netPrice * count,
      "ImageName": item.ImageName,
      "InsertedDate": new Date(),
      "UpdatedDate": new Date(),
      "Status": 0,
      "ProfileID": this.userData.Id
    }
    this.spinner.show();
    this.shoppingService.addToCart(param)
      .subscribe(res => {
        this.spinner.hide();
        this.alertService.success(Language.ALERT.Title_Success, Language.ALERT.Card_Saved_msg);
      },
        err => {
          this.spinner.hide();
          this.alertService.error(Language.ALERT.Title_Error, Language.ALERT.Went_Wrong_Message);
        })
  }
}
