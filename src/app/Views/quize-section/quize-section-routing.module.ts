import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { Language } from 'src/app/Other/translation.constants';

const routes: Routes = [{
  path: '',
  data: {
    title: Language.TAB.Admin_Section,
    status: false
  },
  children: [
    {
      path: '',
      redirectTo: 'quize-section',
      pathMatch: 'full'
    },
    {
      path: 'quize',
      loadChildren: () => import('./quize/quize.module').then(m => m.QuizeModule)
    },
    {
      path: 'quize-result',
      loadChildren: () => import('./quize-result/quize-result.module').then(m => m.QuizeResultModule)
    },
    {
      path: 'quize-master',
      loadChildren: () => import('./quize-master/quize-master.module').then(m => m.QuizeMasterModule)
    }
  ]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class QuizeRoutingModule { }