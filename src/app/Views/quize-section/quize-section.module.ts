import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { QuizeRoutingModule } from './quize-section-routing.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    QuizeRoutingModule
  ]
})
export class QuizeSectionModule { }
