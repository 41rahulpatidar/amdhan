import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { QuizeMasterComponent } from './quize-master.component';


const routes: Routes = [
  { path: "", component: QuizeMasterComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class QuizeMasterRoutingModule { }
