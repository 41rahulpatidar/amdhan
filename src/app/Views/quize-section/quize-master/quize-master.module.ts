import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { QuizeMasterRoutingModule } from './quize-master-routing.module';
import { QuizeMasterComponent } from './quize-master.component';
import { PackagedModule } from '../../../../app/Packaged/packaged.module';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';

@NgModule({
  declarations: [QuizeMasterComponent],
  imports: [
    CommonModule,
    PackagedModule,
    QuizeMasterRoutingModule,
    NgMultiSelectDropDownModule.forRoot(),
  ]
})
export class QuizeMasterModule { }
