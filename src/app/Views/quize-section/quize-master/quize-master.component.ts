import { Component, OnInit } from '@angular/core';
import { Language } from '../../../Other/translation.constants';
import { varConstants } from '../../../Other/variable.constants';
import { NgxSpinnerService } from 'ngx-spinner';
import { AlertService } from '../../../Service/alert.service';
import { Router } from '@angular/router';
import { UtilService } from '../../../Service/util.service';
import { QuizeService } from '../quize.service';
import { map, tap } from 'rxjs/operators';
@Component({
  selector: 'app-quize-master',
  templateUrl: './quize-master.component.html',
  styleUrls: ['./quize-master.component.scss']
})
export class QuizeMasterComponent implements OnInit {
  label: any = Language;
  questionList: any = [];
  dtOptions: any;
  className = QuizeMasterComponent.name;
  formMode: any;
  questionDetails: any = {
    Id: 0,
    question: "",
    questionType: "",
    questionoption: "",
    isActive: true
  };
  questionairDetails: any = {
    Id: 0,
    question: "",
    isActive: true
  }
  questionairList: any = [];
  userData:  any = {};
  questionType: any = ['input', 'textarea','radio', 'checkbox'];
  questionOptions: any = [];
  questionDetailsOption: any = [];
  questionFormSubmited:boolean = false;
  checkQuestionDetails: boolean= true;
  tabsArray: any = [];

  questionarQuestionList: any = [];
  questionarQuestionSetting: any = {};
  questionarQuestionSelectedItem: any = [];

  constructor(private router: Router,
    private alertService: AlertService,
    private spinner: NgxSpinnerService,
    private quizeService: QuizeService,
    private util: UtilService,
  ) {
    this.dtOptions = util.dtOptions;
    // this.spinner.show();
    
  }

  ngOnInit() {
     this.questionarQuestionSetting = {
      singleSelection: false,
      idField: 'Question_ID',
      textField: 'Question',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      allowSearchFilter: true,
      closeDropDownOnSelection: true,
      itemsShowLimit: 1,
    };

    this.tabsArray = [
      { id: "section1", name: this.label.QUIZE_TAB.Questionair, isActiveClass: "active" },
      { id: "section2", name: this.label.QUIZE_TAB.Question, isActiveClass: "" },
    ];
    this.questionOptions.push(0);
    // this.questionDetails.questionoption[0] = '';
    this.userData = JSON.parse(localStorage.getItem("logged_in_user_details"));
    this.getQuestionairList();
    this.getQuestionList();
   
  }

  openModal(id, data) {
    this.questionFormSubmited = false;
    if(id == 'add-question-modal'){
      this.questionDetails = {
        Id: 0,
        question: "",
        questionType: "",
        questionoption: "",
        isActive: true
      };
      if(data) {
        this.questionDetails.question = data.Question;
        this.questionDetails.questionType = data.QuestionType;
        this.questionDetails.Id = data.Question_ID;
        this.questionDetails.questionoption = data.QuestionOption;
        this.questionDetails.isActive = data.IsActive;

        this.formMode = varConstants.Edit;
      }
    }else{
      this.questionarQuestionSelectedItem = [];
      this.questionairDetails = {
        Id: 0,
        question: "",
        isActive: true
      };
      console.log(data, 'row')
      if(data) {
        this.questionairDetails.question = data.QuestionnaireName;
        this.questionairDetails.Id = data.QuestionnaireHeaderID;
        this.questionairItemByHeaderId(this.questionairDetails.Id);
       
        this.formMode = varConstants.Edit;
      }
    }
    document.getElementById(id).style.display = "block";
    document.getElementById(id).classList.add("show");
    document.body.classList.add("modal-open");
  }

  questionairItemByHeaderId(questionairHeaderId){
    this.spinner.show();
    this.quizeService.getQuestionairItemByHeaderId({Questionnaire_Header_ID: questionairHeaderId})
    .subscribe((data: any) => {
      this.spinner.hide();
      let questionarQuestionList = data;
      this.questionarQuestionSelectedItem = [];
      for(let i = 0; i < questionarQuestionList.length; i++){
        let tmp = this.questionList.find(item => item.Question_ID == questionarQuestionList[i]['QuestionID'])
        if(tmp != undefined){
          this.questionarQuestionSelectedItem.push(tmp);
        }
      }
    }, error => {
      this.spinner.hide();
      console.log(error, "error");
    });
  }

  onClickOK(item: any, modalId) {
    this.questionFormSubmited = true;
    if(modalId == 'add-question-modal'){
      if(this.questionDetails.question == '' || this.questionDetails.questionType == ''){
        return false;
      }
      
      if(this.questionDetails.questionType == 'radio' || this.questionDetails.questionType == 'checkbox'){
        if(this.questionDetailsOption.length == 0){
          return false;
        }else{
          let checkQuestionDetails = true;
          for(let i = 0; i < this.questionDetailsOption.length; i++){

            if(this.questionDetailsOption[i].trim() == '' || this.questionDetailsOption[i] == null){
              this.checkQuestionDetails = false;
              console.log(this.questionDetailsOption, 'frr', this.questionDetailsOption[i])
            }
          }
          if(this.checkQuestionDetails == false){
            return false;
          }
           console.log(this.questionDetailsOption.length, 'fr1111r', this.questionOptions)
          if(this.questionDetailsOption.length != this.questionOptions.length){
            return false;
          }
          console.log(this.questionDetailsOption, 'op', this.checkQuestionDetails)
        }
      }else{
        this.questionDetailsOption = [];
      }
      this.questionDetails.questionoption = this.questionDetailsOption.join();
      console.log(this.questionDetails)
      this.saveQuizeData();
    }else{
      if(this.questionairDetails.question == '' || this.questionarQuestionSelectedItem.length == 0){
        return false;
      }

      this.saveQuestionairData();
    }
  }

  addQuizeOptions(){
    // console.log(this.questionDetails)
    this.questionOptions.length;
    // this.questionDetails.questionoption_ = '';
    // let name = 'questionoption'+this.questionOptions.length;
    // this.questionDetails.name = '';
    this.questionOptions.push(this.questionOptions.length);
    console.log(this.questionDetailsOption, this.questionOptions)

    
  }

  removeQuizeOptions(index){
    this.questionOptions.splice(index, 1);
    this.questionDetailsOption.splice(index, 1);
    let tmp = this.questionOptions;
    this.questionOptions = [];
    for(let i = 0; i< tmp.length; i++){
      this.questionOptions.push(i);
    }
    console.log(this.questionDetailsOption, index, this.questionOptions)
  }

  getQuestionList() {
    this.questionList = [];
    this.spinner.show();
    
    this.quizeService.getQuizeData()
    .pipe(
      tap(data => { 
        this.util.PrintLogs(varConstants.INFO,this.className,this.getQuestionList.name,data);  
      }),
      map(data => {
        let newData;
      
      	newData = data;
        return newData;
      }),
    )
    .subscribe((data: any) => {
      this.spinner.hide();
      this.questionList = data;
    }, error => {
      this.spinner.hide();
      console.log(error, "error");
    });
  }

  onDeleteQuize(quize){
    console.log(this.label.ALERT)
    let isConfirm = this.alertService.warning(this.label.ALERT.Title_Warning, this.label.ALERT.Confirm_Delete_Message);
    isConfirm.then((result: any) => {
      if (result.value) {
        this.spinner.show()
        this.quizeService.deleteQuize(quize.Question_ID).subscribe((data: any) => {
          if (data) {
            this.alertService.success(Language.ALERT.Title_Success, Language.ALERT.Delete_Message);
          } else {
            this.alertService.error(Language.ALERT.Title_Error, Language.ALERT.Went_Wrong_Message);
          }
          this.getQuestionList();
          this.spinner.hide()
        }, error => {
          this.spinner.hide()
          this.alertService.error(Language.ALERT.Title_Error, Language.ALERT.Went_Wrong_Message);
        });
      }
    });
  }

  onDeleteQuestionair(item){
    console.log(this.label.ALERT)
    let isConfirm = this.alertService.warning(this.label.ALERT.Title_Warning, this.label.ALERT.Confirm_Delete_Message);
    isConfirm.then((result: any) => {
      if (result.value) {
        this.spinner.show()
        this.quizeService.deleteQuestionairDetails({id:item.QuestionnaireHeaderID}).subscribe((data: any) => {
          if (data) {
            this.alertService.success(Language.ALERT.Title_Success, Language.ALERT.Delete_Message);
          } else {
            this.alertService.error(Language.ALERT.Title_Error, Language.ALERT.Went_Wrong_Message);
          }
          this.getQuestionairList();
          this.spinner.hide()
        }, error => {
          this.spinner.hide()
          this.alertService.error(Language.ALERT.Title_Error, Language.ALERT.Went_Wrong_Message);
        });
      }
    });
  }

  onChangeTab(tabDetails: any) {
    for (let i = 0; i < this.tabsArray.length; i++) {
      if (tabDetails.id == this.tabsArray[i].id) {
        this.tabsArray[i].isActiveClass = "active";
      } else {
        this.tabsArray[i].isActiveClass = "";
      }
    }
  }
  
  saveQuizeData() {
    let param = {
          Question: this.questionDetails.question,
          QuestionType: this.questionDetails.questionType,
          QuestionOption: this.questionDetails.questionoption,
          IsActive: this.questionDetails.isActive,
          Question_ID: this.questionDetails.Id
      }
    if(this.questionDetails["Id"]){//If form is in edit mode it will go for update
      this.spinner.show()
      this.quizeService.updateQuestionDetails(this.questionDetails.Id, param).subscribe((data: any) => {
        this.alertService.success(Language.ALERT.Title_Success, Language.ALERT.Saved_Message);
        this.closeModal('add-question-modal');
        this.getQuestionList();
        this.spinner.hide()
      }, error => {
        this.spinner.hide();
        this.alertService.error(Language.ALERT.Title_Error, Language.ALERT.Went_Wrong_Message);
      });
    }else{//It will go for save
      this.spinner.show();
      
      this.quizeService.saveQuestionDetails(param).subscribe((data: any) => {
        this.alertService.success(Language.ALERT.Title_Success, Language.ALERT.Saved_Message);
        this.closeModal('add-question-modal');
        this.spinner.hide();
        this.getQuestionList();
      }, error => {
          this.spinner.hide();
          this.alertService.error(Language.ALERT.Title_Error, Language.ALERT.Went_Wrong_Message);
      });
    }
  }

  saveQuestionairData(){
    if(this.questionairDetails["Id"]){//If form is in edit mode it will go for update
      this.spinner.show();
      let param = {
          QuestionnaireName: this.questionairDetails.question,
          QuestionnaireHeaderID: this.questionairDetails.Id,
          UpdatedBy : this.userData.Id
      }
      this.quizeService.updateQuestionairDetails(param, param.QuestionnaireHeaderID).subscribe((data: any) => {
        
        this.updateQuestionairItem('UPDATE', param.QuestionnaireHeaderID, this.questionarQuestionSelectedItem);
        this.spinner.hide()
      }, error => {
        this.spinner.hide();
        this.alertService.error(Language.ALERT.Title_Error, Language.ALERT.Went_Wrong_Message);
      });
    }else{//It will go for save
      this.spinner.show();
      let param = {
          QuestionnaireName: this.questionairDetails.question,
          QuestionnaireHeaderID: this.questionairDetails.Id,
          InsertedBy : this.userData.Id
      }
      this.quizeService.addQuestionairDetails(param).subscribe((data: any) => {
      this.updateQuestionairItem('ADD', data.QuestionnaireHeaderID, this.questionarQuestionSelectedItem);
        
      }, error => {
          this.spinner.hide();
          this.alertService.error(Language.ALERT.Title_Error, Language.ALERT.Went_Wrong_Message);
      });
    }
  }

  updateQuestionairItem(Action, QuestionnaireHeaderID, QuestionItemList){
    let questionsId = [];
    for (let i = 0; i < QuestionItemList.length; i++) {
      questionsId.push(QuestionItemList[i]['Question_ID'])
    }
    if(Action == 'ADD'){
      this.spinner.show();
      let param  = {
        "QuestionnaireItemID": 0,
        "QuestionnaireHeaderID": QuestionnaireHeaderID,
        "QuestionID": questionsId.join(),
        "InsertedBy": this.userData.Id,
      }
      this.quizeService.addQuestionairItemDetails(param).subscribe((data: any) => {
        this.closeModal('add-questionair-modal');
        this.spinner.hide();
        this.getQuestionairList();
        this.alertService.success(Language.ALERT.Title_Success, Language.ALERT.Saved_Message);
      }, error => {
          this.spinner.hide();
          this.alertService.error(Language.ALERT.Title_Error, Language.ALERT.Went_Wrong_Message);
      });
    }else{
      this.spinner.show();
      let param  = {
        "QuestionnaireItemID": 0,
        "QuestionnaireHeaderID": QuestionnaireHeaderID,
        "QuestionID": questionsId.join(),
        "UpdatedBy": this.userData.Id,
      }
      this.quizeService.updateQuestionairItemDetails(param, param.QuestionnaireHeaderID).subscribe((data: any) => {
        this.closeModal('add-questionair-modal');
        this.spinner.hide();
        this.getQuestionairList();
        this.alertService.success(Language.ALERT.Title_Success, Language.ALERT.Saved_Message);
      }, error => {
          this.spinner.hide();
          this.alertService.error(Language.ALERT.Title_Error, Language.ALERT.Went_Wrong_Message);
      });
    }
  }

  getQuestionairList(){
    this.questionairList = [];
    this.spinner.show();
    
    this.quizeService.getQuestionairDetails()
    .pipe(
      tap(data => { 
        this.util.PrintLogs(varConstants.INFO,this.className,this.getQuestionList.name,data);  
      }),
      map(data => {
        let newData;
      
        newData = data;
        return newData;
      }),
    )
    .subscribe((data: any) => {
      this.spinner.hide();
      this.questionairList = data;
    }, error => {
      this.spinner.hide();
      console.log(error, "error");
    });
  }

  closeModal(id) {
    document.getElementById(id).style.display = "none";
    document.getElementById(id).classList.remove("show");
    document.body.classList.remove("modal-open");
   
  }
}
