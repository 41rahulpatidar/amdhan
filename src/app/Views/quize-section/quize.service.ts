import { Injectable } from '@angular/core';
import { RestApiService } from 'src/app/Service/rest-api.service';
import { apiUrl } from 'src/app/Other/apiUrl.constants';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class QuizeService {

  constructor(private restService: RestApiService) { }

  updateQuestionDetails(editId, request): Observable<any> {
    return this.restService.put(apiUrl.MASTER_QUESTION+'/'+editId, request);
  }

  saveQuestionDetails(request): Observable<any> {
    return this.restService.post(apiUrl.MASTER_QUESTION, request);
  }

  getQuizeData(){
  	return this.restService.get(apiUrl.MASTER_QUESTION);
  }

  deleteQuize(deleteId): Observable<any> {
    return this.restService.delete(apiUrl.MASTER_QUESTION+'/'+deleteId, deleteId);
  }

  getQuizeResultData(): Observable<any> {
    return this.restService.get(apiUrl.MASTER_QUESTION);
  }

  submitQuizeAnswer(request): Observable<any> {
    return this.restService.post(apiUrl.POST_QUESTION_ANSWER, request);
  }

  getSubmitQuizeAnswer(request): Observable<any> {
    return this.restService.get(apiUrl.QUESTION_ANSWER, request);
  }

  getCustomerList(request): Observable<any> {
    return this.restService.get(apiUrl.USER_DETAILS, request);
  }

  addQuestionairDetails(request): Observable<any> {
    return this.restService.post(apiUrl.QUESTIONAIR_HEADER, request);
  }

  updateQuestionairDetails(request, editId): Observable<any> {
    return this.restService.put(apiUrl.QUESTIONAIR_HEADER+'?id='+editId, request);
  }

  getQuestionairDetails(): Observable<any> {
    return this.restService.get(apiUrl.QUESTIONAIR_HEADER);
  }

  deleteQuestionairDetails(request): Observable<any> {
    return this.restService.delete(apiUrl.QUESTIONAIR_HEADER, request);
  }


  addQuestionairItemDetails(request): Observable<any> {
    return this.restService.post(apiUrl.QUESTIONAIR_HEADER_ITEMS_POST, request);
  }

  updateQuestionairItemDetails(request, editId): Observable<any> {
    return this.restService.put(apiUrl.QUESTIONAIR_HEADER_ITEMS_PUT, request);
  }

  getQuestionairItemDetails(): Observable<any> {
    return this.restService.get(apiUrl.QUESTIONAIR_HEADER_ITEMS);
  }

  getQuestionairItemByHeaderId(request){
    return this.restService.get(apiUrl.QUESTIONAIR_ITEMS_BY_HEADER_ID, request);
  }

}
