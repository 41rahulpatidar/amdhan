import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { QuizeResultComponent } from './quize-result.component';

describe('QuizeResultComponent', () => {
  let component: QuizeResultComponent;
  let fixture: ComponentFixture<QuizeResultComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ QuizeResultComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QuizeResultComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
