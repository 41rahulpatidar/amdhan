import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { QuizeResultComponent } from './quize-result.component';

const routes: Routes = [
  { path: "", component: QuizeResultComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class QuizeResultRoutingModule { }
