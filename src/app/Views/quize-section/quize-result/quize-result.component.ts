import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { AlertService } from 'src/app/Service/alert.service';
import { Language } from '../../../Other/translation.constants';
import { varConstants } from 'src/app/Other/variable.constants';
import { NgxSpinnerService } from 'ngx-spinner';
import { UtilService } from 'src/app/Service/util.service';
import { QuizeService } from '../quize.service';
import { DatePipe } from '@angular/common';
import { IDropdownSettings } from 'ng-multiselect-dropdown';

@Component({
  selector: 'app-quize-result',
  templateUrl: './quize-result.component.html',
  styleUrls: ['./quize-result.component.scss']
})
export class QuizeResultComponent implements OnInit {

	userData: any;
  label: any = Language;
  isMenuAccess: boolean = true;
  isAddEditPermission: boolean = true;
  quizeResultList: any = [];
  customerList: any = [];
  customerSettings: any = {};
  customerSelectedItem: any = [];


  constructor(private router: Router,
    private alertService: AlertService,
    private spinner: NgxSpinnerService,
    private quizeService: QuizeService,
    private util: UtilService,) { }

  ngOnInit() {
    this.userData = JSON.parse(localStorage.getItem("logged_in_user_details"));
    
    

    //**** User Menu Access Start ****//
    if(this.isMenuAccess == false){
      this.router.navigate(["/dashboard"]);
    }
    if(this.userData.RoleType == '2'){
      this.isAddEditPermission = true;
    }
    if(this.userData.RoleType == '3'){
      if(this.userData.Lst_RoleData1[0].ApprovalLevel == 'L01'){
        this.isAddEditPermission = false;
      }else{
        this.isAddEditPermission = false;
      }
    }
    //**** User Menu Access End ****//
    this.customerSettings = {
      singleSelection: true,
      idField: 'Id',
      textField: 'UserName',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      allowSearchFilter: true,
      closeDropDownOnSelection: true
    };


    if(this.userData.RoleType == 1){
      this.getCustomerList();
      this.getQuestionResultList();
    }else{
      this.getQuestionResultList(this.userData.Id);
    }
  }

  getQuestionResultList(customerId = null) {
    this.quizeResultList = [];
    this.spinner.show();
    let param = {
      CustomerNo: customerId
    }
    this.quizeService.getSubmitQuizeAnswer(param)
      .subscribe((data: any) => {
        this.spinner.hide();
        this.quizeResultList = data;
      }, error => {
        this.spinner.hide();
        console.log(error, "error");
      });
  }

  getCustomerList(){
    // this.customerList = [];
    let param  = {
      isCustomerPortalAccess: true
    }
    this.quizeService.getCustomerList(param)
      .subscribe((data: any) => {
        let customerData = []
        for (let i = 0; i < data.length; i++) {
          let result = {
            Id: data[i]['Id'],
            UserName: data[i]['FirstName']+ ' ' +data[i]['LastName']
          }
          customerData.push(result);
        }

        this.customerList = customerData;
        // this.customerList = data;
        console.log(this.customerList)
        // this.customerList = data;
      }, error => {
        this.spinner.hide();
        console.log(error, "error");
      });
  }

  selectCustomer(item: any){
    this.getQuestionResultList(item.Id);
  }

  deSelectCustomer(item: any){
    this.getQuestionResultList();
  }

  editQuestionANswers(item){
    console.log(item)
    localStorage.setItem('QuestionAns_ID', item.QuestionAns_ID);
    localStorage.setItem('Question_ID', item.Question_ID);
    this.router.navigate(['quize-section/quize']);
  }
  
}
