import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { QuizeResultRoutingModule } from './quize-result-routing.module';
import { PackagedModule } from '../../../../app/Packaged/packaged.module';
import { QuizeResultComponent } from './quize-result.component';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';

@NgModule({
  declarations: [QuizeResultComponent],
  imports: [
    CommonModule,
    PackagedModule,
    QuizeResultRoutingModule,
    NgMultiSelectDropDownModule.forRoot(),
    OwlDateTimeModule,
    OwlNativeDateTimeModule
  ]
})
export class QuizeResultModule { }
