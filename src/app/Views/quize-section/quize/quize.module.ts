import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { QuizeRoutingModule } from './quize-routing.module';
import { PackagedModule } from '../../../../app/Packaged/packaged.module';
import { QuizeComponent } from './quize.component';

@NgModule({
  declarations: [QuizeComponent],
  imports: [
    CommonModule,
    PackagedModule,
    QuizeRoutingModule
  ]
})
export class QuizeModule { }
