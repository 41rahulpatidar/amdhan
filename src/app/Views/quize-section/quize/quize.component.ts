import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { AlertService } from 'src/app/Service/alert.service';
import { Language } from '../../../Other/translation.constants';
import { varConstants } from 'src/app/Other/variable.constants';
import { NgxSpinnerService } from 'ngx-spinner';
import { UtilService } from 'src/app/Service/util.service';
import { QuizeService } from '../quize.service';
import { DatePipe } from '@angular/common';
import { IDropdownSettings } from 'ng-multiselect-dropdown';

@Component({
  selector: 'app-quize',
  templateUrl: './quize.component.html',
  styleUrls: ['./quize.component.scss']
})
export class QuizeComponent implements OnInit {

  userData: any;
  label: any = Language;
  isMenuAccess: boolean = true;
  isAddEditPermission: boolean = true;
  quizeQuestions: any = [];
  currentQuestion: number = 0;
  quizeDetails: any = {};

  constructor(private router: Router,
    private alertService: AlertService,
    private spinner: NgxSpinnerService,
    private quizeService: QuizeService,
    private util: UtilService,) { }

  ngOnInit() {
    this.userData = JSON.parse(localStorage.getItem("logged_in_user_details"));
    
    

    //**** User Menu Access Start ****//
    if(this.isMenuAccess == false){
      this.router.navigate(["/dashboard"]);
    }
    if(this.userData.RoleType == '2'){
      this.isAddEditPermission = true;
    }
    if(this.userData.RoleType == '3'){
      if(this.userData.Lst_RoleData1[0].ApprovalLevel == 'L01'){
        this.isAddEditPermission = false;
      }else{
        this.isAddEditPermission = false;
      }
    }
    //**** User Menu Access End ****//
    this.getQuizeQuestions();
  }
  
  getQuizeQuestions(){
    this.spinner.show();
    this.quizeService.getQuizeData().subscribe((data: any) => {
      this.spinner.hide();
      for (let i = 0; i < data.length; i++) {
        data[i]['QuestionOptionArr'] = data[i]['QuestionOption'].split(',');
        data[i]['currentIndex'] = i+1;
      }
      this.quizeQuestions = data;
      console.log(this.quizeQuestions)
      this.spinner.hide();
    }, error => {
      this.spinner.hide();
      console.log(error, "error");
    });
  }

  onChangeCheckBoxEvent(event: any, id, value){
    console.log(event, 'item', id, 'id', value, 'value')
    if(this.quizeDetails[id] == undefined){
      this.quizeDetails[id] = [];
    }
    if(this.quizeDetails[id] != ""){
      this.quizeDetails[id] = this.quizeDetails[id].split(',');
    }
    if(event.target.checked){
      if(this.quizeDetails[id].indexOf(value) == -1){
        this.quizeDetails[id].push(value);
      }
    }else{
      if(this.quizeDetails[id].indexOf(value) != -1){
        let index = this.quizeDetails[id].indexOf(value);
        this.quizeDetails[id].splice(index, 1);
      }
    }
    this.quizeDetails[id] = this.quizeDetails[id].join();
    console.log(this.quizeDetails)
  }

  submitQuizeAnswer(){
    console.log(this.quizeDetails);
    let param = []
    for(let key in this.quizeDetails){
      console.log(key, this.quizeDetails[key])
      let obj = {
        "Answer": this.quizeDetails[key],
        "Question_ID": key,
        "InsertedBy": this.userData.Id
      }
      param.push(obj);
    }
    this.spinner.show();
    this.quizeService.submitQuizeAnswer(param).subscribe((data: any) => {
      this.spinner.hide();
      this.router.navigate(['quize-section/quize-result']);
    }, error => {
      this.spinner.hide();
      console.log(error, "error");
    });
  }

  ngOnDestroy(){
    localStorage.removeItem('QuestionAns_ID');
    localStorage.removeItem('Question_ID');
  }
}
