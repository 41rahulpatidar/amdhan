import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { Language } from 'src/app/Other/translation.constants';
import { AlertService } from 'src/app/Service/alert.service';
import { AppServiceService } from 'src/app/Service/app-service.service';
import { UtilService } from 'src/app/Service/util.service';
import { varConstants } from 'src/app/Other/variable.constants';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  @ViewChild('loginForm', { static: true }) loginForm: any;
  className = LoginComponent.name;
  credentials: any = {};
  isLodingButtonEnable:boolean = false;
  label: any = Language;
  publicMessage: any = [];

  constructor(private router: Router,
    private appService: AppServiceService,
    private alertService: AlertService,
    private spinner: NgxSpinnerService,
    private util: UtilService) {
  }

  ngOnInit() {
    this.getPublicMessageDetails();
  }

  login() {
    let isFormValid = true;
    for (let key in this.credentials) {
      if (this.loginForm.form.controls[key]) {
        if (this.loginForm.form.controls[key].status == "INVALID") {
          this.loginForm.form.controls[key].markAsTouched();
          this.loginForm.form.controls[key].markAsDirty();
          document.getElementById(key).focus();
          isFormValid = false;
        }
      }
    }

    if (isFormValid == false) {
      return false;
    }
    this.isLodingButtonEnable = true;
    this.spinner.show()
    if (this.credentials) {
      if (this.credentials.username && this.credentials.password) {
        this.appService.login(this.credentials).subscribe((data: any) => {
          this.spinner.hide()
          if (data) {
            if(this.isUserAllowedToLogin(data)){
              if(data.IsCustomerPortalAccess == null || data.IsCustomerPortalAccess == false){
                 this.alertService.info('', 'Access denied to customer portal, Please contact to adminstrator.');
              }else{
                localStorage.setItem("vendor_token", "demo");
                localStorage.setItem("logged_in_user_details", JSON.stringify(data));
                localStorage.setItem('privateMessages', 'true');
                this.router.navigate(['/dashboard']);
              }
            }
            this.isLodingButtonEnable = false;
          }
        }, error => {
          this.spinner.hide()
          this.router.navigate(['/dashboard']);
          this.isLodingButtonEnable = false;           
          if(error.status == 401 && error.statusText == varConstants.isUnauthorized){
            this.alertService.error(Language.ALERT.Login_Failed, Language.ALERT.Invalid_Cred);
          }else{
            this.alertService.error(Language.ALERT.Title_Error, Language.ALERT.Went_Wrong_Message);
            this.util.PrintLogs(varConstants.ERROR, this.className, this.login.name, error);
          }
        });
      }
    }
  }

  getRandomString() {
    var token = '';
    var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var charactersLength = characters.length;
    for (var i = 0; i < 30; i++) {
      token += characters.charAt(Math.floor(Math.random() * charactersLength));
    }

    return token;
  }

  getPublicMessageDetails(){
    this.appService.getPublicMessageDetails().subscribe((data: any) => {
      this.publicMessage = data;
    })
  }

  isUserAllowedToLogin(data){
    let isAllowed = false;

    // if(data.Vendor_Details['isActive'] == varConstants.true){
    //     isAllowed = true;
    // }else{
    //   isAllowed = false;
    //   // this.alertService.error(Language.ALERT.Login_Failed, Language.ALERT.User_Inactive);
    // }
    
    isAllowed = true;
    return isAllowed;
  }
}
