import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LoginRoutingModule } from './login-routing.module';
import { LoginComponent } from './login.component';
import { PackagedModule } from 'src/app/Packaged/packaged.module';


@NgModule({
  declarations: [LoginComponent],
  imports: [
    CommonModule,
    PackagedModule,
    LoginRoutingModule
  ]
})
export class LoginModule { }
