import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { Language } from 'src/app/Other/translation.constants';

const routes: Routes = [{
  path: '',
  data: {
    title: Language.TAB.Admin_Section,
    status: false
  },
  children: [
    {
      path: '',
      redirectTo: 'marketing-section',
      pathMatch: 'full'
    },
    {
      path: 'message',
      loadChildren: () => import('./message/message.module').then(m => m.MessageModule)
    },
    {
      path: 'message-details',
      loadChildren: () => import('./message-details/message-details.module').then(m => m.MessageDetailsModule)
    }
  ]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MarketingRoutingModule { }