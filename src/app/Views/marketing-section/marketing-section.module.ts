import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MarketingRoutingModule } from './marketing-section-routing.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    MarketingRoutingModule
  ]
})
export class MarketingSectionModule { }
