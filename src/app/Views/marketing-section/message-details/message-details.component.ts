import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { AlertService } from 'src/app/Service/alert.service';
import { Language } from '../../../Other/translation.constants';
import { varConstants } from 'src/app/Other/variable.constants';
import { NgxSpinnerService } from 'ngx-spinner';
import { UtilService } from 'src/app/Service/util.service';
import { MarketingService } from '../marketing.service';
import { DatePipe } from '@angular/common';
import { IDropdownSettings } from 'ng-multiselect-dropdown';

@Component({
  selector: 'app-message-details',
  templateUrl: './message-details.component.html',
  styleUrls: ['./message-details.component.scss']
})
export class MessageDetailsComponent implements OnInit {

	@ViewChild('addMessageDetailsForm', { static: true }) addMessageDetailsForm: any;
  messageDetails: any = {};
  userData: any;
  label: any = Language;
 
  viewModeField: boolean = false;
  editMessageId: any = '';
  formSaveErrorMsg: any = '';
  pipe = new DatePipe('en-US');
  messageDetailsLabel : any = '';
  currentDate: any = new Date();
  currentSection: any;
  visibaleButton: boolean= true;
  isMenuAccess: boolean = true;
  isAddEditPermission: boolean = true;

  senderTypeList: any = [{id: 'ALL', text: 'All'},{id: 'GROUP', text: 'Group'},{id: 'CUSTOMER', text: 'Customer'}];
  senderTypeSelectItem: any = [{id: 'ALL', text: 'All'}];
  senderTypeSetting: any = {};

  groupList: any = [];
  groupSelectItem: any = [];
  groupSetting: any = {};

  customerList: any = [];
  customerSelectItem: any = [];
  customerSetting: any = {};

  constructor(private router: Router,
    private alertService: AlertService,
    private spinner: NgxSpinnerService,
    private marketingService: MarketingService,
    private util: UtilService,) { }

  ngOnInit() {
    this.messageDetails.IsActive = false;
    this.messageDetailsLabel = this.label.MARKETING_TAB.Message_Details;
    this.userData = JSON.parse(localStorage.getItem("logged_in_user_details"));
    if (history.state.data) {
      console.log(history)
      // return false;
      this.messageDetails = history.state.data;
      
    } else {
      this.router.navigate(['/dashboard']);
    }
    this.editMessageId = localStorage.getItem('editMessageId');
    

    //**** User Menu Access Start ****//
    if(this.isMenuAccess == false){
      this.router.navigate(["/dashboard"]);
    }
    if(this.userData.RoleType == '2'){
      this.isAddEditPermission = true;
    }
    if(this.userData.RoleType == '3'){
      if(this.userData.Lst_RoleData1[0].ApprovalLevel == 'L01'){
        this.isAddEditPermission = false;
      }else{
        this.isAddEditPermission = false;
      }
    }
    //**** User Menu Access End ****//

    this.senderTypeSetting = {
      singleSelection: true,
      idField: 'id',
      textField: 'text',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      allowSearchFilter: true,
      closeDropDownOnSelection: true
    };

    this.groupSetting = {
      singleSelection: true,
      idField: 'GroupID',
      textField: 'GroupCode',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      allowSearchFilter: true,
      closeDropDownOnSelection: true
    };

    this.customerSetting = {
      singleSelection: true,
      idField: 'Id',
      textField: 'CustomerName',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      allowSearchFilter: true,
      closeDropDownOnSelection: true
    };
    this.getCustomerMasterList();
  }

  getSingleMessageDetails(){
    this.spinner.show();
    this.marketingService.getSingleMessageDetails(this.editMessageId).pipe()
      .subscribe((data: any) => {
        this.spinner.hide();
        data.from = this.messageDetails.from;
        this.messageDetails = data;
        this.messageDetails.FromDate = new Date(data.From_Date);
        this.messageDetails.ToDate = new Date(data.To_Date);
        if(data.Send_Message == 'ALL' || data.Send_Message == 'All'){
          this.senderTypeSelectItem = [{id: 'ALL', text: 'All'}];
        }else if(data.Send_Message == 'CUSTOMER' || data.Send_Message == 'Customer'){
          this.senderTypeSelectItem = [{id: 'CUSTOMER', text: 'Customer'}];
          let tmp = this.customerList.find(item => item.Id == data.CustomerID)
          this.customerSelectItem = [];
          this.customerSelectItem.push(tmp);
        }else if(data.Send_Message == 'GROUP' || data.Send_Message == 'Group'){
          this.senderTypeSelectItem = [{id: 'GROUP', text: 'Group'}];
          let tmp = this.groupList.find(item => item.GroupID == data.GroupID)
          this.groupSelectItem = [];
          this.groupSelectItem.push(tmp);
          
        }
      }, error => {
        this.spinner.hide()
        this.alertService.error(Language.ALERT.Title_Error, Language.ALERT.Went_Wrong_Message);
        // this.util.PrintLogs(varConstants.ERROR, this.className, this.getSingledeliveryDetailsname, error);
      });
  }

  enableForm() {
    this.viewModeField = false;
  }

  deSelectSenderType(item: any){
    this.senderTypeSelectItem = [];
    this.senderTypeSelectItem.push(item);
    console.log(this.senderTypeSelectItem, item, '111')
  }

  getCustomerMasterList(){
    this.spinner.show();
    this.marketingService.getCustomerMasterList().pipe()
      .subscribe((data: any) => {
        this.customerList =  data
        this.customerList.map((index) => {
          index.CustomerName = index.CustomerNumber+'-'+ index.CustomerName;
          index.Id = index.Id;
        });
        this.spinner.hide();
        this.getCustomerGroupList();
      }, error => {
        this.spinner.hide();
        this.alertService.error(Language.ALERT.Title_Error, Language.ALERT.Went_Wrong_Message);
     });
  }

  getCustomerGroupList(){
    this.spinner.show();
    this.marketingService.getCustomerGroupList().pipe()
      .subscribe((data: any) => {
        this.groupList =  data
        if(this.editMessageId != undefined && this.editMessageId != null){
          this.getSingleMessageDetails();
        }
        this.spinner.hide();
      }, error => {
        this.spinner.hide();
        this.alertService.error(Language.ALERT.Title_Error, Language.ALERT.Went_Wrong_Message);
     });
  }

  saveMessageDetails(formDetails){
     if(formDetails && formDetails.form.status == 'INVALID'){
      return false;
    }
    let senderType = '';
    if(this.senderTypeSelectItem.length > 0){
      senderType = this.senderTypeSelectItem[0].id;
    }else{
      return false;
    }
    let customerIds = '';
    let groupIds = '';
    if(senderType == 'CUSTOMER'){
      for (let i = 0; i < this.customerSelectItem.length; i++) {
        // customerIds.push(this.customerSelectItem[i].id);
        customerIds = this.customerSelectItem[i].Id;
      }
    }else if(senderType == 'GROUP'){
      for (let i = 0; i < this.groupSelectItem.length; i++) {
        // groupIds.push(this.groupSelectItem[i].id);
        groupIds = this.groupSelectItem[i].GroupID;
      }
    }
    let item = {
      Send_Message: senderType,
      CustomerID: customerIds,
      GroupID: groupIds,
      From_Date: this.pipe.transform(this.messageDetails.FromDate, 'yyyy-MM-dd'),
      To_Date: this.pipe.transform(this.messageDetails.ToDate, 'yyyy-MM-dd'),
      Message: this.messageDetails.Message,
      IsActive : this.messageDetails.IsActive
    }
    console.log(item);
    this.marketingService.createMessageDetails(item).pipe()
      .subscribe((data: any) => {
        this.router.navigate(['/marketing-section/message'])
        this.spinner.hide();
      }, error => {
        this.spinner.hide();
        this.alertService.error(Language.ALERT.Title_Error, Language.ALERT.Went_Wrong_Message);
     });
  }

  ngOnDestroy(){
    localStorage.removeItem('editMessageId');
  }
  
}
