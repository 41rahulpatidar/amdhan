import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MessageDetailsRoutingModule } from './message-details-routing.module';
import { PackagedModule } from '../../../../app/Packaged/packaged.module';
import { MessageDetailsComponent } from './message-details.component';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';

@NgModule({
  declarations: [MessageDetailsComponent],
  imports: [
    CommonModule,
    PackagedModule,
    MessageDetailsRoutingModule,
    NgMultiSelectDropDownModule.forRoot(),
    OwlDateTimeModule,
    OwlNativeDateTimeModule
  ]
})
export class MessageDetailsModule { }
