import { Component, OnInit,ChangeDetectorRef  } from '@angular/core';
import { Language } from '../../../Other/translation.constants';
import { NgxSpinnerService } from 'ngx-spinner';
import { UtilService } from 'src/app/Service/util.service';
import { varConstants } from 'src/app/Other/variable.constants';
import { MarketingService } from '../marketing.service';
import { AlertService } from 'src/app/Service/alert.service';
import { Router } from '@angular/router';
import { map, tap } from 'rxjs/operators';

@Component({
  selector: 'app-message',
  templateUrl: './message.component.html',
  styleUrls: ['./message.component.scss']
})
export class MessageComponent implements OnInit {

	label: any = Language;
  deliveryArray: any = [{fieldname: "demo"}];
  dtOptions: any;
  userData: any;
  currentSection: any;
  headerVisible: boolean = true;
  datatable: any;
  isMenuAccess: boolean = true;
  isAddEditPermission: boolean = true;

  constructor(
    private router: Router,
    private spinner: NgxSpinnerService,
    private util: UtilService,
    private marketingService: MarketingService,
    private alertService: AlertService,
    private chRef: ChangeDetectorRef) { 
    this.dtOptions = this.util.dtOptions;
  }

  ngOnInit() {
    this.userData = JSON.parse(localStorage.getItem("logged_in_user_details"));
    this.currentSection = JSON.parse(localStorage.getItem("current_section"));
    this.util.tableHorizontalScroll('message_header_table');
    this.getMessageHeaderData();

    //**** User Menu Access Start ****//
    if(this.isMenuAccess == false){
      this.router.navigate(["/dashboard"]);
    }
    if(this.userData.RoleType == '2'){
      this.isAddEditPermission = false;
    }
    if(this.userData.RoleType == '3'){
      if(this.userData.Lst_RoleData1[0].ApprovalLevel == 'L01'){
        this.isAddEditPermission = false;
      }else{
        this.isAddEditPermission = false;
      }
    }

    if(this.headerVisible == true){
      this.headerVisible = this.isAddEditPermission;
    }
    //**** User Menu Access End ****//

  }

  getMessageHeaderData() {
      this.spinner.show();
      this.marketingService.getMessageHeaderData()
      .pipe(
        map(data => {
          return data;
        }),
      )
        .subscribe((data: any) => {
          this.spinner.hide();
          if (Array.isArray(data)) {
              this.deliveryArray = data;
              this.chRef.detectChanges();
              const table: any = $('table');
              this.datatable = table.DataTable();
          }
        }, error => {
          this.spinner.hide()
          this.alertService.error(Language.ALERT.Title_Error, Language.ALERT.Went_Wrong_Message);
       });
  } 

  deleteMessageDetails(messageId){
    this.spinner.show();
    let param = {
      id: messageId
    }
    this.marketingService.deleteMessageDetails(param).pipe()
      .subscribe((data: any) => {
        this.spinner.hide();
        this.getMessageHeaderData();
      }, error => {
        this.spinner.hide()
        this.alertService.error(Language.ALERT.Title_Error, Language.ALERT.Went_Wrong_Message);
        // this.util.PrintLogs(varConstants.ERROR, this.className, this.getSingledeliveryDetailsname, error);
      });
  }
  
  viewMessageDetails(message) {
    localStorage.setItem('editMessageId', message.MessageID);
    message.from = "/marketing-section/message";
    message.tab = this.label.MARKETING_TAB.Message;
    message.mode = varConstants.isRegistered;
    this.router.navigate(["/marketing-section/message-details"], { state: { data: message } });
  }

}
