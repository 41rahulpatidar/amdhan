import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MessageRoutingModule } from './message-routing.module';
import { PackagedModule } from '../../../../app/Packaged/packaged.module';
import { MessageComponent } from './message.component';

@NgModule({
  declarations: [MessageComponent],
  imports: [
    CommonModule,
    PackagedModule,
    MessageRoutingModule
  ]
})
export class MessageModule { }
