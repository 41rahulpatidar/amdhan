import { Injectable } from '@angular/core';
import { RestApiService } from 'src/app/Service/rest-api.service';
import { apiUrl } from 'src/app/Other/apiUrl.constants';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class MarketingService {

  constructor(private restService: RestApiService) { }
  
  getMessageHeaderData(): Observable<any> {
    return this.restService.get(apiUrl.MARKETING_MESSAGE_HEADER);
  }

  getSingleMessageDetails(messageId): Observable<any> {
    return this.restService.get(apiUrl.MARKETING_MESSAGE_HEADER+'/'+messageId);
  }

  createMessageDetails(param): Observable<any> {
    return this.restService.post(apiUrl.MARKETING_MESSAGE_HEADER, param);
  }

  updateMessageDetails(request, messageId): Observable<any> {
    return this.restService.put(apiUrl.MARKETING_MESSAGE_HEADER+'?MessageID='+messageId, request);
  }

  deleteMessageDetails(request): Observable<any> {
    return this.restService.delete(apiUrl.MARKETING_MESSAGE_HEADER, request);
  }

  getCustomerMasterList(): Observable<any> {
    return this.restService.get(apiUrl.CUSTOMER_MASTER);
  }

  getCustomerGroupList(): Observable<any> {
    return this.restService.get(apiUrl.MARKETING_CUSTOMER_GROUP);
  }
}
