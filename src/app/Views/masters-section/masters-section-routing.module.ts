import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { Language } from 'src/app/Other/translation.constants';

const routes: Routes = [{
  path: '',
  data: {
    title: Language.TAB.Admin_Section,
    status: false
  },
  children: [
    {
      path: '',
      redirectTo: 'master-section',
      pathMatch: 'full'
    },
    {
      path: 'customer',
      loadChildren: () => import('./customer/customer.module').then(m => m.CustomerModule)
    },
    {
      path: 'dealer',
      loadChildren: () => import('./dealer/dealer.module').then(m => m.DealerModule)
    },
    {
      path: 'material',
      loadChildren: () => import('./material/material.module').then(m => m.MaterialModule)
    },
    {
      path: 'material-details',
      loadChildren: () => import('./material-details/material-details.module').then(m => m.MaterialDetailsModule)
    },
    {
      path: 'price-list',
      loadChildren: () => import('./price-list/price-list.module').then(m => m.PriceListModule)
    }
  ]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MastersSectionRoutingModule { }