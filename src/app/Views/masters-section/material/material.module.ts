import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MaterialRoutingModule } from './material-routing.module';
import { PackagedModule } from '../../../../app/Packaged/packaged.module';
import { MaterialComponent } from './material.component';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';

@NgModule({
  declarations: [MaterialComponent],
  imports: [
    CommonModule,
    PackagedModule,
    MaterialRoutingModule,
    OwlDateTimeModule, 
    OwlNativeDateTimeModule
  ]
})
export class MaterialModule { }
