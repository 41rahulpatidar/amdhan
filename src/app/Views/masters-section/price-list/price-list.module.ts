import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PriceListRoutingModule } from './price-list-routing.module';
import { PriceListComponent } from './price-list.component';


@NgModule({
  declarations: [PriceListComponent],
  imports: [
    CommonModule,
    PriceListRoutingModule
  ]
})
export class PriceListModule { }
