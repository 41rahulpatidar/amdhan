import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MaterialDetailsRoutingModule } from './material-details-routing.module';
import { PackagedModule } from '../../../../app/Packaged/packaged.module';
import { MaterialDetailsComponent } from './material-details.component';
import { Angular2CsvModule } from 'angular2-csv';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';

@NgModule({
  declarations: [MaterialDetailsComponent],
  imports: [
    CommonModule,
    PackagedModule,
    MaterialDetailsRoutingModule,
    Angular2CsvModule,
    NgMultiSelectDropDownModule.forRoot(),
  ]
})
export class MaterialDetailsModule { }
