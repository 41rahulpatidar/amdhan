import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { AlertService } from 'src/app/Service/alert.service';
import { Language } from '../../../Other/translation.constants';
import { varConstants } from 'src/app/Other/variable.constants';
import { NgxSpinnerService } from 'ngx-spinner';
import { UtilService } from 'src/app/Service/util.service';
import { MasterService } from '../master.service';
import { IDropdownSettings } from 'ng-multiselect-dropdown';
import {DatePipe} from '@angular/common';
import { saveAs as download } from "file-saver";

@Component({
  selector: 'app-material-details',
  templateUrl: './material-details.component.html',
  styleUrls: ['./material-details.component.scss']
})
export class MaterialDetailsComponent implements OnInit {

	@ViewChild('addInquiryForm', { static: true }) addInquiryForm: any;
  materialDetails: any = {};
  userData: any;
  tabsArray: any = [];
  label: any = Language;
  pipe = new DatePipe('en-US');
  materialDetailsLabel : any = '';
 
  isMenuAccess: boolean = true;
  isAddEditPermission: boolean = true;
  questionairList: any = [];
  questionairSelectedItem: any = [];
  questionairSettings: any = {};
  questionFormSubmited:boolean = false;

  questionairDetails: any;

  constructor(private router: Router,
    private alertService: AlertService,
    private spinner: NgxSpinnerService,
    private invoicePaymentService: MasterService,
    private util: UtilService,) { }

  ngOnInit() {
    this.questionairSettings = {
      singleSelection: true,
      idField: 'QuestionnaireHeaderID',
      textField: 'QuestionnaireName',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      itemsShowLimit: 1,
      allowSearchFilter: true,
      closeDropDownOnSelection: true
    }

    this.materialDetailsLabel = this.label.DELIVERY_TAB.Delivery_Details;
    this.userData = JSON.parse(localStorage.getItem("logged_in_user_details"));
    if (history.state.data) {
      this.materialDetails = history.state.data;
      this.materialDetails.QuestionnaireHeader = this.materialDetails.QuestionnaireHeaderID;

    } else {
      this.router.navigate(['/master-section/material']);
    }
   

    //**** User Menu Access Start ****//
    if(this.isMenuAccess == false){
      this.router.navigate(["/dashboard"]);
    }
    if(this.userData.RoleType == '2'){
      this.isAddEditPermission = false;
    }
    if(this.userData.RoleType == '3'){
      if(this.userData.Lst_RoleData1[0].ApprovalLevel == 'L01'){
        this.isAddEditPermission = false;
      }else{
        this.isAddEditPermission = false;
      }
    }
    //**** User Menu Access End ****//
    this.getQuestionairList();
  }
  
  getQuestionairList(){
    this.invoicePaymentService.getQuestionairHeaderList().pipe()
      .subscribe((data: any) => {
       this.questionairList = data;
        this.questionairSelectedItem = [];
        let tmp = this.questionairList.find(item => item.QuestionnaireHeaderID == this.materialDetails.QuestionnaireHeaderID);
        if(tmp != undefined){
          this.questionairSelectedItem.push(tmp);
        }
        this.spinner.hide();
      }, error => {
        this.spinner.hide()
        this.alertService.error(Language.ALERT.Title_Error, Language.ALERT.Went_Wrong_Message);
        // this.util.PrintLogs(varConstants.ERROR, this.className, this.getSingleInvoiceDetailsname, error);
      });
  }
 
  addQuestionairItem(item: any){
    this.questionFormSubmited = true;
    if(this.questionairSelectedItem.length == 0){
      return false;
    }
    this.materialDetails.QuestionnaireHeaderID = this.questionairSelectedItem[0]['QuestionnaireHeaderID'];
    this.spinner.show();
    this.invoicePaymentService.updateMaterial(this.materialDetails, this.materialDetails.Material_ID).pipe()
      .subscribe((data: any) => {
        this.spinner.hide();
        this.alertService.success(Language.ALERT.Success, Language.ALERT.Saved_Message);
        this.router.navigate(['/master-section/material'])
     }, error => {
        this.spinner.hide()
        this.alertService.error(Language.ALERT.Title_Error, Language.ALERT.Went_Wrong_Message);
        // this.util.PrintLogs(varConstants.ERROR, this.className, this.getSingleInvoiceDetailsname, error);
      });
  }

  


  ngOnDestroy(){
    localStorage.removeItem('editMaterialId');
  }
}
