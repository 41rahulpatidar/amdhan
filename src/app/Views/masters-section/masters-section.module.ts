import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MastersSectionRoutingModule } from './masters-section-routing.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    MastersSectionRoutingModule
  ]
})
export class MastersSectionModule { }
