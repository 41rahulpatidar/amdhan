import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CustomerRoutingModule } from './customer-routing.module';
import { PackagedModule } from '../../../../app/Packaged/packaged.module';
import { CustomerComponent } from './customer.component';

@NgModule({
  declarations: [CustomerComponent],
  imports: [
    CommonModule,
    PackagedModule,
    CustomerRoutingModule
  ]
})
export class CustomerModule { }
