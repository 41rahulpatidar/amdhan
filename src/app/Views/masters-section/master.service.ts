import { Injectable } from '@angular/core';
import { RestApiService } from 'src/app/Service/rest-api.service';
import { apiUrl } from 'src/app/Other/apiUrl.constants';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class MasterService {

  constructor(private restService: RestApiService) { }
  
  getMaterialInfo(param): Observable<any> {
    return this.restService.get(apiUrl.MATERIAL_MANAGEMENT_QUESTIONAIR, param);
  }

  getMaterialInfoExportData(param): Observable<any> {
    return this.restService.get(apiUrl.EXPORT_UTILITY, param, 'blob');
  }

  getQuestionairHeaderList(): Observable<any> {
    return this.restService.get(apiUrl.QUESTIONAIR_HEADER);
  }

  updateMaterial(param, editId): Observable<any> {
    return this.restService.put(apiUrl.MATERIAL_MANAGEMENT+'/'+editId, param);
  }

  getMaterialInfoById(materialId): Observable<any> {
    return this.restService.get(apiUrl.MATERIAL_MANAGEMENT+'/'+materialId);
  }

  getQuestionairHeaderDetailsById(headerId): Observable<any> {
    return this.restService.get(apiUrl.QUESTIONAIR_HEADER+'/'+headerId);
  }
}
