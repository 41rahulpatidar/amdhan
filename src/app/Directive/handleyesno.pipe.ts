import { Pipe, PipeTransform } from '@angular/core';
import { Language } from '../Other/translation.constants';

@Pipe({
  name: 'handleYesNo'
})
export class HandleYesNo implements PipeTransform {
  label: any = Language;
  transform(value: any, ...args: any[]): any {
    return value ? this.label.Yes : this.label.No;
  }

}
