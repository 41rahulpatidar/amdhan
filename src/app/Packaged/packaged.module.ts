import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NumberDirective } from '../Directive/number-only.directive';
import { NgbModule, NgbDateParserFormatter } from '@ng-bootstrap/ng-bootstrap';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { NgxSpinnerModule } from 'ngx-spinner';
import { InputMaskModule } from 'racoon-mask-raw';
import { RecaptchaModule, RecaptchaFormsModule } from 'ng-recaptcha';
import { NgSelectModule } from '@ng-select/ng-select';
import { RestApiService } from '../Service/rest-api.service';
import { AppServiceService } from '../Service/app-service.service';
import { Interceptor } from '../Interceptor/interceptor';
import { EncodeHttpParamsInterceptor } from '../Interceptor/encode-http-params.interceptor';
import { FooterComponent } from '../Shared/footer/footer.component';
import { HeaderComponent } from '../Shared/header/header.component';
import { CardComponent } from '../Shared/card/card.component';
import { ContainerComponent } from '../Shared/container/container.component';
import { DataTablesModule } from 'angular-datatables';
import { PerfectScrollbarModule, PERFECT_SCROLLBAR_CONFIG, PerfectScrollbarConfigInterface } from 'ngx-perfect-scrollbar';
import { ModalFormComponent } from '../Shared/modal-form/modal-form.component';
import { DragScrollModule } from 'ngx-drag-scroll';
import { HandleYesNo } from '../Directive/handleyesno.pipe';

const DEFAULT_PERFECT_SCROLLBAR_CONFIG: PerfectScrollbarConfigInterface = {
  suppressScrollX: true
};


@NgModule({
  declarations: [
    HeaderComponent,
    FooterComponent,
    ContainerComponent,
    CardComponent,
    NumberDirective,
    ModalFormComponent,
    HandleYesNo
  ],
  imports: [
    CommonModule,
    NgbModule,
    HttpClientModule,
    ReactiveFormsModule,
    NgxSpinnerModule,
    // InputMaskModule,
    // RecaptchaModule,
    // RecaptchaFormsModule,
    PerfectScrollbarModule,
    NgSelectModule,
    FormsModule,
    DataTablesModule,
    DragScrollModule,
  ],
  exports: [
    NgbModule,
    HttpClientModule,
    ReactiveFormsModule,
    NgxSpinnerModule,
    // InputMaskModule,
    // RecaptchaModule,
    // RecaptchaFormsModule,
    PerfectScrollbarModule,
    NgSelectModule,
    FormsModule,
    DataTablesModule,
    FooterComponent,
    HeaderComponent,
    ContainerComponent,
    CardComponent,
    ModalFormComponent,
    DragScrollModule,
    HandleYesNo
  ],
  providers: [
    RestApiService,
    AppServiceService,
    {
      provide: PERFECT_SCROLLBAR_CONFIG,
      useValue: DEFAULT_PERFECT_SCROLLBAR_CONFIG
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: Interceptor,
      multi: true
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: EncodeHttpParamsInterceptor,
      multi: true
    },
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
})
export class PackagedModule { }
  