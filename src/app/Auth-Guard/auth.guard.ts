import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  constructor(private router: Router) { }

  canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    let url: string = state.url;
    return this.checkAuthorization(url);
  }

  canActivateChild(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    let url: string = state.url;
    return this.checkAuthorization(url);
  }

  canLoad(route: Router): Observable<boolean> | Promise<boolean> | boolean {
    return this.checkAuthorization(null);
  }

  checkAuthorization(url) {
    if (localStorage.getItem('vendor_token')) {
      return true;
    } else {
      localStorage.clear();
      this.router.navigate(['/login']);
      return false;
    }

  }
}
