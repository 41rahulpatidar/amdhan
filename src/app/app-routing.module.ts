import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from './Auth-Guard/auth.guard';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full'
  },
  {
    path: "login",
    loadChildren: () => import('./Views/login/login.module').then(m => m.LoginModule)
  },
  {
    path: "dashboard",
    canActivate: [AuthGuard],
    canLoad: [AuthGuard],
    loadChildren: () => import('./Views/dashboard/dashboard.module').then(m => m.DashboardModule)
  },
  {
    path: "master-section",
    canActivate: [AuthGuard],
    canLoad: [AuthGuard],
    loadChildren: () => import('./Views/masters-section/masters-section.module').then(m => m.MastersSectionModule)
  },
  {
    path: "inquiry-quotation-section",
    canActivate: [AuthGuard],
    canLoad: [AuthGuard],
    loadChildren: () => import('./Views/inquiry-quotation-section/inquiry-quotation-section.module').then(m => m.InquiryQuotationSectionModule)
  },
  {
    path: "asn-shipment-section",
    canActivate: [AuthGuard],
    canLoad: [AuthGuard],
    loadChildren: () => import('./Views/asn-shipment-section/asn-shipment-section.module').then(m => m.AsnShipmentSectionModule)
  },
  {
    path: "orders",
    canActivate: [AuthGuard],
    canLoad: [AuthGuard],
    loadChildren: () => import('./Views/orders/orders.module').then(m => m.OrdersModule)
  },
  {
    path: "invoice-payment",
    canActivate: [AuthGuard],
    canLoad: [AuthGuard],
    loadChildren: () => import('./Views/invoice-payment/invoice-payment.module').then(m => m.InvoicePaymentModule)
  },
  {
    path: "admin-section",
    canActivate: [AuthGuard],
    canLoad: [AuthGuard],
    loadChildren: () => import('./Views/admin-section/admin-section.module').then(m => m.AdminSectionModule)
  },
  {
    path: "marketing-section",
    canActivate: [AuthGuard],
    canLoad: [AuthGuard],
    loadChildren: () => import('./Views/marketing-section/marketing-section.module').then(m => m.MarketingSectionModule)
  },
  {
    path: "quize-section",
    canActivate: [AuthGuard],
    canLoad: [AuthGuard],
    loadChildren: () => import('./Views/quize-section/quize-section.module').then(m => m.QuizeSectionModule)
  },
  {
    path: "shopping",
    canActivate: [AuthGuard],
    canLoad: [AuthGuard],
    loadChildren: () => import('./Views/shopping/shopping.module').then(m => m.ShoppingModule)
  },
  // {
  //   path: "**",
  //   data: {
  //     title: "Page not found",
  //     status: false
  //   },
  //   // component: NotFoundComponent
  // }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
