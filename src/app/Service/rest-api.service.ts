import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient, HttpParams } from "@angular/common/http";
import { Observable, of, throwError } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';
import { apiUrl } from 'src/app/Other/apiUrl.constants';
import { environment } from '../../environments/environment';

@Injectable({
    providedIn: 'root'
})
export class RestApiService {
    //Ashish - will fetch the app_url fomr json
    environment: any = { "apiBaseURL": environment.apiUrl };

    constructor(private http: HttpClient, private router: Router) {
        //if apibase url not avail in localstorage we will put it from json file, issue fixed
        if (localStorage.getItem("app_conf")) {
            this.environment.apiBaseURL = JSON.parse(localStorage.getItem("app_conf"))[0].app_env.app_base_URL
        }
        else{
            this.get_proj_conf().subscribe(data=>{
                this.environment.apiBaseURL = data[0].app_env.app_base_URL;
            });            
        }
    }

    get(url, param?, responseType?): Observable<any> {
        if (responseType) {
            return this.http.get<any>(this.environment.apiBaseURL + url, { params: this.setParam(param), responseType: responseType })
                .pipe(
                    tap(data => this.log(url)),
                    catchError(this.handleError(url, param))
                );

        } else {
            return this.http.get<any>(this.environment.apiBaseURL + url, { params: this.setParam(param) })
                .pipe(
                    tap(data => this.log(url)),
                    catchError(this.handleError(url, param))
                );

        }
    }

    post(url, param, responseType?): Observable<any> {
        if (responseType) {
            return this.http.post<any>(this.environment.apiBaseURL + url, param, { responseType: responseType })
                .pipe(
                    tap(data => this.log(url)),
                    catchError(this.handleError(url, param))
                );
        } else {
            return this.http.post<any>(this.environment.apiBaseURL + url, param)
                .pipe(
                    tap(data => this.log(url)),
                    catchError(this.handleError(url, param))
                );
        }
    }

    put(url, param?): Observable<any> {
        return this.http.put<any>(this.environment.apiBaseURL + url, param)
            .pipe(
                tap(data => this.log(url)),
                catchError(this.handleError(url, param))
            );
    }

    delete(url, param): Observable<any> {
        return this.http.delete<any>(this.environment.apiBaseURL + url, { params: this.setParam(param) })
            .pipe(
                tap(data => this.log(url)),
                catchError(this.handleError(url, param))
            );
    }

    patch(url, data): Observable<any> {

        return this.http.patch<any>(this.environment.apiBaseURL + url, data).pipe(
                tap(data => this.log(url)),
                catchError(this.handleError(url, data))
            )
    }

    

    //OAuth Version 2.0
    login(url, param) {
        return this.http.request("POST", this.environment.apiBaseURL + url, { responseType: "json", params: param })
            .pipe(
                tap(data => this.log(url)),
                catchError(this.handleError(url, param))
            );
    }

    setParam(params) {
        let httpParams = new HttpParams();
        if (params) {
            for (let param in params)
                httpParams = httpParams.append(param, params[param]);
        }
        return httpParams;
    }

    /**
     * Handle Http operation that failed.
     * Let the app continue.
     * @param operation - name of the operation that failed
     * @param result - optional value to return as the observable result
     */
    public handleError<T>(operation = 'operation', result?: T) {
        return (error: any) => {

            // TODO: send the error to remote logging infrastructure

            if (error.error instanceof Error) {
                // A client-side or network error occurred. Handle it accordingly.
                console.log("Instanceof Error", error)
            } else if (error.status == 0) {
                //when session time out
                // localStorage.clear();
                return;
            } else {
                // The backend returned an unsuccessful response code.
                // The response body may contain clues as to what went wrong,
                return throwError(error);
            }

            // TODO: better job of transforming error for user consumption
            // this.log(`${operation} failed: ${error.message}`);

            // Let the app keep running by returning an empty result.
            return of(result as T);
        };
    }


    /** Log a HeroService message with the MessageService */
    public log(message: string) {
        // this.messageService.add('HeroService: ' + message);
    }

    get_proj_conf(): Observable<any> {
        return this.http.get(apiUrl.PROJECT_CONFIG_JSON);
    }
}