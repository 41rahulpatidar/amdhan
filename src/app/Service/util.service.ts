import { Injectable } from '@angular/core';
import { varConstants } from '../Other/variable.constants';

@Injectable({
    providedIn: 'root'
})
export class UtilService {

    isSystemLogEnabled: boolean = false;
    loggedInUserDetails:any = [];
    constructor() {
        if (localStorage.getItem("app_conf")) {
            this.isSystemLogEnabled = JSON.parse(localStorage.getItem("app_conf"))[0].app_env.is_system_log_enabled;
        }
    }

    //This will print the logs
    PrintLogs(logType, className?, methodName?, logMessage?, desc?: any) {
        if (!desc) {
            desc = '';
        }
        if (!logMessage) {
            logMessage = '';
        }
        if (!methodName) {
            methodName = '';
        }
        if (!className) {
            className = '';
        }
        //DEBUG - for printing general data
        //ERROR - for printing error data
        //INFO - for printing logged info
        if (this.isSystemLogEnabled) {
            let currentTimeStamp = new Date().toLocaleString();
            switch (logType) {
                case varConstants.INFO:
                    console.info(logType + " | " + currentTimeStamp + " | " + className + " | " + methodName + " | ", logMessage, desc);
                    break;
                case varConstants.ERROR:
                    console.error(logType + " | " + currentTimeStamp + " | " + className + " | " + methodName + " | ", logMessage, desc);
                    break;
                case varConstants.LOG:
                    console.log(logType + " | " + currentTimeStamp + " | " + className + " | " + methodName + " | ", logMessage, desc);
                    break;
                default:
                    console.log(logType + " | " + currentTimeStamp + " | " + className + " | " + methodName + " | ", logMessage, desc);
                    break;
            }
        }
    }

    //This is univarsal conf. for all tables
    dtOptions = {
        pagingType: 'full_numbers',
        pageLength: 5,
        lengthMenu: [5, 10, 25],
        processing: true,
        // select: true,
        // scrollY: true,
        // oLanguage: {"sZeroRecords": "", "sEmptyTable": ""}
    };


    setDateFormat(date) {
        if (date) {
            let newDate = new Date(date);
            let dateObj = {
                year: newDate.getFullYear(),
                month: newDate.getMonth() + 1,
                day: newDate.getDate()
            }
            return dateObj;
        } else {
            return "";
        }
    }

    getDateFormat(date) {
        if (date) {
            let day = '';
            if (date.day <= 9) {
                day = '0' + date.day;
            } else {
                day = date.day;
            }
            let month = '';
            if (date.month <= 9) {
                month = '0' + date.month;
            } else {
                month = date.month;
            }

            return date.year + "-" + month + "-" + day;
        } else {
            return "";
        }
    }

    tableHorizontalScroll(tableId?) {
        let interval = setInterval(function () {
            let element = document.getElementById(tableId);
            if (element) {
                var dataTableElement = document.getElementById(tableId).parentElement;
                if (dataTableElement) {
                    dataTableElement.setAttribute("id", "level-1-element");
                    var levelOneElement = document.getElementById("level-1-element").parentElement;
                    if (levelOneElement) {
                        if (levelOneElement.classList.contains("row")) {
                            levelOneElement.setAttribute("class", "table-horizontal-scroll");
                            dataTableElement.setAttribute("id", "");
                        }
                    }
                    clearInterval(interval);
                }
            }
        }, 100);
    }

    getLoggedInVendorID(){
        let vendorNo = null;
        if (localStorage.getItem("logged_in_user_details")) {
            this.loggedInUserDetails = JSON.parse(localStorage.getItem("logged_in_user_details"));
            if(this.loggedInUserDetails.RoleType == varConstants.adminUser){
                vendorNo = this.loggedInUserDetails.VendorNo;
            }else{
                vendorNo = null;
            }
        }
        return vendorNo;
    }
}
