import { Injectable } from '@angular/core';
import Swal from 'sweetalert2'
import { varConstants } from '../Other/variable.constants';

@Injectable({
    providedIn: 'root'
})
export class AlertService {

    constructor() {}

    success(title, message, htmlData?) {
        return Swal.fire({
            title: title,
            text: message,
            type: 'success',
            html: htmlData,
            allowOutsideClick: false,
            showConfirmButton: true,
            showCloseButton: false,
            // timer: 1000,
            animation: false
        });
    }

    error(title, message, htmlData?) {
        Swal.fire({
            title: title,
            text: message,
            type: 'error',
            html: htmlData,
            confirmButtonColor: '#ff0000',
            showCloseButton: false,
            showConfirmButton: true,
            allowOutsideClick: false,
            animation: false
        });
    }

    info(title, message, htmlData?) {
        Swal.fire({
            title: title,
            text: message,
            type: 'info',
            html: htmlData,
            showCloseButton: false,
            animation: false,
            allowOutsideClick: false
        });
    }

    warning(title, message, htmlData?) {
        return Swal.fire({
            title: title,
            text: message,
            type: 'warning',
            html: htmlData,
            showCancelButton: true,
            confirmButtonColor: '#ff0000',
            confirmButtonText: 'Yes',
            cancelButtonText: 'No',
            showCloseButton: false,
            animation: false,
            allowOutsideClick: false
        });
    }

    userInput(title,message,from,inputType, inputValidationMsg,htmlData?) {
        return Swal.fire({
            title: title,
            text: message,
            type: 'info',
            // html: htmlData,
            showCancelButton: true,
            input: inputType,
            confirmButtonText: 'Yes',
            cancelButtonText: 'No',
            showCloseButton: false,
            animation: false,
            allowOutsideClick: false,
        });
    }
    
}
