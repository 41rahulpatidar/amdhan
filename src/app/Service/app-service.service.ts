import { Injectable } from '@angular/core';
import { apiUrl } from '../Other/apiUrl.constants';
import { RestApiService } from './rest-api.service';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AppServiceService {

  constructor(private restService: RestApiService, private httpClient: HttpClient) { }
  getProjectConf(){
    return this.httpClient.get(apiUrl.PROJECT_CONFIG_JSON);
  }

  login(data): Observable<any> {
   return this.restService.post(apiUrl.LOGIN, data);
  }
  
  getPublicMessageDetails(): Observable<any> {
   return this.restService.get(apiUrl.GET_PUBLIC_MESSAGES);
  }

  getPrivateMessageDetails(param): Observable<any> {
   return this.restService.get(apiUrl.GET_PRIVATE_MESSAGES, param);
  }

  getDashboardTileCount(): Observable<any> {
   return this.restService.get(apiUrl.DASHBOARD_API);
  }
}
